#include "Game.h"
Team Team::neutral={sf::Color(0,0,0,255),0};
sf::RenderWindow* System::window;
Vec2i System::windowDim;
Vec2 System::mousepos;
Numi System::smallestSide;
uint32_t System::rand=0;
bool System::runningq=false;
EventM<Vec2> System::leftclickevent;
EventM<Vec2> System::leftclickupevent;
EventM<Vec2> System::rightclickevent;
EventM<size_t> System::consoleopenevent;
//Parallelizer System::parallelizer;

size_t System::numthreads=0;

Selector* StateSwitcher::selector;
