// *** ADDED BY HEADER FIXUP ***
#include <algorithm>
#include <complex>
// *** END ***
#pragma once

#include "Bot.h"


class WRules{
public:
    WRules():maxspeed(4),maxspeedSqr(maxspeed*maxspeed),maxangspeed(1),friction(0.95f),maxDistance(1024){}
    Num maxspeed;
    Num maxspeedSqr;
	Num maxangspeed;
	Num friction;
	Num maxDistance;
	//Num maxDistanceSqr=1024*1024;



  };

class UnboundedW:public WRules{
    public:
    UnboundedW():WRules(){};

	void setRadius(Moveable& a,Num r){
		a.shape.setRadius(r);
		a.mass=a.computeMass(r);
		a.moment=a.computeMoment(a.mass,r);
		a.inverseMass=1.0f/a.mass;
	}


    void addVelToPos(Moveable& a,Vec2 p){
        a.setPos(a.shape.pos+p);
	}

//	void setPos(Moveable& a,Vec2 p){
//	    a.setPos(p);
//	}
	//static void addPos(Moveable& a,Vec2 p);
	void addAccToVel(Moveable& a,Vec2 p){
		setVelCons(a,a.vel+p);
	}
	void setVel(Moveable& a,Vec2 v,Num k){
            a.vel=v;
            a.speedSqr=k;
	}
	void setVelCons(Moveable& a,Vec2 v){
		//Vec2 oldvel=a.vel;
		 //a.vel=v;

        Num k=Vec2Tool::getlengthSqr(v);//getlengthSqr(a.vel);
/*
		 if(isinf(k)||isnan(k)){
			 cout<<"hit infinity force skipping a step"<<endl;
			 return;
		 }*/

        setVel(a,v,k);

		 if(k>maxspeedSqr){
			 //cout<<"reached max speed"<<endl;
			 //Num k2=sqrt(k);
			 Num l=sqrt(k);
			 Vec2 norm=a.vel/l;
			 //cout<<k<<endl;
			 //a.vel=norm*maxspeed;
			 setVel(a,norm*maxspeed,k);
		 }

	}


	void addAccToVel(Moveable& a,Num r){
		a.angv+=r;//a.nAAcc;

		if(a.angv>maxangspeed){
            a.angv=maxangspeed;
		}else if(a.angv<-maxangspeed){
		    a.angv=-maxangspeed;
		}
	}
	void addVelToAng(Moveable& a,Num p){

	    rotateVec(a.ang,p);
	    /*
	    if(isinf(a.ang.x)||isnan(a.ang.x)){
			 cout<<"hit infinity force skipping a step"<<endl;
			 return;
		 }*/
	    //a.ang=ro
		//a.ang+=p;
		//constrainAng(a);
	}
};
class BoundedW:public UnboundedW{
    public:
    Vec3 bounds;
    BoundedW(Vec3 b):UnboundedW(),bounds(b){}

	void setPos(Moveable& a,Vec2 p){
	    a.setPos(p);
        constrainPos(a);
	}
	void setRadius(Moveable& a,Num r){
	    UnboundedW::setRadius(a,r);
	    constrainPos(a);
	}
    void addVelToPos(Moveable& a,Vec2 p){
		setPos(a,a.shape.pos+p);
	}

    void constrainPos(Moveable& a){
        if(!SCircle::contains(a.getShape(),bounds)){
            const Num sidedamp=0.3f;
            const Num rad=a.shape.radius;
            const Vec2 pos=a.shape.pos;
            Vec2 newpos=pos;
            Vec2 newvel=a.Vel();
            if(pos.x<bounds.x+rad){
                newpos.x=bounds.x+rad;
                newvel.x=-sidedamp*newvel.x;
            }else if( pos.x>bounds.x+bounds.z-rad){
                newpos.x=bounds.x+bounds.z-rad;
                newvel.x=-sidedamp*newvel.x;
            }
            if(pos.y<bounds.y+rad){
                newpos.y=bounds.y+rad;
                newvel.y=-sidedamp*newvel.y;
            }else if( pos.y>bounds.y+bounds.z-rad){
                newpos.y=bounds.y+bounds.z-rad;
                newvel.y=-sidedamp*newvel.y;
            }
            a.setPos(newpos);
            //UnboundedW::setPos(a,newpos);
            setVelCons(a,newvel);
        }
    }
    Vec3 Bounds(){
        return bounds;
    }
};

template<class T>
class World{
    friend class Moveable;
    friend class Ship;
private:



public:
    T ww;
    World(T w):ww(w){}

    /*
    Vec2 arrive(Bot& b,MState target){
        return arrive(b,target,b.rotTime());
    }*/


    void rotateAndGoForward(Ship& s,Vec2 desiredacc){
        Num innerangle=rotate(s,desiredacc);

        Num accuracy= (float)Z_PI_8;
        if(innerangle<=accuracy){
            goForward(s);
            s.goingForward=std::min(255,s.goingForward+16);
        }else{
            s.goingForward=std::max(0,s.goingForward-16);
        }
    }
    Vec2 arriveNoRot(Bot& b,MState target){
        return arrive(b,target,b.getTimeNeededToStopNoRot());
    }
    Vec2 arrive(Bot& b,MState target){
        return arrive(b,target,b.getTimeNeededToStopRot());
    }
    Vec2 arrive(Bot& b,MState target,Num timeNeededToStop){
        //TODO
        //Num timeNeededToStop=(b.grottime+(b.Speed())/b.Lacc());//+timeNeededToRot;


        //Num timeNeededToStop=b.getTimeNeededToStopRot();
        //cout<<target.pos<<endl;
        //maxtimespeed=
        //b.timeNeededTo

        //return Vec2(10.0f,0);
        Vec2 targetOffset=(target.pos+target.vel*timeNeededToStop)-(b.Pos()+b.Vel()*timeNeededToStop);
        Vec2 finalVel=targetOffset;
        //Num maxspeed=0.5f;

        //if(Speed()!=0){
        //finalVel.truncate(b.MaxSteerSpeed());
        Vec2Tool::truncate(finalVel,b.MaxSteerSpeed());
        finalVel-=b.Vel();
            //finalVel=truncate(finalVel,maxsteerspeed)*maxsteerspeed-Vel();

        //}
        //Vec2 k=normalize(Vel())*maxspeed;
        //finalVel=k-finalVel;
        /*
        if(Speed()>maxspeed){
            Vec2 slowdown=-Vel()
            finalVel+=slowdown;
        }*/
        //b.desiredUnitRot+=finalVel;
        //cout<<finalVel<<endl;
        return finalVel;
        //rotateAndGoForward(b,finalVel);

        //return finalVel;
        //ModifyBot::addToDesiredAcc(this,finalVel);

    }

    void kill(Ship& b){
        b.goingForward=0;
        b.goingLeft=0;
        b.goingRight=0;
    }
    void goTowards(Ship& s,Vec2 desiredacc){
    //	Vec2 p(      cos(s->Ang()), sin(s->Ang())  );

        Vec2Tool::normalize(desiredacc);
        Vec2 k=desiredacc;
        Vec2 p=k*s.Lacc();
        s.addToAcc(p);
        //addAccToVel(s,p);
    }


    void stabalizeNoRot(Ship& s){
        if(s.SpeedSqr()>0.000f){
            Vec2 vv=s.Vel();
            Vec2Tool::normalize(vv);
            Vec2 neg=-vv;
            s.addToAcc(neg*s.Lacc());
            //addAccToVel(s,neg*s.Lacc());
        }
    }
    Num rotate(Ship& s,Vec2 desiredacc){
        float desiredaccLengthSqr=Vec2Tool::getlengthSqr(desiredacc);
        float desiredaccLength=sqrt(desiredaccLengthSqr);

        Vec2 desiredaccUnitVector=desiredacc/desiredaccLength;
        //cout<<desiredaccUnitVector<<endl;

        Vec2 rotUnitVector=s.Ang();//Vec2(cos(Ang()),sin(Ang()));

        //Find magnitude of inner angle using dot product
        //Vec2 rotUnitVector=s.t();
        float innerProduct=Vec2Tool::innerProduct(rotUnitVector,desiredaccUnitVector);// rotUnitVector.x*desiredaccUnitVector.x+rotUnitVector.y*desiredaccUnitVector.y;
        float innerAngle=Vec2Tool::innerAngle(innerProduct);

        //cout<<rotUnitVector<<","<<desiredaccUnitVector<<","<<innerAngle<<endl;

        //use crossproduct to find right sign.  http://math.stackexchange.com/questions/45097/dot-product-negative-angle
        float crossProductZ=Vec2Tool::crossProduct(desiredaccUnitVector,rotUnitVector);// ((desiredaccUnitVector.x*rotUnitVector.y)-(desiredaccUnitVector.y*rotUnitVector.x));
        //the angle between targetmove angle and rotation with correct sign
        //cout<<crossProductZ<<endl;

        float targetMoveAngleRelative;
        if(crossProductZ==0.0f&&innerAngle>Z_PI_2){
            targetMoveAngleRelative=innerAngle*1.0f;
        }else{
            targetMoveAngleRelative=innerAngle*MyTool::sign(crossProductZ);
        }



        //Calculate the angle needed to come to a stop
        float angleNeededToStopRotate=(pow(fabs(s.AngV()),2)/(2*(s.Aacc())))*MyTool::sign(s.AngV());

        //cout<<angleNeededToStopRotate<<endl;

        //Add that to the target angle
        float targetMoveAngleRelativeCompensated=targetMoveAngleRelative+angleNeededToStopRotate;
        //cout<<targetMoveAngleRelative<<endl;;

        if(targetMoveAngleRelativeCompensated>0){
                //cout<<"turn left"<<endl;
            goLeft(s);
            s.goingLeft=std::min(255,s.goingLeft+16);
        }else{
            s.goingLeft=std::max(0,s.goingLeft-16);
        }
        if(targetMoveAngleRelativeCompensated<0){
                //cout<<"turn right"<<endl;
            s.goingRight=std::min(255,s.goingRight+16);
                goRight(s);

        }else{
            s.goingRight=std::max(0,s.goingRight-16);
        }
        return innerAngle;
    }

    void goLeft(Ship& s){
        s.addToAccRot(-s.Aacc());

    }
    void goRight(Ship& s){
        s.addToAccRot(s.Aacc());

    }

    void goForward(Ship& s){
        s.addToAcc(s.Ang()*s.Lacc());

    }

//    void step(SolidMoveable& l){
//        //cout<<l.Pos();
//        l.step();
//        /*
//        RepelAnswer ans=botCollideQuad.handleCollide(l);
//        addToAcc(l,ans.force);
//        addToAccRot(l,ans.rotforce);
//        healthcontrol.handleHealthLoss(l,ans.healthloss);
//        */
//        //awakecontrol.handle(l,ans.healthloss);
//
//    }
    void applyMove(PairCollidable& l){

        ww.addAccToVel(l,l.AccumLAcc());
        //l.vel*=friction;
		ww.addVelToPos(l,l.Vel());
//
		ww.addAccToVel(l,l.AccumAAcc());
		ww.addVelToAng(l,l.AngV());
        //l.endstep();
        l.accumAAcc=0;
		l.accumLAcc=Vec2(0,0);
    }





	//these are not thread safe


    std::array<Vec2,2> avoid(Bot* za,Bot* zb){
        Bot& va=*za;
        Bot& vb=*zb;

        Vec2 offset=vb.Pos()-va.Pos();
        Num ll=sqrt(Vec2Tool::getlengthSqr(offset));
        Vec2 offsetnorm=offset/ll;//normalize(offset);

        Vec2 relativeV=vb.Vel()-va.Vel();
        Num relativeVSqr=Vec2Tool::getlengthSqr(relativeV);
        Num relativeVMag=sqrt(relativeVSqr);
        Vec2 relativeVNorm=relativeV/relativeVMag;


        Num d=Vec2Tool::innerProduct(offset,relativeVNorm);


        Num h=sqrt(ll*ll-d*d);

        //v=d/t
        Num t=-d/relativeVMag;//not sure why i have to negate it
        //cout<<t<<endl;



        std::array<Vec2,2> finalans;
        Num radiusTot=va.getShape().Radius()+vb.getShape().Radius();
        Num leeway=radiusTot/2.0f;
        Num radlee=radiusTot+leeway;

        Num rottimetotal=va.getTimeNeededToRot()+vb.getTimeNeededToRot();
        t-=rottimetotal;
        //if(t>0&&t<180&&h<radlee){

        Num maxtime=150.0f;
        if(t>=0&&t<maxtime&&h<radlee){

            Num timeurgency=1.0f-(t/maxtime);
            Num hurgency=1.0f-(h/radlee);

            Num urgencyTotal=(timeurgency+hurgency)/2.0f;
            //cout<<"COLLSION ALERT"<<endl;
            //System::drawArrow(1,0,1,.5,fb.Pos(),hb.Pos());
            System::drawLine(sf::Color(255,0,0,int(urgencyTotal*255.0f*0.25f)),va.Pos(),vb.Pos());


//            Num intensity;
//
//            //Num k=(a.length/radiusTot)*3.0f;
//            //k=k*k;
//            Num j=max(0.0f,-(h-radlee));
//            //j=j*j;
//            intensity=j/radlee;
//            intensity=intensity*intensity;
//            //Num z=(h+2*t);
//            intensity+=1/(t*t);

            //desiredvel+=-intensity*(a.offset/a.length);
            //use crossproduct to find right sign.  http://math.stackexchange.com/questions/45097/dot-product-negative-angle

            Num crossProductZ=Vec2Tool::crossProduct(offsetnorm,relativeVNorm);
            //float crossProductZ=((offsetnorm.x*relativeVNorm.y)-(offsetnorm.y*relativeVNorm.x));
            //the angle between targetmove angle and rotation with correct sign
            //float targetMoveAngleRelative=innerAngle*
            Vec2 targAng=Vec2(relativeVNorm.y,-relativeVNorm.x);//-(relativeVNorm);
            //targAng*=0.5f;

            //Num urgencyMult=1000.0f;
            finalans[0]=-urgencyTotal*targAng*MyTool::sign(crossProductZ);
            finalans[1]=-finalans[0];//hurgency*targAng*Nsign(crossProductZ);

            //finala*=fb.iMass();

            //Num intensity=(1/a.lengthSqr)*avoidstate->pt.radius;
            //Num intensitynegForce=intensity*-1;
            //Vec2 norm=a.offset/a.length;
            //Vec2 comp=norm*intensitynegForce;
            //cout<<a.length<<endl;
            //nLAcc+=comp*(inverseMass*100.0f);
            //desiredvel+=comp*(ship->Lacc()*500.0f);

        }
        return finalans;

    }
      Num getMaxDistance(){
        return ww.maxDistance;
    }
};


class BoundedWorld:public World<BoundedW>{
    public:
    BoundedWorld(Vec3 bound):World<BoundedW>(BoundedW(bound)){}
    Vec3 Bounds(){
        return ww.Bounds();
    }
};

class UnboundedWorld:public World<UnboundedW>{
    public:
    UnboundedWorld():World<UnboundedW>(UnboundedW()){};

};
