// *** ADDED BY HEADER FIXUP ***
#include <algorithm>
#include <vector>
// *** END ***
#pragma once
#include "MyLib.h"
#include <unordered_map>


//#include <boost/thread.hpp>

#include "Quad.h"
#include "Moveable.h"






class PairAnswer{
    public:
        PairAnswer():a(),finished(false){}
        PairAnswer(RepelAnswer aa,bool f):a(aa),finished(f){}
    RepelAnswer a;
    bool finished;
};

//class PairCollidableCont{
//public:
//    PairCollidableCont():handledMap(){
//        handledMap.reserve(256);
//    }
//    RepelAnswer storedAnswer;
//    unordered_map<uint32_t,bool> handledMap;
//};

//class PairCollidable:public virtual QuadObj{
//    public:
//    virtual ~PairCollidable(){}
//    virtual PairCollidableCont& getPairCollidableCont()=0;
//    virtual uint32_t& getID() const=0;
//};
//




template <class T,class A,int MaxLevel>
class PairCollideQuad{
    public:

    class RepelPair{
        public:
        RepelPair(T& a,T& b,SCircle::ResidualData aa):first(&a),second(&b),ans(aa),firstAns(),secondAns(){}
        T* first;
        T* second;
        //typename Quad<T>::Answer::ResidualData ans;
        SCircle::ResidualData ans;
        RepelAnswer firstAns;
        RepelAnswer secondAns;
    };
//    class CollideAction{
//        public:
//        std::array<RepelAnswer,2> collide(T& th,T& other,typename Quad<T>::Answer::ResidualData a){};
//    };

    Quad<T,MaxLevel> quad;
    A collideAction;
    //mutable boost::mutex mm;
    //boost::condition_variable finishedACompute;
    std::vector<RepelPair> repelpairs;

    PairCollideQuad(Vec3 bound,A ca):quad(bound){
        //SolidMoveable::gameQuad=this;
        repelpairs.reserve(100);
    }


    void insert(T& t){
        quad.insert(t);
    }
    void queryRadius(std::vector<QAnswer<T>>& ans,T* self,SCircle sq){

        quad.queryRadius(ans,self,sq);
    }

    void draw(){

        quad.draw();
    }
    void findRepelPairs( T& objA,std::vector<QAnswer<T>>&  query){

        for(auto a:query){
            T& other=*a.b;
            if(!objA.handled.contains(other.getID())){
                other.handled.push_back(objA.getID());
                repelpairs.push_back(RepelPair(objA,other,a.r));

            }
        }
    }
    void computeRepels(){
        //this can happen in parallel
        calcRepels();
        //this must happen serially
        addUpRepelPairAns();

    }
    void calcRepels(){
        for(size_t i=0;i<repelpairs.size();i++){
            RepelPair& a=repelpairs[i];
            std::array<RepelAnswer,2> ans=collideAction.collide(*a.first,*a.second,a.ans);
            repelpairs[i].firstAns=ans[0];
            repelpairs[i].secondAns=ans[1];
        }
    }
    void addUpRepelPairAns(){
        for(auto i:repelpairs){
            RepelPair& a=i;
            a.first->storedAnswer+=a.firstAns;
            a.second->storedAnswer+=a.secondAns;
        }
        repelpairs.clear();
    }
    void resetStoredAnswer(T& t){
        t.storedAnswer=RepelAnswer();
    }
    void endstep(T& b){
        b.handled.clear();
        //b.handledMap.clear();
    }

    void updateQuadObj(T& b){
        quad.updateQuadObj(b);
    }
};
