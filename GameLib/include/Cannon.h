#pragma once

#include "Selector.h"
#include "Moveable.h"
#include "World.h"
class Cannon{
    public:
    Cannon(Soldier& ss):s(&ss),target(ss.Pos()){}
    Soldier* s;
    MState target;
    void step(BoundedWorld& w){
        Vec2 aa=w.arrive(*s,target);
        w.rotateAndGoForward(*s,aa);
    }
};

class Timer{
    public:
    size_t counter;
    Cannon* obj;
};
class CannonControl{
    public:
    std::vector<Cannon*> objs;
    std::vector<Timer> idleobjs;
    BoundedWorld world;
    size_t cc;
    CannonControl(BoundedWorld b):world(b),cc(0){}
    Cannon* createCannon(Soldier& s){
        Cannon* c=new Cannon(s);
        objs.push_back(c);
        return c;
    }
    void step(){
        for(auto i:objs){
            i->step(world);
        }
        cc++;
        if(cc==30){
            sparseStep();
            cc=0;
        }
    }
    void sparseStep(){
        //cout<<"boob"<<endl;
        std::vector<Cannon*> toWakeup;
        for(size_t i=0;i<idleobjs.size();i++){
            idleobjs[i].counter--;
            if(idleobjs[i].counter<=0){
                cout<<"wakeup pls"<<endl;
                toWakeup.push_back(idleobjs[i].obj);
                idleobjs.erase(idleobjs.begin()+i);
                i--;
            }
        }
        for(auto i:toWakeup){
            objs.push_back(i);
        }

    }
    void draw(){
        for(auto i:objs){
            System::drawLine(sf::Color(100,10,30),i->s->Pos(),i->target.pos);
        }
    }

    void fire(Cannon& c,Vec2Unit u){
        c.s->addToAcc(u*10.0f);
        VectorTool::remove(objs,&c);
        idleobjs.push_back(Timer{5,&c});
    }
    Cannon* begin(){
        return objs[0];
    }
    Cannon* end(){
        return objs[objs.size()-1];
    }
};


class CannonFinderAns:public FinderAns{
    public:
    Cannon* selectedCannon;
    CannonControl* cannoncontrol;
    size_t state;
    Num triggerRadius;
    CannonFinderAns():FinderAns(0){}
    CannonFinderAns(Num disSqr,Cannon* f,CannonControl* fc):FinderAns(disSqr),selectedCannon(f),cannoncontrol(fc),state(1),triggerRadius(64.0f){}
    void leftclickDown(Vec2 p){
        //cout<<"clicked on cannon!"<<endl;
        if(state==2){
            selectedCannon->target.pos=p;
            StateSwitcher::switchToDefault();
        }
    }
    void leftclickUp(Vec2 p){
        //TODO optimize this block
        Vec2 off=p-selectedCannon->s->Pos();
        Num l=sqrt(Vec2Tool::getlengthSqr(off));
        if(state==1&&l>triggerRadius){
            cannoncontrol->fire(*selectedCannon,Vec2Tool::norm(off));
            StateSwitcher::switchToDefault();
        }
        state=2;

    }
    void step(){
    }
    void draw(){
        Num rr=selectedCannon->s->getShape().radius+10.0f;
        System::drawCircle(selectedCannon->s->Pos(),rr,sf::Color::Red);
    }

};
class CannonFinder:public Finder{
public:
    CannonFinderAns ca;
    CannonControl* cannoncontrol;

    CannonFinder(CannonControl* c):ca(),cannoncontrol(c){}
    FinderAns* findClosest(Vec2 mousepos){
        Cannon* closest=nullptr;
        Num closestDis=NUMMAX;
        for(auto it=cannoncontrol->objs.begin();it!=cannoncontrol->objs.end();it++){
            Cannon* f=*it;

            Vec2 offset=f->s->Pos()-mousepos;
            Num len=sqrt(Vec2Tool::getlengthSqr(offset))-f->s->getShape().radius;

            if(len<closestDis){
                closest=f;
                closestDis=len;
            }

         }
        if(closest!=nullptr){
            ca=CannonFinderAns(closestDis,closest,cannoncontrol);
            return &ca;
        }
        return nullptr;
    }
};

