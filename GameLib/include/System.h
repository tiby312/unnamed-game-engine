// *** ADDED BY HEADER FIXUP ***
#include <algorithm>
#include <iostream>
// *** END ***
/*
 * Dd.h
 *
 *  Created on: May 24, 2013
 *      Author: ken
 */
#pragma once
//#include <GL/glew.h>
//#include <GL/GL.h>

#include <SFML/Graphics.hpp>
#include <SFML/OpenGL.hpp>
#include <SFML/Window.hpp>
#include <GL/glext.h>

#include "MyLib.h"
#include "Event.h"
//#include "Parallelizer.h"
//#include <functional>


//class ClickHandleable{
//    public:
//    virtual void leftclick(Vec2 pos)=0;
//    virtual void rightclick(Vec2 pos)=0;
//};
//
//
//
//class ClickObj{
//    public:
//    static ClickHandleable* clickobj;
//    static void registerClickHandleable(ClickHandleable* c){
//        clickobj=c;
//    }
//    static void leftclick(Vec2 pos){
//        if(clickobj!=nullptr){
//            clickobj->leftclick(pos);
//        }else{
//            cout<<"no click handler registered"<<endl;
//        }
//    }
//    static void rightclick(Vec2 pos){
//        if(clickobj!=nullptr){
//            clickobj->rightclick(pos);
//        }else{
//            cout<<"no click handler registered"<<endl;
//        }
//    }
//};



class System {
public:
	static sf::RenderWindow* window;
	//const static unsigned int windowx=512;
	//const static unsigned windowy=512;
	static uint32_t rand;
	static Vec2i windowDim;
	static Numi smallestSide;
	//static std::function<void(Vec2)> mouseHandler;
	//static vec2 boundpos;
	//static vec2 bounddim;
	static bool runningq;
    static EventM<Vec2> leftclickevent;
    static EventM<Vec2> leftclickupevent;
    static EventM<Vec2> rightclickevent;
    static EventM<size_t> consoleopenevent;
    //static Parallelizer parallelizer;
    static size_t numthreads;
    static Vec2 mousepos;
    /*
	static void Start(Num worldsize);

	//std::function<void(const Foo&, int)>
	static void registerMouseHandler(   std::function<void(Vec2)> f );
	static bool running();
	static void stepStart();
	static void stepEnd();
	 static void drawSquare(Color c,Num a,Vec3 bb);
	 static void drawLine(Color c,Num a,Vec2 p1,Vec2 p2);
	 static void drawArrow(Color c,Num a,Vec2 p1,Vec2 p2);

	 static void drawCircle(Vec2 pos,Num radius,Color color,Num a);
	 static void drawBot(Vec2 pos,Num size,Vec2Unit ang,Color color);
	 */

	 static sf::RenderWindow& Start(size_t numthr){
        //System::registerMouseHandler(ClickObj::click);
        cout<<"start create window"<<endl;
		sf::ContextSettings settings;
		settings.depthBits = 24;
		settings.stencilBits = 8;
		settings.antialiasingLevel = 4;
		settings.majorVersion = 3;
		settings.minorVersion = 0;
		windowDim=Vec2i(512,512);
		smallestSide=512;
		numthreads=numthr;
		window= new sf::RenderWindow(sf::VideoMode(windowDim.x,windowDim.y),"hi",sf::Style::Default,settings);
		//parallelizer=new Parallelizer(numthreads);
		//boundpos=b;
		//bounddim=d;

		window->setFramerateLimit(30);



//
//		glViewport(0,0,windowDim.x,windowDim.y);
//		glMatrixMode(GL_PROJECTION);
//		glLoadIdentity();
//		glOrtho( 0,worldsize,worldsize,0,-1,1);
//		glMatrixMode(GL_MODELVIEW);
//		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
//		glEnable( GL_BLEND );
//		//glEnable(GL_MULTISAMPLE_ARB);
		runningq=true;
		cout<<"created window"<<endl;
        return *window;

	}


	//std::function<void(const Foo&, int)>
	/*
	void System::registerMouseHandler(   std::function<void(Vec2)> f ){
		mouseHandler=f;
	}*/
	static bool running(){
		return runningq;
	}
	static void stepStart(){

        window->clear(sf::Color::White);
        //System::drawSquare(sf::Color::Red,Vec3(0,0,100));
    //glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);       //window.clear(sf::Color::Black);
    //glClearColor(1.0,1.0,1.0,1.0f);



	}
	static void setMousePos(){
	    Vec2i k=sf::Mouse::getPosition(*window);
	    Vec2 yy=Vec2(k.x,k.y);
        mousepos=yy/(Num)smallestSide*(Num)512;
	}
	static Vec2 MousePos(){
	    return mousepos;
	}
	static void stepEnd(){
	    rand+=1;
	    if(rand==10){
            rand=0;
	    }
        window->display();

            sf::Event event;
        setMousePos();
        while (window->pollEvent(event))
        {
            // "close requested" event: we close the window
            if (event.type == sf::Event::Closed){
                window->close();
            	runningq=false;
            }else if(event.type ==sf::Event::MouseButtonPressed){

                 if(event.mouseButton.button == sf::Mouse::Left){
                    leftclickevent.fire(MousePos());
                 }else if(event.mouseButton.button == sf::Mouse::Right){
                     rightclickevent.fire(MousePos());
                 }
			    //Vec2i k=sf::Mouse::getPosition(*window);
			}else if(event.type==sf::Event::MouseButtonReleased){

                 if(event.mouseButton.button == sf::Mouse::Left){
                    leftclickupevent.fire(MousePos());
                 }else if(event.mouseButton.button == sf::Mouse::Right){

                 }

            }else if (event.type == sf::Event::Resized){
                // adjust the viewport when the window is resized
                windowDim=Vec2i(event.size.width,event.size.height);
                cout<<windowDim.x<<","<<windowDim.y<<endl;
                smallestSide=std::min(event.size.width,event.size.height);

                //sf::View v;
                //v.setCenter(0,0);
                //sf::View v=window->getDefaultView();
                //Vec2 rr=v.getSize();
                //cout<<rr<<endl;
                //cout<<rr.left<<","<<rr.top<<","<<rr.width<<","<<rr.height<<endl;

                Num a=(float)smallestSide/(float)windowDim.x;
                Num b=(float)smallestSide/(float)windowDim.y;
                cout<<"==="<<a<<","<<b<<endl;
                sf::View v(sf::FloatRect(0,0,512.0f,512.0f));
                //v.setViewport(sf::FloatRect(0.5f, 0, 0.5f, 1));
                v.setViewport(sf::Rect<float>(Vec2(0,0),Vec2(a,b)));
                //v.setSize(Vec2(smallestSide,smallestSide));
                //v.setCenter(0,0);
                //sf::FloatRect k=window->getDefaultView().getViewport();
                //cout<<k.width<<","<<k.height<<endl;
                window->setView(v);
                //glViewport(0, 0, smallestSide, smallestSide);
            }else if (event.type==sf::Event::KeyPressed){
                if(event.key.code==sf::Keyboard::Key::T){
                    cout<<"t pressed"<<endl;
                    consoleopenevent.fire(0);
                }
            }
        }

  //glAccum(GL_ACCUM, 0.9f);
	}
    static uint32_t steprandom(){
        return rand;
    }
//
    static void drawStadium(const Stadium& st,sf::Color col){


        Vec2 mid=st.pos;

        Vec2 a=Vec2Tool::rotatey(st.aang,st.sh.ang);

        drawLine(sf::Color(255,0,255),st.pos,st.pos+a*20.0f);

        Vec2Unit pp=Vec2Tool::perp90deg(a);
        Vec2 circle1pos=mid+pp*st.sh.length/2.0f;
        Vec2 circle2pos=mid-pp*st.sh.length/2.0f;
        drawCircle(circle1pos,st.sh.radius,col);
        drawCircle(circle2pos,st.sh.radius,col);


        Vec2 line1a=circle1pos+a*st.sh.radius;
        Vec2 line1b=circle2pos+a*st.sh.radius;
        Vec2 line2a=circle1pos-a*st.sh.radius;
        Vec2 line2b=circle2pos-a*st.sh.radius;
        drawLine(col,line1a,line1b);
        drawLine(col,line2a,line2b);

    }
	 static void drawSquare(sf::Color c,Vec3 bb){
	     sf::RectangleShape rectangle(sf::Vector2f(bb.z,bb.z));

	     rectangle.setPosition(sf::Vector2f(bb.x,bb.y));
	     rectangle.setFillColor(c);
	     window->draw(rectangle);
		 //cout<<"pls"<<endl;
//		 glLoadIdentity();
//		glTranslatef(0,0,0);
//		glColor4f(c.r, c.g, c.b,a);
//		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
//		glLineWidth(4);
//		//cout<<b.x<<endl;
//		//glRecti(fix16_to_float(b.x),fix16_to_float(b.y),fix16_to_float(b.x+b.z),
//				//fix16_to_float(b.y+b.z));
//
//		Num n1=bb.x+bb.z;
//		Num n2=bb.y+bb.z;
//		glRecti(bb.x,bb.y,n1,n2);

		 /*
			sf::RectangleShape s(b.size().toSF());
			s.setPosition(b.topleft.toSF());
			s.setOutlineColor(sf::Color(0,255,255,100));
			s.setOutlineThickness(5);
			s.setFillColor(sf::Color(255,255,255,100));
			sys->window.draw(s);
			*/
	}

	static void drawLine(sf::Color c,Vec2 p1,Vec2 p2){
	    Vec2 offset=p2-p1;
	    Num ssqr=Vec2Tool::getlengthSqr(offset);
	    sf::RectangleShape line(Vec2(sqrt(ssqr),3));
	    line.setFillColor(c);

	    //Num ang=offset*sqrt(ssqr);

        Num ang=atan2f(offset.y,offset.x);
        line.rotate(ang*(180/Z_PI));
        line.setPosition(p1);
        window->draw(line);
//		 glLineWidth(2.5);
//		glColor4f(c.r,c.g, c.b,a);
//		glBegin(GL_LINES);
//		glVertex2i(p1.x,p1.y);
//		glVertex2i(p2.x,p2.y);
//		//gl.Vertex3f(p1.X, p1.Y, 0.0);
//		//gl.Vertex3f(p2.X, p2.Y, 0);
//		glEnd();
	 }
	static void drawArrow(sf::Color c,Num a,Vec2 p1,Vec2 p2){
//		drawLine(c,a,p1,p2);
//		//drawLine(r,g,b,a,p2,p2)
//		drawCircle(p2,4,c,a);

	}
	 static void drawCircle(Vec2 pos,Num radius,sf::Color color){
	     sf::CircleShape circle(radius);

        circle.setFillColor(color);
        circle.setPosition(pos);
        circle.setOrigin(Vec2(radius,radius));
        window->draw(circle);


//		 glColor4f(color.r,color.g, color.b,a);
//		 glLineWidth(2);
//		 float cx=pos.x;
//		 float cy=pos.y;
//		  const int num_segments=5+(int)(radius/10);
//			float theta = 2 * 3.1415926 / float(num_segments);
//			float c = fastycos(theta);//precalculate the sine and cosine
//			float s = fastysin(theta);
//
//
//			float x = radius;//we start at angle = 0
//			float y = 0;
//
//			glBegin(GL_LINE_LOOP);
//			for(int ii = 0; ii < num_segments; ii++)
//			{
//				glVertex2f(x + cx, y + cy);//output vertex
//
//				//apply the rotation matrix
//				float t;
//				t = x;
//				x = c * x - s * y;
//				y = s * t + c * y;
//			}
//			glEnd();

	 }
	static void drawBot(Vec2 pos,Num size,Vec2Unit ang,sf::Color color){
//
//		drawCircle(pos,size,color,0.5);
//		//glLoadIdentity();
//		glColor4f(color.r,color.g,color.b,1.0f);
//		Num j=size/sqrtf(2.0f);
//		glBegin(GL_TRIANGLES);
//		Vec2 p1(-j,-j);
//		Vec2 p2(j,0.0f);
//		Vec2 p3(-j,+j);
//		glVertex2f(pos.x+(p1.x*ang.x)-(p1.y*ang.y),pos.y+(p1.x*ang.y)+(p1.y*ang.x));
//		glVertex2f(pos.x+(p2.x*ang.x)-(p2.y*ang.y),pos.y+(p2.x*ang.y)+(p2.y*ang.x));
//		glVertex2f(pos.x+(p3.x*ang.x)-(p3.y*ang.y),pos.y+(p3.x*ang.y)+(p3.y*ang.x));
//		glEnd();
		//glLoadIdentity();

	}





};
