// *** ADDED BY HEADER FIXUP ***
#include <iostream>
// *** END ***
#pragma once

#include <thread>
#include <chrono>
#include <ctime>
#include <list>
#include <SFML/System/Clock.hpp>
#include "System.h"
#include <queue>
class Fps{
public:
	sf::Clock clock;
	int avg;
	int total;
	std::queue<int> q;
	int counter;
	Fps():clock(),avg(),total(),q(),counter(0){
		clock.restart();
	}
	void step(){
		float currentTime = clock.getElapsedTime().asSeconds();
		clock.restart();
		uint32_t fps = (uint32_t)(1.f / (float)(currentTime));
		//cout<<fps<<endl;


		total+=fps;
		q.push(fps);
		if(q.size()>100){
			uint32_t k=this->q.back();
			q.pop();
			total-=k;
		}
		avg=(uint32_t)(total/(float)q.size());

		if(counter<=0){
			cout<<"fps:"<<avg<<endl;
			counter=30;
		}
		counter--;
		/*
		char c[10];
		sprintf(c, "%d", (uint32_t)avg);
		std::string string(c);
		sf::String str(string);

		sf::Text textt(str,*ff);
		w->draw(textt);	}
		*/
	}

};
