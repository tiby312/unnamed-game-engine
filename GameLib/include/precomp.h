#ifndef PRECOMP
#define PRECOMP

#include "MyLib.h"
#include "Quad.h"
#include "PairCollideQuad.h"
#include "FAttacker.h"
#include "FArranger.h"
#include "Selector.h"
#include "System.h"
#include "World.h"
#include "Event.h"
#include "Fps.h"
// #include your rarely changing headers here

#endif
