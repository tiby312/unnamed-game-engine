// *** ADDED BY HEADER FIXUP ***
#include <algorithm>
#include <vector>
// *** END ***
#pragma once
#include <queue>
#include <condition_variable>
#include <thread>
#include <chrono>

#include "System.h"


template <class T>
class PVector{
    struct Pair{
        size_t sstart;
        size_t send;
    };
    public:
    PVector():threads(System::numthreads){};
    std::vector<Pair> threads;
    void push_back(T t){
        vec.push_back(t);
        computeThreadLoads();
        //printThreadLoads();
    }
    bool empty(){
        return vec.empty();
    }
    void computeThreadLoads(){
        size_t numthr=threads.size();
        size_t numo=vec.size();

        size_t q=numo/numthr;
        size_t r=numo-(numthr*q);


        std::vector<size_t> widths(numthr);
        for(size_t i=0;i<numthr;i++){
            widths[i]=q;
        }
        for(size_t i=0, rcounter=0;rcounter<r;rcounter++,i++){
            widths[i]+=1;
        }

        size_t counter=0;
        for(size_t i=0;i<numthr;i++){
            threads[i].sstart=counter;
            counter+=widths[i];
            threads[i].send=counter-1;
        }

    }

    void printThreadLoads(){
        for(size_t i=0;i<threads.size();i++){
            size_t sstart=threads[i].sstart;
            size_t send=threads[i].send;
            cout<<"("<<sstart<<","<<send<<")";
        }
        cout<<endl;
    }
    T & operator[](const size_t & i) { return vec[i]; }
    size_t size(){
        return vec.size();
    }
    T * begin(){
        return &vec[0];
    }
    T * end(){
        return &vec[size()];
    }
    private:
        std::vector<T> vec;

};

//
//class Parallelizer{
//    struct Job{
//        Job():func(){}
//        Job(std::function<void()> f):func(f){}
//        std::function<void()> func;
//        void run(){
//            func();
//        }
//    };
//    public:
//
//    template<class T,class T2>
//    void doParallel(PVector<T>& a,std::function<void(size_t,T,T2)> func,T2 shared){
//        std::vector<Job> jobst;
//        jobst.reserve(System::numthreads);
//        for(size_t i=0;i<a.threads.size();i++){
//            size_t sstart=a.threads[i].sstart;
//            size_t send=a.threads[i].send;
//            PVector<T>* ap=&a;
//            std::function<void()> func2=[sstart,send,func,ap,shared](){
//                for(size_t j=sstart;j<=send;j++){
//                    func(  j,(*ap)[j]  ,shared );
//                }
//            };
//            Job j=Job(func2);
//            jobst.push_back(j);
//        }
//        for(size_t i=0;i<jobst.size()-1;i++){
//            addJob(jobst[i]);
//        }
//        jobst[jobst.size()-1].run();
//        waitFinish();
//    }
//
//    void startWorker(){
//        while(1){
//            Job j;
//            {
//               boost::unique_lock<boost::mutex> lock(mm);
//               while (jobs.empty()) {
//                    notempty.wait(lock);
//               }
//               j=jobs.front();
//               jobs.pop();
//                notfull.notify_one();
//               busyworkers++;
//            }
//                 j.run();
//            {
//                boost::unique_lock<boost::mutex> lock(mm);
//                busyworkers--;
//                if(jobs.empty()&&busyworkers==0){
//                    emptyFinish.notify_one();
//                }
//            }
//        }
//    }
//    size_t maxQueueSize;
//    std::queue<Job> jobs;
//    size_t busyworkers;
//    mutable boost::mutex mm;
//    boost::condition_variable notempty;
//    boost::condition_variable emptyFinish;
//    boost::condition_variable notfull;
//    //std::condition_variable notfull;
//
//
//    Parallelizer():maxQueueSize(System::numthreads*16),jobs(),busyworks(0),mm(),notempty(),emptyFinish(),notfull(){
//        for(size_t i=0;i<System::numthreads-1;i++){
//            new boost::thread([this] { startWorker(); } );
//        }
//    }
//
//    void addJob(Job j){
//        {
//            boost::unique_lock<boost::mutex> lock(mm);
//            while(jobs.size()>=maxQueueSize){
//                notfull.wait(lock);
//            }
//            jobs.push(j);
//            notempty.notify_one();
//        }
//
//    }
//    void waitFinish(){
//
//       boost::unique_lock<boost::mutex> lock(mm);
//       while (!jobs.empty()||busyworkers!=0) {
//            emptyFinish.wait(lock);
//       }
//
//    }
//
//};
