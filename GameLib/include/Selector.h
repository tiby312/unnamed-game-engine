// *** ADDED BY HEADER FIXUP ***
#include <iostream>
// *** END ***
#pragma once
#include "MyLib.h"
#include "System.h"
#include "Event.h"
//#include "Formation.h"


//#include "GameStuff.h"

//
//class SelectableCont{
//    public:
//        SelectableCont():action(){}
//        PositionEventM action;
//        //PositionEventM rotateTo;
//};



class Selectable{
    public:
    virtual ~Selectable(){}
    virtual void leftclickDown(Vec2 p)=0;
    virtual void leftclickUp(Vec2 p)=0;
    virtual void step()=0;
    virtual void draw()=0;
};


class FinderAns:public Selectable{
    Num dis;
    //bool finished;
    public:
    FinderAns(Num d):dis(d){}
    Num Dis(){
        return dis;
    }

};

class Finder{
    public:
    virtual FinderAns* findClosest(Vec2 mousepos)=0;
};





//template <class T>
class Selector{
    public:
        Selectable* defaultSelected;
        Selectable* selectedItem;



    Selector(Selectable* df):defaultSelected(df),selectedItem(df){
        System::leftclickevent.registerListener([this](Vec2 p){this->leftclick(p);},true);
        System::leftclickupevent.registerListener([this](Vec2 p){this->leftclickup(p);},true);
        System::rightclickevent.registerListener([this](Vec2 p){this->rightclick(p);},true);
    }


    void step(){
        selectedItem->step();
    }
    void draw(){
        selectedItem->draw();
    }
    void rightclick(Vec2 pos){
        //selectedItem->rickclickDown(pos);
    }
    void leftclick(Vec2 pos){
        cout<<"left clicked="<<pos<<endl;
        selectedItem->leftclickDown(pos);
    }
    void leftclickup(Vec2 pos){
        selectedItem->leftclickUp(pos);
    }
};

class StateSwitcher{
    public:
    static Selector* selector;
    static void init(Selector* s){
        selector=s;
    }
    static void switchTo(Selectable* s){
        cout<<"switching selectable"<<endl;
        selector->selectedItem=s;
    }
    static void switchToDefault(){
        cout<<"switching to default"<<endl;
        selector->selectedItem=selector->defaultSelected;
    }
};

class DefaultSelectableFinder:public Selectable{
    public:
    std::vector<Finder*> finders;
    Num fingerRadius;
    DefaultSelectableFinder(std::vector<Finder*> fs):finders(fs),fingerRadius(30.0f){}
    void leftclickDown(Vec2 p){
        FinderAns* ans=nullptr;
        Num closestDis=999999.9f;
        for(size_t i=0;i<finders.size();i++){
            FinderAns* aa=finders[i]->findClosest(p);
            if(aa!=nullptr&&aa->Dis()<closestDis){
                closestDis=aa->Dis();
                ans=aa;
            }
        }
        //cout<<"finder size="<<finders.size()<<"ans="<<ans<<"closestdis="<<closestDis<< "finderradius="<<fingerRadius<<endl;
        if(ans!=nullptr&&closestDis<fingerRadius){
            cout<<"selected something"<<endl;
            StateSwitcher::switchTo(ans);
        }else{
            cout<<"nothing to select:"<<p<<endl;
        }
    };
    void leftclickUp(Vec2 p){};
    void step(){};
    void draw(){};
};


