// *** ADDED BY HEADER FIXUP ***
#include <algorithm>
#include <complex>
#include <iostream>
#include <vector>
// *** END ***

#pragma once
#include "Formation.h"
#include "BotControl.h"



class Frame{
    Vec2Unit up;
    Vec2Unit right;
    public:
    Frame(Vec2 u):up(u),right(Vec2Tool::perp90deg(up)){
    }
    Vec2 convertFromAbsoluteFrame(Vec2 k){
        Vec2 f;
        f.x=Vec2Tool::innerProduct(k,up);
        f.y=Vec2Tool::innerProduct(k,right);
        return f;
    }
};

class FormationCollideAction:public BounceAction{
public:
//    std::array<Vec2,2> pushForce(Formation& th,Formation& avoidobj,SCircle::ResidualData aa){
//
//        Frame fr(th.getDesiredUnitRot());
//        Vec2 reloffset=fr.convertFromAbsoluteFrame(aa.offset);
//
//        Num theta=atan2f(reloffset.y,reloffset.x);
//        if(abs(theta)>Z_PI_2){
//            return std::array<Vec2,2>();
//        }
//        Num normedtheta=theta/Z_PI_2;
//        Num truncted=MyTool::truncate(normedtheta,0.1f);
//        Num forcemag=-0.05f*truncted;
//
//        Vec2 forceUnitVec=Vec2Tool::perp90deg(Vec2Tool::norm(aa.offset));
//        Vec2 fin=forceUnitVec*forcemag;
//
//        return std::array<Vec2,2>{-fin,fin};
//
//    }
    std::array<RepelAnswer,2> collide(Formation& th,Formation& avoidobj,SCircle::ResidualData a){;
//        //SolidMoveable& th=dynamic_cast<SolidMoveable&>(th2);
//        //SolidMoveable& avoidobj=dynamic_cast<SolidMoveable&>(other2);
//        if(a.radius<=0.001f){
//            RepelAnswer ans1=RepelAnswer{Vec2(),0.0f,0.0f};
//            RepelAnswer ans2=RepelAnswer{Vec2(),0.0f,0.0f};
//            std::array<RepelAnswer, 2> answer = { {ans1,ans2} };
//            return answer;
//        }
//


         Num closestDis;
         if(Stadium::intersect(th.getStadium(),avoidobj.getStadium(),closestDis)){
            //cout<<"WAAAAAAAAAAAAAAA"<<endl;
            //cout<<closestDis<<endl;
            StadiumShape as=th.shape;
            StadiumShape bs=avoidobj.shape;
            Num n=as.radius+bs.radius;
            Num closeness=closestDis/n;
            //cout<<"closeness="<<closeness<<endl;
            Num coe=(1.0f-closeness)*0.02f;
            Vec2 drag1=-th.Vel()*coe;
            Vec2 drag2=-avoidobj.Vel()*coe;
            RepelAnswer ans1=RepelAnswer{drag1,0.0f,0.0f};
            RepelAnswer ans2=RepelAnswer{drag2,0.0f,0.0f};
            std::array<RepelAnswer, 2> answer = { {ans1,ans2} };
            //std::array<RepelAnswer,2> bounceanswer;
             if(th.getTeam()!=avoidobj.getTeam()){
                answer[0].force*=2.0f;
                answer[1].force*=2.0f;
             }
             //cout<<"yo"<<endl;
             System::drawLine(sf::Color::Green,th.Pos(),avoidobj.Pos());
             return answer;
         }else{
            RepelAnswer ans1=RepelAnswer{Vec2(),0.0f,0.0f};
            RepelAnswer ans2=RepelAnswer{Vec2(),0.0f,0.0f};
            std::array<RepelAnswer, 2> answer = { {ans1,ans2} };
            return answer;
         }



        //Num n=th.getShape().Radius()+avoidobj.getShape().Radius();
        //Num coe=(1.0f-(a.radius/n))*0.03f;





//        if(th.getTeam()!=avoidobj.getTeam()){
//            //bounceanswer=BounceAction::collide(th,avoidobj,a);
//            {
//
//            auto k=pushForce(th,avoidobj,a);
//            answer[0].force+=k[0];
//            answer[1].force+=k[1];
//            }
//            {
//                auto k=pushForce(avoidobj,th,-a);
//                answer[1].force+=k[0];
//                answer[0].force+=k[1];
//            }
//
//
//
//            Num t=avoidobj.Radius()+th.Radius();
//            if(a.radiuspair.radius<t*0.5f){
//                Num intensity=(t-a.radiuspair.radius);
//                //intensity*=intensity;
//                intensity*=0.00001f;
//                Vec2 norm=a.offset/a.radiuspair.radius;
//                Vec2 comp=norm*(intensity*-1.0f);
//
//                answer[0]+=RepelAnswer{comp*th.iMass(),0,0};
//                answer[1]+=RepelAnswer{-comp*avoidobj.iMass(),0,0};
//            }
//
//            //return std::array<RepelAnswer,2>{{bounceanswer[0],bounceanswer[1]}};
//            #ifdef DRAWDEBUG
//            System::drawLine(sf::Color::Green,th.Pos(),avoidobj.Pos());
//            #endif
//        }

        //return std::array<RepelAnswer,2>{{answer[0]+bounceanswer[0],answer[1]+bounceanswer[1]}};

        /*
        std::array<RepelAnswer,2> bounceanswer=BounceAction::collide(th,avoidobj,a);
        return std::array<RepelAnswer,2>{{bounceanswer[0],bounceanswer[1]}};
        */
        //return answer;

    }

};



class FormationControl:public ControlP<Formation,FormationCollideAction,BoundedWorld,4>{
    friend class Formation;
    public:
    BotControl* botcontrol;
    FormationControl(BoundedWorld w,BotControl& bc):ControlP(w),botcontrol(&bc),counter(0),cc(0){}
    Numi counter;
    Numi cc;
};


class FormationControlManager{
    public:

    static void recomputeRadii(FormationControl& fc,Formation& f){
        Num cr=15.0f;
        Num att=30.0f;
        Num ww;
        Num hh;
        if(f.farranger.ranks.Height()<f.farranger.ranks.Width()){
            //shape.ang=Vec2Unit(1,0);
            ww=f.farranger.ranks.Width();
            hh=f.farranger.ranks.Height();
            f.shape.ang=Vec2Unit(1,0);
            //smallradius=hh;
        }else{
             f.shape.ang=Vec2Unit(0,1);
             hh=f.farranger.ranks.Width();
            ww=f.farranger.ranks.Height();
            //smallradius=ww*farranger.phx.spacing/2.0f;
        }
        f.smallradius=hh*f.farranger.phx.spacing/2.0f;
        //Num d=hh*f.farranger.phx.spacing/2.0f;
        f.shape.radius=cr+f.smallradius;
        f.shape.length=ww*f.farranger.phx.spacing-2*f.smallradius;
        Num lol=ww*f.farranger.phx.spacing/2.0f+cr;
        fc.setRadius(f,lol);
        f.attackradius=f.shape.radius+att;
    }

    static Formation& createFormation(FormationControl& fc,Soldier* leader,const std::vector<Soldier*> bots,MState m,Team t,Num spacing){
        Formation* fp=new Formation(leader,bots,m,t,fc.getAnId(),spacing);


        Soldier* b=fp->farranger.leader;
        BotControl* bcc=fc.botcontrol;
        MState jj=fp->getAbsTarget(fp->farranger.phx.leaderoffset);
        bcc->ControlP::move(*b,jj.pos);

        FormationControl* fcc=&fc;
        for(size_t i=0;i<fp->farranger.ranks.Numspaces();i++){
            Soldier* b=fp->farranger.ranks.at(i);
            MState jj=fp->getAbsTarget(fp->farranger.phx.offsets.at(i));
            bcc->ControlP::move(*b,jj.pos);

                std::function<void(Vec2)> func=[fcc,fp,b](Vec2 v){
                    removeMember(*fcc,*fp,*b);
                };
                b->registerDeathListener(func);
        }


//        std::function<void(Bot*)> func=[bcc,fp](Bot* b){
//            fp->getAbsTarget(fp.farranger.offsets)
//            BotControlManager::moveBot(*bcc,*b,off fp->getPosTarget(*b).pos);
//            //BotControlManager::setAng(*bcc,*b,fp->Ang());
//        };
//        fp->forEveryMemberAndLeader(func);

        fc.addObj(*fp);
        recomputeRadii(fc,*fp);
        return *fp;
    }
    static void removeMember(FormationControl& fc,Formation& f,Soldier& l){
            FAttackerManager::fightableDie(f.fattacker,l);
            PhalanxAddRemoveControl::removeMember(f.farranger,&l);
            recomputeRadii(fc,f);
    }

    static void resize(FormationControl& fc,Formation& f,size_t w,Vec2Unit k){
        f.resizeAndRot(w,k);
        recomputeRadii(fc,f);
    }

    static void step(FormationControl& fc){
//        for(auto i:formations){
//            formationquad.findRepelPairs(*i);
//        }
//        formationquad.computeRepels();
        fc.ControlP::step();

        for(auto i:fc.objs){
            Formation& l=*i;
            step(fc,l);
            RepelAnswer ans=l.getStoredAnswer();
            l.addToAcc(ans.force);
            l.addToAccRot(ans.rotforce);
            fc.pairCollideQuad.resetStoredAnswer(l);
        }

    }
    static void endstep(FormationControl& fc){
        fc.cc++;
        if(fc.cc>2){
            fc.cc=0;

            if(!fc.objs.empty()){
                //handleRotation(fc,*fc.objs[fc.counter]);
                handleAttack(fc,*fc.objs[fc.counter]);
                fc.counter++;
                if(fc.counter>=(Numi)fc.objs.size()){
                    fc.counter=0;
                }
            }

        }
        for(auto i:fc.objs){
            i->resetDesiredAcc();
            //endstep(*i);

            fc.world.applyMove(*i);
            fc.pairCollideQuad.endstep(*i);
        }
        fc.updatePairCollideQuad();
    }

    static void step(FormationControl& fc,Formation& f){

//
//        RepelAnswer ans=f.getStoredAnswer();
//            world->addToAcc(f,ans.force);
//            world->addToAccRot(f,ans.rotforce);
//            formationquad.resetStoredAnswer(f);


//        RepelAnswer ans=formationquad.handleCollide(f,f.CollideRadius());
//        world->addToAcc(f,ans.force);
//        world->addToAccRot(f,ans.rotforce);




//

        f.moveMembers(fc.botcontrol->world);
//
//        try{
//            Bot& z=f.getLeader();
//            //botcontrol->step(z);
//            //gamestuff->botquad().step(z);
//            Vec2 k=fc.world->arrive( z,f.getPosTarget(z) );
//            //cout<<k<<endl;
//            //world->rotateAndGoForward(z,k);
//            fc.world->rotateAndGoForward(z,k);
//            //cout<<"wee"<<endl;
//        }catch(int i){cout<<"no leader"<<endl;};













        Vec2 finalVel=fc.botcontrol->world.arriveNoRot(f,f.target);
        Num lensqr=Vec2Tool::getlengthSqr(finalVel);
        //Num len=sqrt(lensqr);
        //cout<<f.target.pos<<endl;
        if(lensqr>0){
                /*
            finalVel=finalVel/len;


            Vec2 p1=f.getAbsTarget(f.getOffset(f.getLeader())).pos;
            Vec2 p2=f.getLeader().Pos();
            Vec2 v1=f.Vel()*f.getTimeNeededToStopNoRot();
            Vec2 v2=f.getLeader().Vel()*f.getLeader().getTimeNeededToStopRot();
            Vec2 rotoffset=(p1+v1)-(p2+v2);//getrotatedoffset(leader->offset);//leader->Pos();//-leader->offset;


            Num offlen=sqrt(rotoffset.getlengthSqr());
            Num maxdis=10.0f;
            if(offlen>maxdis){
                    //cout<<"stab"<<endl;
                //state.stabalizeNoRot();

                //Vec2 norm=rotoffset/offlen;
                //Vec2 ff=leader->Pos()+(norm*maxdis)-getrotatedoffset(leader->offset);
                //state.setPos(ff);

                //cout<<"setting"<<endl;
                //ModifyMoveable::setPos(&state,ff);
            }else{

            }*/
            //cout<<finalVel<<endl;
            fc.world.goTowards(f,finalVel);
        }
        fc.world.rotate(f,f.desiredUnitRot);
    }
    static void handleRotation(FormationControl& fc,Formation& f){
        //cout<<"rotate"<<endl;
        Vec2 direction;
        for(auto it=fc.objs.begin();it!=fc.objs.end();it++){
            Formation* ff=*it;
            direction+=getDir(f,*ff);
        }
        //direction+=f.state.Vel()*(std::min(1.0f,f.state.Speed()/100.0f));
        if(Vec2Tool::getlengthSqr(direction)<=0){
            //direction=f.Ang();
        }else{
            Vec2Tool::normalize(direction);
            f.setRotationTarget(direction);
        }


        //cout<<f.desiredUnitRot;
    }
    static Vec2 getDir(Formation& f1,Formation& f2){
        try{
        Vec2 offset=f2.getAbsTarget(f2.farranger.phx.leaderoffset).pos-
                    (f1.Pos());
        //static Num maxlensqr=20.0f*20.0f;
        Vec2 jj=f2.Pos()-f1.Pos();
        Num lensqr=Vec2Tool::getlengthSqr(jj);//std::max(maxlensqr,getlengthSqr(offset));
        if(lensqr<=0.0001f){
            return Vec2();
        }
        Num len=sqrt(lensqr);
        Vec2 finish;

            Vec2 norm=offset/len;
            Num mag=1.0f/(lensqr*lensqr);
            finish=norm*mag;

        if(f1.getTeam()!=f2.getTeam()){
            return finish;
        }else{
            return Vec2();//-0.003f*finish;
        }
        }catch(int i){return Vec2();}

    }
    static void handleAttack(FormationControl& fc,Formation& f){
        //for(auto k=engagementsToStop.begin();k!=engagementsToStop.end();k++){
        //cout<<"handle attack!"<<endl;
        std::vector<QAnswer<Soldier>> ans;
        ans.reserve(8);
        Vec2 pp=f.Pos();
        Num frr=f.getShape().Radius();//AttackRadius();
        //RadiusPair frrp(frr,frr*frr);
        SCircle sqr(pp,frr);
        fc.botcontrol->queryRadius(ans,&f.getLeader(),sqr);
        //cout<<"query circle="<<f.Pos()<<","<<frr<<"  size="<<ans.size()<<endl;
        //cout<<"num engagements="<<f.fattacker.engagements.size()<<endl;
        for(auto k=ans.begin();k!=ans.end();k++){
            QAnswer<Soldier> a=*k;
            Soldier& bot=*a.b;
            Num res;
            if(Stadium::intersectCircle(f.getStadium(),bot.getShape(),res)&&bot.getTeam()!=f.getTeam()&&bot.getTeam()!=Team::neutral){
                    //cout<<"VEEEEEEEEEEEEEEE"<<endl;
                //Vec2 w=bot.Pos()+(bot.Vel()*bot.getTimeNeededToStopRot())-f.Pos();
                if(Vec2Tool::getlengthSqr(bot.Pos()-f.Pos())<f.AttackRadiusSqr()){
                    //f.fattacker.checkAttack(bot);
                    //cout<<"YOOOOO"<<endl;
                    auto got = f.fattacker.engmap.find(&bot);
                    //cout<<"blep"<<endl;
                     if(got==f.fattacker.engmap.end()){
                        if(f.fattacker.freeAllies.empty()){
                            //waitingToBeAttacked.push_back(bot);
                            //cout<<"no free allies"<<endl;
                        }else{
                            //cout<<"engage!"<<endl;
                            FAttackerManager::createEngagement(f.fattacker,&bot);

                        }
                    }else{
                        //cout<<"found"<<endl;
                        Engagement* e=got->second;
                        size_t siz=e->allies.size();
                        if(siz<3){
                                FAttackerManager::addToEngagement(f.fattacker,e,3-siz);
                            //FAttackerManager::createEngagement(f.fattacker,&bot,3);
                        }

                        got->second->checked=true;
                    }
                }
            }
        }

        FAttackerManager::findEngagementsToStop(f.fattacker);

    }
    static void draw(FormationControl& fc,sf::RenderWindow& w){
        //attackTexture.clear(sf::Color(0,0,0,0));
        //sf::Color col(0,0,255,255);
        for(auto i:fc.objs){
//            sf::CircleShape circle(i->AttackRadius());
//            circle.setFillColor(col);
//            circle.setPosition(i->Pos());
//            circle.setOrigin(Vec2(i->Radius(),i->Radius()));
//
            //w.draw(circle);
            //attackTexture.draw(circle);
            draw(*i);
        }
        //fc.pairCollideQuad.draw();
//        attackTexture.display();
//        sf::Sprite sprite(attackTexture.getTexture());
//        sprite.setColor(sf::Color(255, 255, 255, 100));
//        w.draw(sprite);
        //System::drawCircle(f.Pos(),f.CollideRadius(),sf::Color(255,0,0,50));
    }
    static void draw(Formation& f){

        //f.getLeader().draw();
/*
        Formation* formation=&f;
        for(size_t i=0;i<f.farranger.phx.offsets.Numspaces();i++){
            Vec2 off=f.farranger.phx.offsets.at(i);
            Soldier* s=f.farranger.ranks.at(i);
            Vec2 p1=s->Pos();
            Vec2 p2=f.getAbsTarget(off).pos;
            sf::Color cc(0,0,0,20);
            System::drawLine(cc,p1,p2);

//            try{
//                FAttackable& fa=s->getPrey();
//                    System::drawLine(sf::Color(200,50,0,30),p1,fa.Pos());
//            }catch(int i){}

        }*/
        System::drawStadium(Stadium(f.shape,f.Pos(),f.Ang()),sf::Color(0,100,100,20));
        //#ifdef DRAWDEBUG
        {
            Vec2 p1=f.Pos();
            Vec2 p2=f.Pos()+f.desiredUnitRot*10.0f;
            System::drawLine(sf::Color(50,0,255,100),p1,p2);
        }
        System::drawLine(sf::Color(100,100,100,60),f.Pos(),f.getTarget().pos);
        System::drawCircle(f.getTarget().pos,16,sf::Color(100,200,50,60));
        System::drawCircle(f.Pos(),f.AttackRadius(),sf::Color(0,200,30,20));
        System::drawCircle(f.Pos(),f.getShape().Radius(),sf::Color(255,0,0,20));
        //#endif
    }

};

