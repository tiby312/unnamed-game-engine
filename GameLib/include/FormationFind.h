#pragma once
#include "FormationControl.h"

class FormationFinderAns:public FinderAns{
    public:
    FormationFinderAns(Num dis,Formation* f,FormationControl* fc):FinderAns(dis),selectedFormation(f),formationcontrol(fc),state(1),maxd(200.0f){}
    FormationFinderAns():FinderAns(0),selectedFormation(),state(1),maxd(200.0f){}

    Formation* selectedFormation;
    FormationControl* formationcontrol;
    size_t state;
    void leftclickUp(Vec2 p){
        if(state==2){
            state2to0(p);
            StateSwitcher::switchToDefault();
        }
    }
    void leftclickDown(Vec2 p){
        if(state==1){
            state1to2(p);
            state++;
        }

    }
    void step(){
        step(state);
    }
    void draw(){
        draw(state);
    }
    Vec2 anchor;
    Vec2 anchoroffset;
    Num anchoroffsetlength;
    Vec2Unit anchorRot;
    Numi resizestate;
    size_t desiredWidth;
    PhalanxP phx;
    Num deadradius;
    Num maxd;
    void state1to2(Vec2 pos){
        cout<<"1to2"<<endl;
        anchor=pos;
        phx=selectedFormation->farranger.phx;
    }
    void state2to0(Vec2 pos){
        cout<<"2to1"<<endl;
        selectedFormation->setMoveTarget(anchor);
        if(resizestate==1){
            Vec2 j=anchoroffset;
            Vec2Tool::normalize(j);
            FormationControlManager::resize(*formationcontrol,*selectedFormation,desiredWidth,(Vec2Unit)j);
        }
        selectedFormation=nullptr;
        resizestate=0;
    }
    size_t disToWidth(){
        Num maxap=10.0f/1.0f;
        Num minap=1.0f/6.0f;
        size_t num=phx.offsets.Numspaces();
        Num maxWidth=sqrtf(maxap*num);
        Num minWidth=sqrtf(minap*num) ;
        //cout<<"maxWidth="<<maxWidth<<" minWidth="<<minWidth<<endl;
        Num wid=minWidth+max(0.0f,min(1.0f,(anchoroffsetlength-deadradius)/maxd))*(maxWidth-minWidth);
        return max((size_t)1,(size_t)floorf(wid));
    }
    void step(size_t ss){
        if(ss==2){
            anchoroffset=System::MousePos()-anchor;
            anchoroffsetlength=sqrt(Vec2Tool::getlengthSqr(anchoroffset));
            anchorRot=anchoroffset/anchoroffsetlength;
            if(anchoroffsetlength>deadradius&&resizestate==0){
                resizestate=1;
                desiredWidth=disToWidth();//selectedFormation->farranger.ranks.Width();
            }

            if(resizestate==1){

                size_t k=disToWidth();
                //cout<<k<<","<<desiredWidth<<endl;
                if(k>=1&&k<phx.offsets.Numspaces()){
                    if(k!=phx.offsets.Width()){
                        phx.resize(k);
                        desiredWidth=k;
                    }
                }
                    //void resize(size_t w){

            }
        }
    }
    void drawFormationBluePrint(){
        sf::Color col=sf::Color(150,70,70,200);
        for(size_t i=0;i<phx.offsets.Numspaces();i++){
            Vec2 off=phx.offsets.at(i);
            Vec2 abs=PhalanxRotPosControl::getAbsTarget(anchor,Vec2(),anchorRot,off).pos;
            System::drawCircle(abs,3,col);
        }
    }
    void draw(size_t ss){
        if(ss>0){
            Formation& f=*selectedFormation;
            sf::Color col=sf::Color(0,100,200,60);
            f.forEveryMemberAndLeader([col](Bot* b){
                System::drawCircle(b->Pos(),b->getShape().Radius()+1,col);
            });
            System::drawCircle(f.Pos(),f.getShape().Radius()+2,col);
            sf::Color zep(255,0,0);
            if(ss>1){
                System::drawCircle(anchor,6,zep);
            }
            if(resizestate==1){
                System::drawLine(zep,System::MousePos(),anchor);
                drawFormationBluePrint();

            }
        }

    }
};

class FormationFinder:public Finder{
    public:
    FormationFinderAns fa;
    FormationControl* formationcontrol;

    FormationFinder(FormationControl* f):fa(),formationcontrol(f){}
    FinderAns* findClosest(Vec2 mousepos){
        Formation* closest=nullptr;
        Num closestDis=9999999.0f;
        for(auto it=formationcontrol->objs.begin();it!=formationcontrol->objs.end();it++){
            Formation* f=*it;

            Vec2 offset=f->Pos()-mousepos;
            Num len=sqrt(Vec2Tool::getlengthSqr(offset))-f->SmallRadius();

            if(len<closestDis){
                closest=f;
                closestDis=len;
            }

         }
        if(closest!=nullptr){
            //cout<<"closest formation="<<closestDis<<endl;
            fa=FormationFinderAns(closestDis,closest,formationcontrol);
            return &fa;
        }
        return nullptr;
    }
};

