#pragma once
#include "Parallelizer.h"
#include "World.h"
#include "PairCollideQuad.h"
template<class T1,class T2,class TWorld,int MaxLevel>
class ControlP{
    public:
    TWorld world;
    //Parallelizer* parallelizer;
    PairCollideQuad<T1,T2,MaxLevel> pairCollideQuad;
    size_t idcounter;
    PVector<T1*> objs;
    std::vector<std::vector<QAnswer<T1>>> queries;
    ControlP(TWorld& w):world(w),pairCollideQuad(w.Bounds(),T2()),idcounter(0),objs(){}

    Numi getAnId(){
        Numi i=idcounter;
        idcounter++;
        return i;
    }

    void addObj(T1& o){
//        if(!contains(o.Pos(),world->bounds)){
//            o.setPos()
//        }
        objs.push_back(&o);
        pairCollideQuad.insert(o);
         std::vector<QAnswer<T1>> vecz;
        vecz.reserve(10);
        queries.push_back(vecz);
    }


    void step(){
        for(size_t i=0;i<objs.size();i++){
            pairCollideQuad.queryRadius(queries[i],objs[i],objs[i]->getShape());
        }

        for(size_t i=0;i<objs.size();i++){
            pairCollideQuad.findRepelPairs(*objs[i],queries[i]);
            queries[i].clear();
        }

        pairCollideQuad.computeRepels();


    }
    void setRadius(T1& b,Num k){
        b.setRadius(k);
        pairCollideQuad.updateQuadObj(b);
    }
    void move(T1& b,Vec2 pos){
        b.setPos(pos);
        pairCollideQuad.updateQuadObj(b);
    }
    void setAng(T1& b,Vec2Unit ang){
        b.setAng(ang);
    }


    void updatePairCollideQuad(){
        for(auto i:objs){
            pairCollideQuad.updateQuadObj(*i);
        }
    }
     void queryRadius(std::vector<QAnswer<T1>>& ans,T1* self,SCircle sq){
        pairCollideQuad.queryRadius(ans,self,sq);
    }
    void insert(T1& t){
        pairCollideQuad.insert(t);
    }

};



