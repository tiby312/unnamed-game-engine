// *** ADDED BY HEADER FIXUP ***
#include <algorithm>
#include <iostream>
#include <list>
#include <vector>
// *** END ***
#pragma once
//#include "Formation.h"
#include "MyLib.h"
#include <type_traits>
#include "Moveable.h"
//#include "GameStuff.h"

#include "hungarian.h"

class MMatrixControl{
    public:
    static size_t getHeight(size_t width,size_t num){
        size_t height=num/width;
        if(num-height*width>0){
            height++;
        }
        return height;
    }
    static size_t calcBackRankSize(size_t width,size_t height,size_t num){
        return width-(width*height-num);
    }
};

template <class T>
class MMatrix{
public:
    MMatrix(size_t w,size_t h,size_t n):width(w),height(h),backranksize(MMatrixControl::calcBackRankSize(w,h,n)),m(n){
    }
    void setM(std::vector<T> nm){
        if(nm.size()!=m.size()){
            cout<<"cannot set this to matrix. wrong size"<<endl;
        }
        m=nm;
    }

    void eraseFromBackRank(size_t k){
        size_t j=(Height()-1)*Width()+k;
        m.erase(m.begin()+j);
        backranksize--;
        if(Height()==1){
            width--;
        }
        if(backranksize==0){
            height--;
            if(height>0){
                backranksize=Width();
            }
        }
    }
    void resize(size_t w){
        width=w;
        height=MMatrixControl::getHeight(width,Numspaces());
        backranksize=MMatrixControl::calcBackRankSize(width,height,Numspaces());
    }
    Vec2i posFromIth(size_t i){
        return Vec2i(i%Width(),i/Width());
    }
    T& at(size_t row,size_t column){
        return m.at(row*width+column);
    }
    T& at(size_t ith) {
        return m.at(ith);
    }

    size_t Width(){
        return width;
    }
    size_t Height(){
        return height;
    }
    size_t Numspaces() const{
        return m.size();
    }
    size_t NumBackrankSpace(){
        return backranksize;
    }
    void forEveryMember( const std::function<void(T)> f){

            std::for_each(m.begin(),m.end(),[=](T i){
                     f(i);
            });
    }

    void print(std::function<void(T)> func){
        if(width==0||height==0){
            cout<<"cannot print nothing"<<endl;
            return;
        }
        cout<<"|----------------------------|"<<endl;
        cout<<"Printing mmatrix. Dim=("<<width<<","<<height<<")"<<endl;
        for(Numi i=0;i<Numspaces();i++){
            if(i % width==0){
                cout<<endl;
            }
            func(m[i]);

        }
        cout<<endl;
        cout<<"|----------------------------|"<<endl;
    }

    private:

    size_t width;
    size_t height;
    //size_t numspace;
    size_t backranksize;
    std::vector<T> m;


};

class PhalanxP{
public:
    PhalanxP():offsets(0,0,0){}
     PhalanxP(size_t w,size_t h,size_t num,Num sp,Num lsp,Vec2 mid):
         offsets(w,h,num),spacing(sp),leaderspacing(lsp),phalanxmid(mid){}
    MMatrix<Vec2> offsets;
    Num spacing;
    Num leaderspacing;
    Vec2 phalanxmid;
    Vec2 leaderoffset;


    void resize(size_t w){
        offsets.resize(w);
        update();
    }
    void update(){

        phalanxmid=Vec2(offsets.Width()*spacing/2.0f,offsets.Height()*spacing/2.0f);

        Num ll;
        if(offsets.Height()>0){
                ll=phalanxmid.y+leaderspacing;
        }else{
            ll=0.0f;
        }

        leaderoffset=Vec2Tool::perp90deg(Vec2(0,ll));
        //cout<<"leaderoffset="<<leaderoffset<<endl;
        //phalanxmid.y+
        //CANNOT BE UNSIGNED SINCE HEIGHT COULD BE ZERO
         Numi backrankpos=offsets.Height()-1;
        for(Numi i=0;i<backrankpos;i++){
            for(size_t j=0;j<offsets.Width();j++){
                offsets.at(i,j)=Vec2Tool::perp90deg(Vec2(j*spacing,i*spacing)-phalanxmid);
            }
        }
        size_t numBackRank=offsets.NumBackrankSpace();
        size_t diff=offsets.Width()-numBackRank;
        //cout<<"numBackRank="<<numBackRank<<" offsets.width="<<offsets.Width()<<endl;
        for(size_t j=0;j<numBackRank;j++){
            Vec2 z=Vec2(((Num)j+(diff/2.0f))*spacing,backrankpos*spacing)-phalanxmid;
            offsets.at(backrankpos,j)=Vec2Tool::perp90deg(z);
        }
        //cout<<"finishd!"<<endl;

    }

};

template <class T>
class Phalanx{
    friend class PhalanxContControl;
    public:

        PhalanxP phx;
        T leader;
        MMatrix<T> ranks;



        T getLeader(){
           return leader;
        }
        void forEveryMember( const std::function<void(T)> f){
            ranks.forEveryMember(f);
        }
        void forEveryMemberAndLeader(const std::function<void(T)> f){
            f(getLeader());
            forEveryMember(f);
        }
        bool valid(){
        if(ranks.Width()==0||ranks.Height()==0||ranks.Numspaces()==0){
            cout<<"cannot resize nothing"<<endl;
            return false;
        }
        return true;
    }
//        void flip(){
//            cout<<"flip ms pls"<<endl;
//            MMatrix<T> newranks=ranks;//(ranks.Width(),ranks.Height(),ranks.Numspaces());
//
//
//            size_t bp=ranks.Height()-1;
////
////
////            //flip along x axis;
//
//            size_t diff=ranks.Width()-ranks.NumBackrankSpace();
//            size_t backstart=diff/2;
//            //size_t backend=backstart+ranks.NumBackrankSpace();
//
//            for(size_t i=0;i<ranks.NumBackrankSpace();i++){
//                newranks.at(0,backstart+i)=ranks.at(bp,i);
//            }
//            for(size_t j=1;j<bp;j++){
//                for(size_t i=0;i<ranks.NumBackrankSpace();i++){
//                    newranks.at(j,backstart+i)=ranks.at(ranks.Height()-1-j,backstart+i);
//                }
//            }
//            for(size_t i=0;i<ranks.NumBackrankSpace();i++){
//                newranks.at(bp,i)=ranks.at(0,backstart+i);
//            }
//
//            //flip along y axis
//            MMatrix<T> newranks2=newranks;
//            for(size_t i=0;i<bp;i++){
//                for(size_t j=0;j<ranks.Width();j++){
//                    newranks2.at(i,j)=newranks.at(i,ranks.Width()-1-j);
//                }
//            }
//            for(size_t j=0;j<ranks.NumBackrankSpace();j++){
//                newranks2.at(bp,j)=newranks.at(bp,ranks.NumBackrankSpace()-1-j);
//            }
//
//
//            ranks=newranks2;
//        }


    private:
        Phalanx(size_t w,size_t h,Num sp,Num lsp,Vec2 mid,T l,std::vector<T> ms):
            phx(w,h,ms.size(),sp,lsp,mid),
            leader(l),
            ranks(w,h,ms.size()){};
};


class PhalanxContControl{
    friend class FInsertable;
    public:
    template <class T>
    static Phalanx<T> createPhalanx(T leader,const std::vector<T>& members, Num s){
        Vec2ui ndim=desiredDim(members.size());
        Phalanx<T> ph(ndim.x,ndim.y,s,2.0f*s,Vec2(),leader,members);
        ph.ranks.setM(members);
        update(ph);
        return ph;
    }



//    static void fillArrangeOffsets(MMatrix<Vec2>& kk){
//            Num mid=kk.width/2.0f;
//
//            size_t backrankpos=kk.height-1;
//            for(Numi i=0;i<kk.height-1;i++){
//                for(Numi j=0;j<kk.width;j++){
//                    Vec2 kkk=Vec2(j-mid,i);
//                    kk.m.at(i).at(j)=kkk;
//                }
//            }
//            size_t diff=kk.width-kk.backranksize;
//            for(Numi j=0;j<kk.backranksize;j++){
//                Vec2 z=Vec2((Num)j+(diff/2.0f)-mid,backrankpos);
//                kk.m.at(backrankpos).at(j)=z;
//            }
//
//        }
    template <class T>
    static Vec2 getOffset(Phalanx<T>& ph,T a){
        Vec2ui ind=a->index;
        return ph.phx.offsets.at(ind.y,ind.x);
    }

    template <class T>
    static void print(Phalanx<T>& ph){
        std::function<void(T)> func=[](T a){
            if(a!=nullptr){
                cout<<"x";
            }else{
                cout<<"-";
            }
        };
        ph.ranks.print(func);
    }
    static Vec2ui desiredDim(size_t num){
            const Num aspect=5.0f/3.0f;
            //const Num aspect=3.0f/5.0f;
            Num width=sqrtf(num*aspect);
            Num height=sqrtf(num/aspect);
            width=round(width);
            height=round(height);
            Num diff=width*height-num;

            if(fabs(diff)>width/2.0){
                if(diff>0){
                    height--;
                    diff=diff-width;
                }else if(diff<0){
                    height++;
                    diff=diff+width;
                }
            }
            if (diff<0){
                width++;
            }
            return Vec2ui(width,height);
        }

        template <class T>
        static void update(Phalanx<T>& ph){

            ph.phx.update();
            if(!ph.valid()){
                return;
            }
            Numi backrankpos=ph.ranks.Height()-1;
            for(Numi i=0;i<backrankpos;i++){
                for(size_t j=0;j<ph.ranks.Width();j++){
                    ph.ranks.at(i,j)->index=Vec2ui(j,i);
                }
            }
            size_t numBackRank=ph.ranks.NumBackrankSpace();
            //size_t diff=ph.ranks.Width()-numBackRank;
            for(size_t j=0;j<numBackRank;j++){
                ph.ranks.at(backrankpos,j)->index=Vec2ui(j,backrankpos);
            }

        }


};


class PhalanxResizeControl{
    public:


    template <class T>
    static void constructProblemMatrix(std::vector<int>& prob,MMatrix<T>& currm,MMatrix<T>& newm,Vec2Unit rot){
        size_t probwidth=currm.Numspaces();
        size_t probheight=currm.Numspaces();

        Vec2i currmid=Vec2i(currm.Width()/2,currm.Height()/2);
        Vec2i newmid=Vec2i(newm.Width()/2,newm.Height()/2);
        size_t currBackrankpos=currm.Height()-1;
        size_t newBackrankpos=newm.Height()-1;

        size_t currBackOff=(currm.Width()-currm.NumBackrankSpace())/2;
        size_t newBackOff=(newm.Width()-newm.NumBackrankSpace())/2;
        Num mult=8.0f;
        for(size_t i=0;i<probheight;i++){
            Vec2i currpos=currm.posFromIth(i);

            if((size_t)currpos.y==currBackrankpos){
                currpos.x+=currBackOff;
            }
             currpos.x-=currmid.x;
            currpos.y-=currmid.y;
            Vec2 currposF=Vec2(currpos.x,currpos.y)*mult;
            for(size_t j=0;j<probheight;j++){
                    Vec2i newpos=newm.posFromIth(j);
                    if((size_t)newpos.y==newBackrankpos){
                        newpos.x+=newBackOff;
                    }
                    newpos.x-=newmid.x;
                    newpos.y-=newmid.y;
                    Vec2 newposF=Vec2(newpos.x,newpos.y)*mult;
                    newposF=Vec2Tool::rotatey(newposF,rot);
                    //static void rotate(Vec2& v1,Vec2 v2){



                    Vec2 offset=newposF-currposF;//Vec2i(newpos.x-currpos.x,newpos.y-currpos.y);
                    //Vec2i offseti=Vec2i(Numi(offset.x),Numi(offset.y));
                    Numi sqr=offset.x*offset.x+offset.y*offset.y;
                    prob[i*probwidth+j]=sqr;
                    //prob[i*probwidth+j]=j;
                    //cout<<sqr<<",";
            }
            //cout<<endl;
        }
    }



    template <class T>
    static void resize(Phalanx<T>& ph,size_t newWidth,Vec2Unit rot){
        if(!ph.valid()){
            return;
        }
        //size_t currWidth=ph.ranks.Width();
        //size_t currHeight=ph.ranks.Height();

//        if(newWidth==currWidth){
//            cout<<"no need to resize"<<endl;
//            return;
//        }

            size_t newHeight=MMatrixControl::getHeight(newWidth,ph.ranks.Numspaces());

            MMatrix<T> newM(newWidth,newHeight,ph.ranks.Numspaces());

            size_t probsize=ph.ranks.Numspaces();
            std::vector<int> k(probsize*probsize);
            //cout<<"constructing problem matrix..."<<endl;
            constructProblemMatrix(k,ph.ranks,newM,rot);


            int* r=&k[0];
            hungarian_problem_t prob;

            int** m = array_to_matrixx(r,probsize,probsize);
            hungarian_init(&prob,m,probsize,probsize,HUNGARIAN_MODE_MINIMIZE_COST);
            //cout<<"running hungarian algorithm..."<<endl;
            hungarian_solve(&prob);

            std::vector<int> ans=getAssignment(prob.assignment,probsize,probsize);

            for(size_t i=0;i<probsize;i++){
                size_t target=ans[i];
                //cout<<prob.a[i]<<endl;
                newM.at(target)=ph.ranks.at(i);
            }

            ph.ranks=newM;
            ph.phx.offsets=MMatrix<Vec2>(newWidth,newHeight,ph.phx.offsets.Numspaces());
            PhalanxContControl::update(ph);
            hungarian_free(&prob);
            freematrix(m,probsize,probsize);


            //PhalanxContControl::print(ph);
//
//            std::vector<T> freeBots=ph.members;
//
//
//            MMatrix<Vec2> newArrangeOffsets(newWidth,newHeight,ph.members.size());
//            PhalanxContControl::fillArrangeOffsets(newArrangeOffsets);
//            newArrangeOffsets.print();
//
//            MMatrix<T> newRanks(newWidth,newHeight,ph.members.size());
//            for(size_t i=0;i<newHeight;i++){
//                size_t num=newRanks.m.at(i).size();
//                size_t mid=(num-1)/2;
//                bool ss;
//                //Num vk=0.25f/4.0f;
//                Num vk=0.01f;
//                if(num%2==0){
//                   SimpyEvenSeries::Iterator ii=SimpyEvenSeries::create();
//                    for(size_t j=0;j<num;j++){
//                        Numi off=(Numi)mid+SimpyEvenSeries::Val(ii);
//                        cout<<"simpy="<<SimpyEvenSeries::Val(ii)<<"off="<<off<<endl;
//                        Vec2 point=newArrangeOffsets.m.at(i).at(off);
//                        point.y*=vk;
//                        T k=findClosest(freeBots,point);
//                        VectorTool::remove(freeBots,k);
//                        newRanks.m.at(i).at(off)=k;
//                        SimpyEvenSeries::increment(ii);
//                    }
//                }else{
//                    SimpySeries::Iterator ii=SimpySeries::create();
//                    for(size_t j=0;j<num;j++){
//                        Numi off=(Numi)mid+SimpySeries::Val(ii);
//                        cout<<"simpy="<<SimpySeries::Val(ii)<<"off="<<off<<endl;
//                        Vec2 point=newArrangeOffsets.m.at(i).at(off);
//                        point.y*=vk;
//                        T k=findClosest(freeBots,point);
//                        VectorTool::remove(freeBots,k);
//                        newRanks.m.at(i).at(off)=k;
//                        SimpySeries::increment(ii);
//
//                    }
//                }
//
//
//            }
//            if(!freeBots.empty()){
//                cout<<"PROBLEM"<<endl;
//            }
//            ph.ranks=newRanks;
//            PhalanxContControl::update(ph,newArrangeOffsets);
        }


//
//    template <class T>
//    static T findClosest(std::vector<T>& bots,Vec2 point){
//            T closest=nullptr;
//            Num closestDisSqr=10000000.0f;
//            for(size_t i=0;i<bots.size();i++){
//                Vec2 bo=bots[i]->arrangeOffset;
//                Vec2 offset=bo-point;
//                Num lengthSqr=Vec2Tool::getlengthSqr(offset);
//                if(lengthSqr<closestDisSqr){
//                    closest=bots[i];
//                    closestDisSqr=lengthSqr;
//                }
//            }
//            return closest;
//    }

};


class PhalanxDraw{
    static void draw(){

    }

};
class PhalanxAddRemoveControl{
    public:
    template <class T>
    static void addMember(Phalanx<T> ph,T a){
//        if(ph.ranks.back().size()>=ph.getDim().x){
//            std::vector<T> j;
//            j.push_back(a);
//            ph.ranks.push_back(j);
//        }else{
//            ph.ranks.back().push_back(a);
//        }
    }

    template <class T>
    static void removeMember(Phalanx<T>& ph,T a){
        //Vec2ui dim=ph.getDim();
        if(!ph.valid()){
            return;
        }
        //cout<<"remove farranger start"<<endl;
        Vec2ui ind=a->index;
        size_t backrankpos=ph.ranks.Height()-1;
        if(ind.y==backrankpos){
            ph.ranks.eraseFromBackRank(ind.x);
            ph.phx.offsets.eraseFromBackRank(ind.x);
        }else{
            size_t ii=ind.y;
            size_t nextii=ind.y+1;
            while(nextii<backrankpos){
                ph.ranks.at(ii,ind.x)=nullptr;
                ph.ranks.at(ii,ind.x)=ph.ranks.at(nextii,ind.x);
                ii++;
                nextii++;
            }
            //nextii--;


            size_t blep;
    //        if(ind.x>=ph.ranks.NumBackrankSpace()){
    //            blep=ph.ranks.NumBackrankSpace()-1;
    //        }else{
                blep=(ind.x/(Num)(ph.ranks.Width()-1))*(ph.ranks.NumBackrankSpace()-1);
            //}


            size_t diff=ph.ranks.Width()-ph.ranks.NumBackrankSpace();
            size_t closestAboveblep=diff/2+blep;
            //cout<<"diff="<<diff<<"diff/2"<<diff/2<<"blep="<<blep<<"closestAboveblep="<<closestAboveblep<<endl;


            Numi blepDiff=ind.x-closestAboveblep;
            //cout<<"blepDiff="<<blepDiff<<endl;


            Numi ss=sign(-blepDiff);
            Numi vvv=ind.x;
            for(Numi i=0;i<abs(blepDiff);i++){
                vvv=ind.x+i*ss;
                ph.ranks.at(ii,vvv)=ph.ranks.at(ii,vvv+ss);
            }




            ph.ranks.at(ii,vvv)=ph.ranks.at(nextii,blep);;
            ph.ranks.eraseFromBackRank(blep);
            ph.phx.offsets.eraseFromBackRank(blep);
        }
        PhalanxContControl::update(ph);
        //cout<<"remove farranger end"<<endl;
            //return;

//        if(ind.x>=ph.ranks.NumBackrankSpace()){
//            T a=ph.ranks.at(nextii,ph.ranks.NumBackrankSpace()-1);
//            ph.ranks[ii][ind.x]=a;
//            ph.ranks.eraseFromBackrank(ph.ranks.NumBackrankSpace()-1);
//            return;
//        }else{
//            size_t ii=ind.x;
//            size_t nextii=ind.x+1;
//
//            while(nextii<ph.ranks.back().size()){
//
//            }
//
//        }
//        NOT FINISHED
    }

};
