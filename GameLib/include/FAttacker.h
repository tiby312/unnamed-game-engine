// *** ADDED BY HEADER FIXUP ***
#include <algorithm>
#include <list>
#include <vector>
// *** END ***
#pragma once
#include "ObjectPool.h"
#include "Event.h"
#include "Moveable.h"
#include <unordered_map>
//class FAttackable{
//    public:
//    virtual ~FAttackable(){}
//	virtual Vec2& Pos() const=0;
//	virtual Vec2& Vel() const=0;
//	virtual Num& Radius() const=0;
//	virtual EventM& getDeathEvent()=0;
//	virtual FAttackableCont& fAttackableCont()=0;
//};

//class FFightableCont{
//    public:
//        FFightableCont():prey(nullptr){}
//    FAttackable* prey;
//};
//class FFightable{
//    public:
//    virtual ~FFightable(){}
//    virtual Vec2& Pos() const=0;
//	virtual Vec2& Vel() const=0;
//	virtual Num& Radius() const=0;
//	virtual FFightableCont& fFightableCont()=0;
//
//};


//T1=fighter
//T2=attackable




class FAttacker;
class Engagement{
    public:
        Engagement():enemy(),allies(),checked(){};
        Engagement(FAttacker& f,FAttackable* b,bool t):enemy(b),allies(),fa(&f),checked(t){}
        FAttackable* enemy;
        std::vector<FFightable*> allies;

        //size_t deathEventId;
        FAttacker* fa;
        bool checked;

};

//template <class T1,class T2>

class FAttacker{
    //static_assert()
    public:
    std::vector<Engagement*> engagements;
    unordered_map<FAttackable*,Engagement*> engmap;
    std::vector<FFightable*> freeAllies;
    FAttacker(std::vector<FFightable*> k):engagements(),engmap(),freeAllies(k){};
};


//class FAttackableManagle{
//    template <class TAttackable>
//    static void(TAttackable a){
//        std::function<void(Vec2)> func=[a](Vec2 a){
//            for(i:a->engagementsImTarget){
//                    destroyEngagement
//            }
//        }
//        a->registerDeathListener(func);
//    }
//
//};

class FAttackerManager{
    public:
    template <class T>
    static FAttacker createFAttacker(std::vector<T> bots){
        std::vector<FFightable*> blep(bots.size());
        for(size_t i=0;i<bots.size();i++){
            FFightable* f=bots[i];
            blep[i]=f;
        }

        return FAttacker(blep);;
    }
    //template <class T1,class T2>
    static void addAlly(FAttacker& fa,FFightable* l){
        fa.freeAllies.push_back(l);
    }

    //template <class T1,class T2>
    static std::vector<FFightable*> findAlliesAndPop(FAttacker& fa,FAttackable* enemy,size_t num){
        std::vector<FFightable*> al;
        //cout<<"free allies siz="<<fa.freeAllies.size()<<endl;
        FAttackable* e=enemy;
        std::sort(fa.freeAllies.begin(),fa.freeAllies.end(),
            [e](const FFightable* a, const FFightable*  b) -> bool
            {
                Vec2 j=a->Pos()-e->Pos();
                Vec2 k=b->Pos()-e->Pos();
                Num aa=Vec2Tool::getlengthSqr(j);
                Num bb=Vec2Tool::getlengthSqr(k);
                return aa > bb;
            }
        );
        for(size_t i=0;i<num;i++){
            if(!fa.freeAllies.empty()){
                al.push_back(fa.freeAllies.back());
                fa.freeAllies.pop_back();
            }else{
                break;
            }
        }
        return al;
    }
    static void addToEngagement(FAttacker& fa,Engagement* e,size_t num){
        std::vector<FFightable*> f=findAlliesAndPop(fa,e->enemy,num);
        for(auto i:f){
            //lockOn(fa,*i,e);
            i->prey=e->enemy;
            i->engagementImIn=e;
            e->allies.push_back(i);
            //VectorTool::remove(fa.freeAllies,i);
        }
    }

    static void createEngagement(FAttacker& fa,FAttackable* enemy){

        //cout<<"foundsss!"<<endl;
        Engagement* ee=new Engagement(fa,enemy,true);

        Engagement& eng=*ee;
        std::vector<FFightable*> founds=findAlliesAndPop(fa,enemy,3);

        for(auto k=founds.begin();k!=founds.end();k++){
            FFightable* l=*k;
            l->prey=enemy;
            eng.allies.push_back(l);
            l->engagementImIn=ee;
        }
        fa.engagements.push_back(ee);

        std::pair<FAttackable*,Engagement*> blep(enemy,ee);
        fa.engmap.insert(blep);
        //fa.engmap.emplace(enemy,ee);


        enemy->engagementsImTarget.push_back(ee);
    }

    //template <class T1,class T2>
    static void findEngagementsToStop(FAttacker& fa){
        std::vector<Engagement*> engagementsToStop;
        for(auto k=fa.engagements.begin();k!=fa.engagements.end();k++){
            Engagement* e=*k;
            if(e->checked==false){
                engagementsToStop.push_back(e);
            }else{
                e->checked=false;//reset
            }
        }
        while(!engagementsToStop.empty()){
            Engagement* e=engagementsToStop.back();
            destroyEngagementCont(e);
            engagementsToStop.pop_back();
        }
    }

    //template <class T1,class T2>


//    template <class T1,class T2>
//    static void deletePrey(FAttackable<T1,T2>& s){
//        while(!s.engagementsImTarget.empty()){
//            destroyEngagement(s.engagementsImTarget.back());
//            s.engagementsImTarget.pop_back();
//        }
//    }

//    template <class T1,class T2>
//    static void destroyEngagementAndDeregister(FAttacker<T1,T2>& fa,Engagement<T1,T2>* e){
//        e->enemy->deregisterDeathListener(e->deathEventId);
//        destroyEngagement(fa,e);
//    }
    //template <class T1,class T2>


    static void fightableDie(FAttacker& fa,FFightable& b){
        if(b.engagementImIn!=nullptr){
            Engagement* e=b.engagementImIn;
            b.engagementImIn=nullptr;
            b.prey=nullptr;
            VectorTool::remove(e->allies,&b);
        }else{
            VectorTool::remove(fa.freeAllies,&b);
        }
    }
    static void attackableDie(FAttackable& b){
        for(auto i:b.engagementsImTarget){
            FAttackerManager::destroyEngagement(i);
        }
        b.engagementsImTarget.clear();
    }
    static void destroyEngagementCont(Engagement* e){
        VectorTool::remove(e->enemy->engagementsImTarget,e);

        destroyEngagement(e);
    }
    static void destroyEngagement(Engagement* e){
        //cout<<"DESTROY! size="<<e->allies.size()<<endl;
        for(auto i:e->allies){
            e->fa->freeAllies.push_back(i);
        }

        FAttacker& fa=*e->fa;
        for(size_t i=0;i<e->allies.size();i++){
            FFightable* l=e->allies.at(i);
            l->engagementImIn=nullptr;
            l->prey=nullptr;
        }
        e->allies.clear();

        FAttackable* ss=e->enemy;
        fa.engmap.erase(ss);

        //e->faacker->engagements.erase()
        //std::vector<Engagement*>& k=e->fattacker->engagements;
        VectorTool::remove(fa.engagements,e);
        //k.erase(std::remove(k.begin(), k.end(), e), k.end());
        delete e;

    }
};
