#ifndef GAME_H
#define GAME_H


//#include "ActionImplementations.h"
//#include "Formation.h"
#include "FormationFind.h"
#include "Selector.h"
#include "Fps.h"
#include "Cannon.h"


class Game
{
    public:
        BotControl botcontrol;
        FormationControl formationcontrol;
        CannonControl cannoncontrol;
        Selector selector;
        sf::RenderWindow* window;
        Fps fps;

        Game(Num size,sf::RenderWindow& w):

            botcontrol(BoundedWorld(Vec3(0.0f,0.0f,size))),
            formationcontrol(BoundedWorld(Vec3(-200.0f,-200.0f,size+200.0f*2)),botcontrol),
            cannoncontrol(BoundedWorld(Vec3(0.0f,0.0f,size))),
            selector(new DefaultSelectableFinder(std::vector<Finder*>{{new FormationFinder(&formationcontrol)},new CannonFinder(&cannoncontrol)  }  )),
            window(&w),
            fps(){
            {
                StateSwitcher::init(&selector);
            }

        }
        void tick(){
            botcontrol.step();
            FormationControlManager::step(formationcontrol);
            cannoncontrol.step();
            selector.step();
            botcontrol.endstep();
            FormationControlManager::endstep(formationcontrol);

            botcontrol.draw(*window);
            FormationControlManager::draw(formationcontrol,*window);
            selector.draw();
            cannoncontrol.draw();
        }


    Soldier& createSoldier(PState ps,MState ms,ShipProperty sp,Team team,bool invinsible){
        Soldier& l=botcontrol.createSoldier(ps,ms,team,sp);

        if(invinsible==false){
            Soldier* lp=&l;
            //InsertableControl* ins=gamestuff->insertableControl;
            BotControl* bcc=&botcontrol;
            //InsertableControl* icc=this->ic;
            l.registerDeathListener(
                 [bcc,lp](Vec2 p){
                    bcc->killBot(*lp);

            }
            );
        }
        return l;
    }
};



#endif // GAME_H
