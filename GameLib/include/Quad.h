// *** ADDED BY HEADER FIXUP ***
#include <algorithm>
#include <iostream>
#include <vector>

// *** END ***
/*
 * Quad.h
 *
 *  Created on: May 24, 2013
 *      Author: ken

 This is a LOOSE quad tree array implementation. It's threadsafe so you can query concurrently.

 Cool things it does:
    -Query returns objects nearby as well as each of their offset, length, and lengthsqr (so you dont have to recompute)
    -No need to rebuild quadtree every game step. It checks to see if an object is still within the same node, and
    will only reinsert if its not. So if nothing is moving, nothing is changing in the quadtree.
 */
#pragma once
#include <array>
#include <mutex>
#include "MyLib.h"
#include "System.h"
#include "Moveable.h"
#include "VectorFixed.h"
/*
    QuadObj
    interface that objects you want to keep in quadtree must implement
    objects that implement this interface are responsible for holding on to the above Cont object
*/
//template <class T>


/*
        Cont
        Container that objects in the quadtree need to hold to keep track of which node they are in.
        This allows one to know where an object is in the quadtree instantly
    */

//    class Cont{
//        public:
//            Cont():nodeD(){}
//            NodeD nodeD;
//            //std::vector<Answer> answers;
//    };





//
//class QuadObj{
//public:
//    virtual ~QuadObj(){}
//	virtual Vec2& Pos() const=0;
//	virtual Vec2& Vel() const=0;
//	virtual Num& Mass() const=0;
//	virtual Num& Radius() const=0;
//	virtual Vec3& Bound() const=0;
//	virtual Cont& getCont()=0;
//};

//
// class ResidualData{
//    public:
//    ResidualData(Vec2 o,RadiusPair r):offset(o),radiuspair(r){}
//    Vec2 offset;
//    RadiusPair radiuspair;
//    ResidualData operator-()  {
//        ResidualData a;
//        a.offset=-offset;
//        a.radiuspair=radiuspair;
//        return a;
//
//   }
//    private:
//        ResidualData(){}
//
//};





constexpr size_t myIntPowConst(size_t base,size_t power){
    return power==1 ? base: base*myIntPowConst(base,power-1);
}

constexpr size_t calcNumberOfNodes(size_t level){
    return (myIntPowConst(4,level+1)-1)/3;
}

template <class T>
class QAnswer{
    //friend class Quad;
    public:
    T* b;
    SCircle::ResidualData r;
    QAnswer(T* bb,SCircle::ResidualData v):b(bb),r(v){}
    QAnswer(){}
};


template <class T,int MaxLevel=4,int MaxInQuad=128>
class Quad{
public:
    //friend class Answer;
    /*
        Answer
        a list of these is returned after each query.
        So, if you query the quad tree for objects nearby, it will return a list of Answers.
        Each answer has info on the object that is near, and carries along residual data like its length
        and offset from the querying object, so that whoever queried doesnt have to compute those again (saves performance).
    */



    Quad(Vec3 bound){
        root=NodeD(0,0);
        emptyNode=NodeD(12345,1234);
        for(size_t i=0;i<nodes.size();i++){
            nodes[i].leaf=true;
            nodes[i].bound=Vec3();
            nodes[i].loosebound=Vec3();

        }
        nodes[0].bound=bound;
        nodes[0].loosebound=MyTool::doubleBound(nodes[0].bound);
        setbounds(root);
    }

    bool insert(T& k){
        NodeD d=whereToInsert(root,k);
        if(d.index!=emptyNode.index){
            actuallyInsert(d,k);
            return true;
        }
        cout<<"insert fail"<<endl;
        return false;
    }



	bool updateQuadObj(T& o){
        bool a=fitsInAChild(o);
        bool b=!SCircle::contains(o.getShape(),getLooseBound(getNodeD(o)));
        bool c=!MyTool::contains(o.Pos(),getBound(getNodeD(o)));
        if(a||b||c){
            return reinsert(o);
        }
        return true;
    }



	void draw(){
		draw(root);
	}


    template<class TShape>
    void queryRadius(std::vector<QAnswer<T>>& ans,T* self,TShape shape) {
        query(ans,self,shape,root);
    }

    template <class TShape>
    void query(std::vector<QAnswer<T>>& ans,T* self,TShape shape,NodeD d) {
        if(! SCircle::intersects(getLooseBound(d),shape) /*SCircle::contains(shape,q.getLooseBound(d))*/){
            return;
        }

        for( auto bb  :getNode(d).objs){
            const SCircle bsq=bb->getShape();
            SCircle::ResidualData res;
            if(bb!=self&&SCircle::intersects(shape,bsq,res)){
                ans.push_back(QAnswer<T>(bb,res));
            }
        }
        if(isLeaf(d)){
            return;
        }
        std::array< NodeD,4> n(children(d));
        for(int i=0;i<4;i++){
            query(ans,self,shape,n[i]);
        }
    }

    struct Node{
        Node():bound(),loosebound(),objs(),leaf(){}
        Vec3 bound;
        Vec3 loosebound;
        //std::vector<T*> objs;
        VectorFixed<T*,MaxInQuad> objs;
        bool leaf;
    };

private:
     static NodeD getEmptyNodeD() {
            static NodeD bar;
            return bar;
    };

	std::array<Node,calcNumberOfNodes(MaxLevel)> nodes;
	NodeD root;
	NodeD emptyNode;
    Quad(){}

    static NodeD getNodeD(T& o){
        return o.nd;
    }

    static void setNodeD(T& o,NodeD d){
        o.nd=d;
    }
    Node& getNode(NodeD d){
        return nodes[d.index];
    }
    Vec3 getBound(NodeD d){
        return getNode(d).bound;
    }
    Vec3 getLooseBound(NodeD d){
        return getNode(d).loosebound;
    }
    void setbounds(const NodeD& d){
		if(d.level<MaxLevel){
			std::array< NodeD,4> c(children(d));
            Num midsize=getNode(d).bound.z/2.0f;

            Node nd=getNode(d);
            getNode(c[0]).bound=Vec3(nd.bound.x,nd.bound.y,midsize);
            getNode(c[1]).bound=Vec3(nd.bound.x+midsize,nd.bound.y,midsize);
            getNode(c[2]).bound=Vec3(nd.bound.x+midsize,nd.bound.y+midsize,midsize);
            getNode(c[3]).bound=Vec3(nd.bound.x,nd.bound.y+midsize,midsize);
            //Num midsize2=d.node->loosebound.z/2.0f;
            for(int i=0;i<4;i++){
                getNode(c[i]).loosebound=MyTool::doubleBound(getNode(c[i]).bound);
            }
            for(int i=0;i<4;i++){
                setbounds(c[i]);
            }
		}
	}

//    void link(NodeD n,T& b){
//
//	}

	bool isLeaf(const NodeD& d){
        return getNode(d).leaf;
	}
	void setLeaf( NodeD& d,bool t){
        getNode(d).leaf=t;
	}


    static std::array< NodeD,4> children(NodeD d) {
		size_t j=d.level+1;
        size_t z=4*d.index;
        std::array< NodeD,4> v{{
            NodeD{z+1,j},
            NodeD{z+2,j},
            NodeD{z+3,j},
            NodeD{z+4,j}}
        };
        return v;
    }
    static NodeD parent(NodeD d) {
		NodeD n;
        n=NodeD((d.index-1)/4,d.level-1     );
		return n;
	}


     bool fitsInAChild(const NodeD& d,const SCircle& shape){
        if(d.level<MaxLevel){
            std::array< NodeD,4> c(children(d));
            for(int i=0;i<4;i++){
                if(SCircle::contains(shape,getLooseBound(c[i]))){
                    //return c[i];
                    return true;
                }
            }
        }
        return false;
    }

	bool fitsInAChild(T& o){
        return fitsInAChild(getNodeD(o),o.getShape());
    }

    void draw(NodeD d){
	    if(d.level>MaxLevel){
            return;
	    }
	    Vec3 k=getBound(d);
		System::drawSquare(sf::Color{0,0,255,10},Vec3(k.x+10.0f,k.y+10.0f,k.z-20.0f));
		//cout<<d.level<<endl;
        Vec3 kj=getBound(d);
		for( auto &bb  :getNode(d).objs){
            //QuadObj* bb=*it;
            const SCircle& bsq=bb->getShape();


			//QuadObj* bbb=*it;
			System::drawLine(
					sf::Color{255,0,0,100},bsq.pos,
					Vec2(kj.x,kj.y));
//			System::drawLine(sf::Color{1,0,0},0.5,bsq.pos,
//						Vec2(kj.x+kj.z,
//						kj.y+kj.z));

		}
        if (isLeaf(d)){
			Vec2 kv(kj.x+10,kj.y+10);
			System::drawCircle(kv,5,sf::Color(0,1,0,255));
			return;
        }

        std::array< NodeD,4> b(children(d));
        for(int i=0;i<4;i++){
               // cout<<b[i].level<<endl;
            draw(b[i]);
        }


	}

    struct Tuple{
        T* o;
        NodeD targ;
    };
    void subdivide(NodeD d){
            if(d.level>=MaxLevel){
                return;
            }

            std::array< NodeD,4> c(children(d));
            Node& n=getNode(d);
            n.leaf=false;
            VectorFixed<Tuple,64> toRemove;
            for(int i=0;i<4;i++){
                getNode(c[i]).leaf=true;
            }
            for(int i=0;i<n.objs.size();i++){
                    T& o=*(n.objs[i]);

                    for(int i=0;i<4;i++){
                        Node& blep=getNode(c[i]);
                        if(SCircle::contains(o.getShape(),blep.loosebound)&&MyTool::contains(o.Pos(),blep.bound)){
                            if(!toRemove.push_back(        Tuple{&o,c[i]}       )){
                                cout<<"THIS CANNOT HAPPEN"<<endl;
                            }
                            break;
                        }
                    }

            }
            for(int i=0;i<toRemove.size();i++){
                Tuple t=toRemove[i];
                n.objs.remove(t.o);
                if(getNode(t.targ).objs.push_back(t.o)){
                    setNodeD(*(t.o),t.targ);
                }else{
                    setNodeD(*(t.o),emptyNode);
                }
            }

    }
    NodeD whereToInsert(NodeD d,T& o){
        //Node& n=getNode(d);

        if(!SCircle::contains(o.getShape(),getNode(d).loosebound)||!MyTool::contains(o.Pos(),getNode(d).bound)){
            return emptyNode;
        }

        if(getNode(d).objs.size()<3){
            return d;
        }else{
            if(SwapIfBiffer()){
                return d;
            }
        }

        if(getNode(d).leaf){
            subdivide(d);
        }

        std::array< NodeD,4> c(children(d));
        for(int i=0;i<4;i++){
            NodeD bla=whereToInsert(c[i],o);
            if  (  bla.index!=emptyNode.index  ){return bla;}
        }

        return d;

    }

    void setParentsToLeaves(NodeD d){
            if (d.index==0) return;
            NodeD par=parent(d);
            Node& n=getNode(par);
            if(n.leaf){
                n.leaf=false;
                setParentsToLeaves(par);
            }
    }
    void actuallyInsert(NodeD d,T& o){
        Node& n=getNode(d);
       if(n.objs.push_back(&o)){
            setNodeD(o,d);
            setParentsToLeaves(d);
        }else{
            setNodeD(o,NodeD());
        }
    }
    /*
    bool insert(NodeD d,T& o){
        //cout<<"d="<<d.index<<" max="<<nodes.size()-1<<endl;;
		if(!SCircle::contains(o.getShape(),getNode(d).loosebound)||!MyTool::contains(o.Pos(),getNode(d).bound)){
			return false;
		}

		if(  d.level>=MaxLevel|| !fitsInAChild(d,o.getShape())  ){
			//link(d,o);
            if(getNode(d).objs.push_back(&o)){
                setNodeD(o,d);
            }else{
                setNodeD(o,NodeD());
            }
			return true;
		}

        bool inserted=false;

        std::array< NodeD,4> c(children(d));
        for(int i=0;i<4;i++){
            if  (  insert(c[i],o)  ){inserted=true;break;}
        }


        if(inserted==true){
            if(isLeaf(d)==true){
                setLeaf(d,false);
            }
            return true;
        }else{
            return false;
        }
	}*/

//    bool hasChildren(NodeD d){
//        if(d.level>MaxLevel){
//            return false;
//        }
//        if(!getNode(d).objs.isEmpty()){
//            return true;
//        }
//        std::array< NodeD,4> c(children(d));
//        for(int i=0;i<4;i++){
//            if  (  hasChildren(c[i])  ){
//                return true;
//            }
//        }
//        return false;
//    }
    void setNewLeaf(NodeD d){
        if(d.level==0){
            return;
        }
        bool allLeaves=true;
        std::array< NodeD,4> c(children(d));
        for(int i=0;i<4;i++){
            if  (  getNode(c[i]).leaf==false  ){allLeaves=false;break;}
        }
        if(allLeaves){
            getNode(d).leaf=true;
        }
        NodeD nn=parent(d);
        setNewLeaf(nn);
        //return true;//TODO
    }
	bool reinsert(T& o){
		NodeD nn=getNodeD(o);
        //cout<<"REINSERTING"<<endl;
        if(!nn.equals(getEmptyNodeD())){
            //cout<<"welp"<<endl;
            if(getNode(nn).objs.remove(&o)){
                //cout<<"yo"<<endl;
                setNodeD(o,NodeD());

                if(getNode(nn).leaf&&getNode(nn).objs.isEmpty()){
                    //cout<<"PO"<<endl;
                        NodeD pp=parent(nn);
                        setNewLeaf(pp);
                        //cout<<"IM EMPTY"<<endl;
                        /*
                        if(!hasChildren(nn)){
                            cout<<"I HAVE CHILDREN"<<endl;
                            getNode(nn).leaf=true;
                        }*/

                }
            }else{
                cout<<"PROBLEM"<<endl;
            }
        }
        NodeD d=whereToInsert(root,o);
        if(d.index!=emptyNode.index){
            actuallyInsert(d,o);
            return true;
        }
		return false;
	}
};


