#pragma once

#include "MyLib.h"

//#include "Quad.h"
#include <array>
#include "Event.h"
//#include "PairCollideQuad.h"
//#include "Game.h"
//#include "System.h"

//#include "WorldObj.h"
//#include "B"
//#include "BotList.h"
//#include "FArranger.h"
//#include "FAttacker.h"
//#include <unordered_map>
#include "VectorFixed.h"
//#include "Modify.h"
/*
class CollideI{
    public:
    virtual ~CollideI(){}
    virtual void handleCollide(Quad* q)=0;
};*/


//class ModifyMoveable;

//
//class Stadium{
//    Vec2Unit ang;
//    Vec2 length;
//    Vec2 radius;
//
//};



class Moveable{
public:
	template<class T>friend class World;
	friend class UnboundedW;
	friend class BoundedW;

    //friend class BotControlManager;
    template<class T1,class T2,class TWorld,int maxlevel>
    friend class ControlP;
    friend class Formation;

	Moveable(PState s,MState m):
            shape(m.pos,s.radius),
            ang(m.ang),
			mass(computeMass(s.radius)),
			inverseMass(1/mass),
			vel(Vec2(0,0)),
			speedSqr(Vec2Tool::getlengthSqr(vel)),
			accumLAcc(0,0),
            moment(computeMoment(mass,s.radius)),
            inverseMoment(1/moment),
            angv(m.angv),
            accumAAcc(){}
    static Num computeMoment(Num mass,Num radius){
        return mass*(radius*radius)/2.0f;
    }
	static Num computeMass(Num r){
		return r*0.001f;
	}
	SCircle getShape() const{
	    return shape;
	}
    Num AngV() const{
        return angv;
    }
	Vec2 Pos() const{
		return shape.pos;
	}
	Num SpeedSqr() const{
		return speedSqr;
	}
	Vec2 Vel() const{
		return vel;
	}
	Num iMass() const{
		return inverseMass;
	}
	Num Mass() const{
		return mass;
	}

//	Vec3 Bound() const{
//		return shape.bound;
//	}
    Num Moment() const{
        return moment;
    }
    Num iMoment() const{
        return inverseMoment;
    }
    Vec2 AccumLAcc() const{
        return accumLAcc;
    }
    Num AccumAAcc() const{
        return accumAAcc;
    }
    Vec2Unit Ang() const{
        return ang;
    }

    void addToAccRot(Num force){
        accumAAcc+=force;
	}
	void addToAcc(Vec2 force){
		 accumLAcc+=force;
	}

	void setPos(Vec2 p){
	    shape.pos=p;
	}
	void setAng(Vec2Unit a){
	    ang=a;
	}
	void setRadius(Num r){
	    shape.radius=r;
	}


protected:

private:

	SCircle shape;
	Vec2Unit ang;
	//RadiusPair radiuspair;
	Num mass;
	Num inverseMass;
	//Vec2 pos;
	Vec2 vel;
	Num speedSqr;
	//Vec3 bound;
	Vec2 accumLAcc;

	Num moment;
	Num inverseMoment;
	Num angv;
	Num accumAAcc;

};

/*
class BrainMoveable:public Moveable{
    public:
    BrainMoveable(PState s,MState m):Moveable(s,m),brain(){
        Brain* b=new DummyBrain();
        connectTo(b);
    }
    Brain* brain;
    void connectTo(Brain* b){
        b->connectTo(this);
        delete brain;
        brain=b;
    }

};
*/


//Team Team::neutral={Color{0.5,0.5,0.5},0};

//uint32_t TeamMoveable::idcounter;





//template <class T>
//class FArranger;
//
//template <class T>
//class Phalanx;




class ShipProperty{
    public:
    Num linthrust;
    Num angthrust;
};
//class ModifyShip;
//class AwakeControl;

class Ship:public Moveable{
    template<class T> friend class World;
	//friend ModifyShip;
	//friend AwakeControl;
public:
	//Ship():Moveable({5.0f,5.0f},4){};
	//Ship(State s);
	//Ship(PState s,MState m,Team team,ShipProperty sp);
	Ship(PState s,MState m,ShipProperty sp):

	//Entity(s,m,linthrust),
	Moveable(s,m),
	lthrust(sp.linthrust),
	athrust(sp.angthrust),
	lacc(lthrust* (iMass())),
	aacc(athrust/Moment()),
	timeNeededToRot(2.0f*sqrt((2*(Z_PI_4/2.0f))/Aacc())),
	goingForward(0),
	goingLeft(0),
	goingRight(0)
	//positiveAwakeness(1.0f)
	{
	    if(sp.linthrust<=0||sp.angthrust<=0){
            cout<<"ship has no thrust??"<<endl;
	    }
	//cout<<aacc<<endl;
    }
//virtual ~Ship(){};

	//~Ship(){};


    Num rotTime() const{
        return timeNeededToRot;
    }

/*
State Ship::getState(){
	return state;
}*/


    Num Athrust() const{
        return athrust;
    }
    Num Aacc() const{
        return aacc;
    }
	Num Lacc() const{
	    return lacc;
	}

	//Vec2 arrive(MState target,Num rottime) const;
	//Vec2 arrive(MState target) const;
	//void step();
	//void endstep();
//	void draw(){
//        System::drawBot(Pos(),Radius(),Ang(),getTeam().color);
//	}


	//void arrive(MState target);

	//void stepQuad(Quad* q);
	//Vec2 collisionAvoid(Answer a) const;
	//virtual void stepQuad(Quad* q);
    Num getTimeNeededToStopNoRot(){
        /*
        vf=vi+a*t;
        0=vi+a*t;
        -vi=a*t;
        t=vi/a;
        */
        return sqrt(SpeedSqr())/Lacc();

    }

    Num getTimeNeededToRot(){
        return timeNeededToRot;
    }
    Num getTimeNeededToStopRot(){
        return getTimeNeededToStopNoRot()+getTimeNeededToRot();
    }
    unsigned char GoingForward(){
        return goingForward;
    }
    unsigned char GoingLeft(){
        return goingLeft;
    }
    unsigned char GoingRight(){
        return goingRight;
    }

private:
    Num lthrust;
    Num athrust;
	Num lacc;
	Num aacc;
	Num timeNeededToRot;
	unsigned char goingForward;
	unsigned char goingLeft;
	unsigned char goingRight;
	//Num timeNeededToStopNoRot;
    //Num awakeness;
    //Num positiveAwakeness;

};




class Team{
    public:
        Team(sf::Color c,Numi i):color(c),id(i){}
       sf::Color color;
    Numi id;
    //inline bool operator==(const Team& lhs, const Team& rhs){ return lhs.id==rhs.id }
    static Team neutral;
    bool operator==(const Team& rhs) const { return id==rhs.id;}
    bool operator!=(const Team& rhs) const { return id!=rhs.id;}
};

class Bot:public Ship{
        friend class BotControl;
private:

    public:
    Bot(PState s,MState m,Team t,Numi id,ShipProperty sp):Ship(s,m,sp),
   desiredacc(0.0f,0.0f),restingUnitVec(0.0f,-1.0f),maxsteerspeed(100.0f),m_team(t),m_id(id){};
	Vec2 desiredacc;
    Vec2 restingUnitVec;
    Num maxsteerspeed;
	//void step();
	//void endstep();
	//void draw();
	void setRestingUnitVec(Vec2 a){
	    restingUnitVec=a;
	}
	Num& MaxSteerSpeed() const{
	    return const_cast<Num&>(maxsteerspeed);
	}
	Vec2& getdesiredAcc() const{
        return const_cast<Vec2&>(desiredacc);
	}
	void resetDesiredAcc(){
	    desiredacc=Vec2();

	}

    uint32_t& getID() const{
        return const_cast<uint32_t&>(m_id);
    }
        bool operator<(const Bot& rhs) const { return m_id<rhs.m_id;}
        /*
        inline bool operator==(const EventM& lhs, const EventM& rhs){ return lhs.id==rhs.id }

        inline bool operator!=(const EventM& lhs, const EventM& rhs){return !operator==(lhs,rhs);}
        inline bool operator< (const EventM& lhs, const EventM& rhs){ lhs.id<rhs.id }
        inline bool operator> (const EventM& lhs, const EventM& rhs){return  operator< (rhs,lhs);}
        inline bool operator<=(const EventM& lhs, const EventM& rhs){return !operator> (lhs,rhs);}
        inline bool operator>=(const EventM& lhs, const EventM& rhs){return !operator< (lhs,rhs);}
        */
        Team& getTeam() const{
            return const_cast<Team&>(m_team);
        }
    private:
        Team m_team;
        uint32_t m_id;

};
//
//class BotControl;
//class TeamMoveable:public Bot{
//    public:
//
//    TeamMoveable(PState s,MState m,Team t,Numi id,ShipProperty sp):Bot(s,m,sp),m_team(t),m_id(id){
//        //idcounter++;
//    }
//
//};
//uint32_t TeamMoveable::idcounter=0;


//class GameQuad;


class NodeD{
    //static NodeD empty;
    public:
    NodeD():index(-1),level(-1){}
    NodeD(size_t i,size_t ii):index(i),level(ii){};
    size_t index;
    size_t level;
    bool equals(NodeD d){
        return d.index==index&&d.level==level;
    }
};






class RepelAnswer{
    public:
    Vec2 force;
    Num healthloss;
    Num rotforce;
    //Num intensity;
    RepelAnswer():force(Vec2(0,0)),healthloss(0),rotforce(0){}
    RepelAnswer(Vec2 a,Num b,Num c):force(a),healthloss(b),rotforce(c){}

    RepelAnswer& operator+=(const RepelAnswer& rhs)
    {
    // actual addition of rhs to *this
        force+=rhs.force;
        healthloss+=rhs.healthloss;
        rotforce+=rhs.rotforce;
        //intensity+=rhs.intensity;
        return *this;
    }


};

inline RepelAnswer operator+(RepelAnswer lhs, const RepelAnswer& rhs)
{
  lhs += rhs;
  return lhs;
}


class PairCollidable:public Bot{
     template <class T,int k,int j>
    friend class Quad;

    template <class T,class A,int MaxLevel>
    friend class PairCollideQuad;
    public:
    PairCollidable(PState s,MState m,Team team,Num id,ShipProperty sp):
        Bot(s,m,team,id,sp),storedAnswer(),handled(){
            //handledMap.reserve(256);
        }
    RepelAnswer getStoredAnswer(){
        return storedAnswer;
    }
    private:
    //used by paircollidequad
    RepelAnswer storedAnswer;
    //unordered_map<uint32_t,bool> handledMap;
    VectorFixed<uint32_t,32> handled;

    //used by qyad
     NodeD nd;

};



class HealthMoveable:public PairCollidable{
    friend class HealthControl;
    public:
    HealthMoveable(PState s,MState m,Team t,Numi id,ShipProperty sp):PairCollidable(s,m,t,id,sp),health(0.01f),deathEvent(),dead(false){}


    size_t registerDeathListener(std::function<void(Vec2)> l){
        return deathEvent.registerListener(l,false);
    }
    void deregisterDeathListener(size_t id){
        deathEvent.degisterListener(id);
    }

    EventM<Vec2>& getDeathEvent(){
        return deathEvent;
    }

    Num Health(){
        return health;
    }

    private:

        Num health;
        EventM<Vec2> deathEvent;
        bool dead;

};


//class Engagement;
//class FAttacker;


class Engagement;
class FAttackable:public HealthMoveable{

    public:
    friend class FAttackerManager;
    friend class BotControlManager;
    friend class BotControl;
    FAttackable(PState s,MState m,Team t,Numi id,ShipProperty sp):HealthMoveable(s,m,t,id,sp){

    }
    private:
    std::vector<Engagement*> engagementsImTarget;
};



class FFightable:public FAttackable{
    public:
    //friend FAttacker;
    friend class FAttackerManager;
    FFightable(PState s,MState m,Team t,Numi id,ShipProperty sp):FAttackable(s,m,t,id,sp),prey(nullptr){}
    FAttackable& getPrey() throw (int){
        if(prey==nullptr){
            throw 0;
        }
        return const_cast<FAttackable&>(*prey);
    }
    private:
        FAttackable* prey;
        Engagement* engagementImIn;
};
class Soldier:public FFightable{
    friend class PhalanxContControl;
    friend class PhalanxResizeControl;
    friend class PhalanxAddRemoveControl;
//    template <class T>
//    friend class PhalanxCont;
//
//    template <class T>
//    friend class Phalanx;
    public:
    Soldier(PState s,MState m,Team team,Num id,ShipProperty sp):
        FFightable(s,m,team,id,sp),offset(),index(){}
    private:
        Vec2 offset;
        Vec2 arrangeOffset;
        Vec2ui index;

};

