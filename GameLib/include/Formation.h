// *** ADDED BY HEADER FIXUP ***
#include <algorithm>
#include <iostream>
// *** END ***
#pragma once

#include "FArranger.h"
#include "FAttacker.h"
#include "Bot.h"
//#include "Ship.h"
//#include "Phalanx.h"
#include "MyLib.h"
#include <vector>
#include "World.h"
#include "Selector.h"
//#include "BotControl.h"



class PhalanxRotPosControl{
    public:
    static MState getAbsTarget(Vec2 pos,Vec2 vel,Vec2Unit rot,Vec2 offs){
        return MState(pos+getrotatedoffset(rot,offs),vel);
    }
    static Vec2 getrotatedoffset(Vec2Unit rot,Vec2 offs){
        Vec2 rotated(offs.x*rot.x-offs.y*rot.y,offs.x*rot.y+offs.y*rot.x);
        return rotated;
    }


};

class Formation:public Soldier{
    friend class FormationControlManager;
    //friend FArranger;
    public:


    Formation(Soldier* leader,const std::vector<Soldier*> bots,MState m,Team t,Num id,Num spacing):
        Soldier(PState(40),m,t,id,ShipProperty{0.0004f,0.008f}),
        desiredUnitRot(m.ang),
        farranger(PhalanxContControl::createPhalanx(leader,bots,spacing)),
        fattacker(FAttackerManager::createFAttacker(bots)),
        shape(),
        target(m),

        attackradius(0),
        reforming(false),
        smallradius(){


        }


//    MState getPosTarget(Bot& b){
//        return getAbsTarget(getOffset(b));
//    }
//    void addMember(Bot& l){
//        FAttackerManager::addAlly(fattacker,&l);
//        PhalanxContControl::addMember(farranger,&l);
//        recomputeRadii();
//        Bot* ll=&l;
//        l.registerDeathListener(  [this,ll](Vec2 k){
//            removeMember(*ll);
//        });
//    }


    Soldier& getLeader()throw (int){
        if(farranger.getLeader()==nullptr){
            throw 0;
        }
        return *farranger.getLeader();
    }
    MState getTarget(){
        return target;
    }
    Num SmallRadius(){
        return smallradius;
    }
    void resizeAndRot(size_t w,Vec2Unit k){
        setRotationTarget((Vec2Unit)k);




        Vec2Unit offang=k;


        Vec2Tool::rotateBackwards(offang,Ang());
        //offang.y=-offang.y;
        PhalanxResizeControl::resize(farranger,w,offang);
        //recomputeRadii();
        setAng(k);
    }
    void setMoveTarget(Vec2 poss){
        target.pos=poss;//Vec2(256.0f,256.0f);
    }
    void flip(){
        //this->setAng(Vec2Tool::Flip180(Ang()));
        //farranger.flip();
    }

    void setRotationTarget(Vec2Unit poss){
        desiredUnitRot=poss;

        //Num innerAngle=Vec2Tool::innerAngle(Vec2Tool::innerProduct(desiredUnitRot,Ang()));
//        cout<<"innerAngle="<<innerAngle;
//        if(innerAngle>=Z_PI_2){
//            flip();
//        }

    }

    Stadium getStadium(){
        return Stadium(shape,Pos(),Ang());
    }
    Stadium getAttackStadium(){
        StadiumShape sh=shape;
        sh.radius=attackradius;
        return Stadium(sh,Pos(),Ang());
    }
//    bool static intersect(Formation& f1,Formation& f2,Num& dis){
//        auto s1=Stadium(f1.shape,f1.Pos(),f1.Ang());
//         auto s2=Stadium(f2.shape,f2.Pos(),f2.Ang());
//         return Stadium::intersect(s1,s2,dis);
//    }


    void avoidLeader(std::vector<Vec2>& ans,Vec2& leaderavoid,BoundedWorld& w){
        if(farranger.ranks.Numspaces()==0){
            return;
        }
        for(size_t i=0;i<farranger.ranks.Numspaces();i++){
            Bot* b=farranger.ranks.at(i);
            std::array<Vec2,2> ansy=w.avoid(b,farranger.getLeader());
            //ansy[1]/20.0f;


            ansy[0]*=200.0f;
            //ansy[1]*=100.0f;
            ans[i]=ansy[0];
            leaderavoid+=ansy[1];
        }
        leaderavoid/=farranger.ranks.Numspaces()*200.0f;
    }
    void moveMembers(BoundedWorld& w){
        std::vector<Vec2> avoidans(farranger.ranks.Numspaces());
        //cout<<"reforming="<<reforming<<endl;
        Vec2 leaderavoid(0.0f,0.0f);
        if(reforming==true){
            avoidLeader(avoidans,leaderavoid,w);
        }

        Vec2 leaderoff=getAbsTarget(farranger.phx.leaderoffset).pos-farranger.leader->Pos();
        Num lensqr=Vec2Tool::getlengthSqr(leaderoff);
        if(lensqr<=15.0f*15.0f){
            reforming=false;
        }else{
            reforming=true;
        }

          Soldier& z=*farranger.leader;
          Vec2 k=w.arrive( z,getAbsTarget(farranger.phx.leaderoffset));
          //cout<<leaderavoid<<endl;
          w.rotateAndGoForward(z,k+leaderavoid);


        for(size_t i=0;i<farranger.ranks.Numspaces();i++){
            MState kz=getAbsTarget(farranger.phx.offsets.at(i));
            //cout<<kz.pos<<kz.vel<<",";
            Soldier* l=farranger.ranks.at(i);
            Vec2 k=w.arrive( *l, kz);

            try{
                FAttackable& prey=l->getPrey();
                //cout<<prey.Pos()<<endl;
                Vec2 offset=prey.Pos()-l->Pos();
                Num maxdis=50.0f;
                Num dis=sqrt(Vec2Tool::getlengthSqr(offset));
                Num factor=3.0f*std::max(0.0f,std::min(1.0f,(maxdis-dis)/maxdis));
                k+=factor*w.arrive(*l,MState(prey.Pos(),prey.Vel()));
            }catch(int){};
            w.rotateAndGoForward(*l,k+avoidans[i]);
        }



//            //botcontrol->step(z);
//            //gamestuff->botquad().step(z);
//            Vec2 k=fc.world->arrive( z,f.getPosTarget(z) );
//            //cout<<k<<endl;
//            //world->rotateAndGoForward(z,k);
//            fc.world->rotateAndGoForward(z,k);
//            //cout<<"wee"<<endl;
    }
    MState getAbsTarget(Vec2 offs) const{
        return PhalanxRotPosControl::getAbsTarget(Pos(),Vel(),Ang(),offs);

    }
    Vec2 getrotatedoffset(Vec2 offs) const{
        return PhalanxRotPosControl::getrotatedoffset(Ang(),offs);
    }


    void forEveryMemberAndLeader(const std::function<void(Soldier*)> f){
        farranger.forEveryMemberAndLeader(f);
    }


    void forEveryMember( const std::function<void(Bot*)> f){
        farranger.forEveryMember(f);

    }

    Vec2 getOffset(Soldier& b){
        return PhalanxContControl::getOffset(farranger,&b);
        //return farranger.getOffset(&b);
    }
    Vec2Unit getDesiredUnitRot(){
        return desiredUnitRot;
    }

    Num& AttackRadius() const{
        return const_cast<Num&>(attackradius);
    }
    Num AttackRadiusSqr() {
        return (attackradius*attackradius);
    }


        Vec2Unit desiredUnitRot;
        Phalanx<Soldier*> farranger;
        FAttacker fattacker;
        StadiumShape shape;
    private:


        MState target;
        Num attackradius;

        //Num attackradiussqr;
        //Num collideradius;
        //Num collideradiussqr;
        bool reforming;
        Num smallradius;

};



