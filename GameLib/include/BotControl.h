// *** ADDED BY HEADER FIXUP ***
#include <vector>
#include <SFML/Graphics/Vertex.hpp>
#include <SFML/Graphics/Shader.hpp>
// *** END ***
#pragma once
#include "BotDraw.h"
#include "Parallelizer.h"
#include "HealthControl.h"
#include "FAttacker.h"
#include "ControlP.h"

class BounceAction{
    public:
    std::array<RepelAnswer,2> collide(Soldier& th,Soldier& avoidobj,SCircle::ResidualData a){
        //SolidMoveable& th=dynamic_cast<SolidMoveable&>(th2);
        //SolidMoveable& avoidobj=dynamic_cast<SolidMoveable&>(avoidobj2);
        //SolidMoveable* th=dynamic_cast<SolidMoveable*>(th2);
        //SolidMoveable* avoidobj=dynamic_cast<SolidMoveable*>(avoidobj2);

        if(a.radius<=0.000f){
            uint32_t k=(th.getID())^(avoidobj.getID());
            Num s(k);
            Num co=cos(s+System::steprandom());
            Num si=sin(s+System::steprandom());

            RepelAnswer ans1=RepelAnswer{Vec2(co,si)*0.001f,0.0f,0.0f};
            RepelAnswer ans2=RepelAnswer{Vec2(-co,-si)*.001f,0.0,0.0f};
            std::array<RepelAnswer, 2> answer = { {ans1,ans2} };
            return answer;
        }
        Vec2 norm=a.offset/a.radius;



        /*
        Num j=t-a.length;
        if(j<=0){
            RepelAnswer ans1=RepelAnswer();
            RepelAnswer ans2=RepelAnswer();
            std::array<RepelAnswer, 2> answer = { {ans1,ans2} };
            return answer;
        }*/



        Num t=avoidobj.getShape().Radius()+th.getShape().Radius();
        Num intensity=(t-a.radius);
        intensity*=intensity;
        intensity*=0.00003f;

//        Num t=avoidobj.Radius()+th.Radius();
//        Num dis=std::min(1.0f,a.radiuspair.radius/t) ;
//
//        dis=std::max(dis,0.1f);
//        dis*=dis;
//        Num intensity=1.0f/dis;//1.0f-(a.radiuspair.radiusSqr/(t*t));
//        intensity*=0.0001f;

        Num healthloss;
        if(th.getTeam()!=avoidobj.getTeam()&&th.getTeam()!=Team::neutral&&avoidobj.getTeam()!=Team::neutral){
            healthloss=intensity;
        }else{
            healthloss=0;
        }

        //Num intensity=(t*t)/(a.lengthSqr);

        //intensity*=((th.Bouncyness()+avoidobj.Bouncyness())/2.0f);//0.01f;

        //Num ll=-(  a.length-(t));
        /*
        Num ll=t-a.length;
        Num intensity=(ll/t);
        if(intensity<=0){
            RepelAnswer ans1=RepelAnswer();
            RepelAnswer ans2=RepelAnswer();
            std::array<RepelAnswer, 2> answer = { {ans1,ans2} };
            return answer;
        }*/

        /*
        Vec2 relativeV=avoidobj->Vel()-th->Vel();
        Num relativeVSqr=getlengthSqr(relativeV);
        Num relativeVMag=sqrt(relativeVSqr);

        //intensity=th->Mass()+avoidobj->Mass();
        Num k=1/(avoidobj->Mass()+th->Mass());
        intensity=intensity*intensity;
        intensity=intensity/k;
        intensity*=((th->Bouncyness()+avoidobj->Bouncyness())/2.0f);//0.01f;
        */
        //Num jj=(1+relativeVMag)/(th->iMass()+avoidobj->iMass());
        //intensity*jj;
        //intensity*=0.1f;
        //System::drawArrow(Color{0,0.5,0.5},intensity,th->Pos(),avoidobj->Pos());
        //cout<<"in
        //Num intensitynegForce=intensity*-1.0f;


        //cout<<intensity<<endl;


        //Dampening


        Vec2 relativeV=avoidobj.Vel()-th.Vel();
        Num relativeVSqr=Vec2Tool::getlengthSqr(relativeV);//getlengthSqr(relativeV);
        Num relativeVMag=sqrt(relativeVSqr);
        Vec2 relativeVNorm=relativeV/relativeVMag;
        Num d=Vec2Tool::innerProduct(a.offset,relativeVNorm);
        Num time=-d/relativeVMag;//not sure why i have to negate it
        if(time<0){
            intensity*=0.2f;
        }

        //size_t jjj=(size_t)min(255.0f,255.0f*intensity*20.0f);
        //System::drawLine(sf::Color{255,0,0,jjj},th.Pos(),	avoidobj.Pos()+Vec2(4,4));

        if(intensity<0){
            cout<<"no"<<endl;
        }
        Vec2 comp=norm*(intensity*-1.0f);

        if(isnan(comp.x)){
            comp=norm*1000.0f;
            cout<<"NOOOOOOOOOOOOOOOO"<<endl;
        }

        //std::array<Vec2,2> repel(SolidMoveable* th,SolidMoveable* avoidobj,AnswerBag a);
        //return std::array<Vec2,2>
        //cout<<dot<<endl;
        Num rotforce=0.0f;
        /*
        Num rotforce=intensity*relativeVMag*d;
        if(isnan(d)){
            //cout<<"NOOOOOOOOOOOO"<<endl;
        }
        if(isnan(rotforce)){
            rotforce=0.0f;

        }*/


        RepelAnswer ans1=RepelAnswer{comp*th.iMass(),healthloss,rotforce*th.iMoment()};
        RepelAnswer ans2=RepelAnswer{-comp*avoidobj.iMass(),healthloss,-rotforce*avoidobj.iMoment()};


        std::array<RepelAnswer, 2> answer = { {ans1,ans2} };
        return answer;
    }
};




/*
template<class T>
class Container{

    public:
        std::vector<T> obj;

    Num idcounter=0;
    Container():objs(){

        objs.reserve(7000);
        //points.reserve(3000*3);
    }
    //PState p,Vec2 pos,Team team,ShipProperty sp
    T& create(T2 k){
        T b=T(k,idcounter);
        objs.push_back(b);
        idcounter++;
        //cout<<idcounter<<" bots"<<endl;
        return objs.back();
    }
};*/



class BotControl:public ControlP<Soldier,BounceAction,BoundedWorld,4>{
public:
    //friend Bo;
    BotDraw botdraw;
    HealthControl healthcontrol;

    BotControl(BoundedWorld w):ControlP(w),botdraw(3000),healthcontrol(){}
    void setTeam(Soldier& b,Team t){
        b.m_team=t;
    }
    void killBot(Soldier& b){
        FAttackerManager::attackableDie(b);
        setTeam(b,Team::neutral);
        world.kill(b);
    }
    Soldier& createSoldier(PState p,MState ms,Team team,ShipProperty sp){
        Soldier& b=*new Soldier(p,ms,team,getAnId(),sp);//BotContainer::createBot(p,pos,team,sp);
        addObj(b);
        return b;
    }
    void step(){


    //        std::function<void(size_t,Bot*,BotControl*)> func=[](size_t i,Bot* b,BotControl* bc){
    //            bc->pairCollideQuad.queryRadius(bc->queries[i],bc->livebots[i],bc->livebots[i]->getSCircle());
    //        };
    //        parallelizer->doParallel(livebots,func,this);

       ControlP<Soldier,BounceAction,BoundedWorld,4>::step();
        for(auto i:objs){
            Soldier& l=*i;
            RepelAnswer ans=l.getStoredAnswer();

            l.addToAcc(ans.force);
            l.addToAccRot(ans.rotforce);
            pairCollideQuad.resetStoredAnswer(l);
            healthcontrol.handleHealthLoss(l,ans.healthloss);
        }
    }
    void endstep(){

        for(size_t i=0;i<objs.size();i++){
            Soldier& l=*objs[i];
            l.resetDesiredAcc();
            world.applyMove(l);

            botdraw.setPoints(i,l);
            pairCollideQuad.endstep(l);
        }

        //pairCollideQuad.update();
        updatePairCollideQuad();
        healthcontrol.fireDeathEvents();
    }
    void draw(sf::RenderWindow& w){
        pairCollideQuad.draw();
        botdraw.draw(w);


    }

};


