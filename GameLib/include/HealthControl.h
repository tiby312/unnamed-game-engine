// *** ADDED BY HEADER FIXUP ***
#include <algorithm>
#include <list>
// *** END ***
#pragma once

/*
Purpose of this class is because healthmoveable has private deathevent obj that we dont want to expose.
*/
class HealthControl{
    std::vector<HealthMoveable*> deathline;
    public:
        HealthControl():deathline(){}
    void fireDeathEvents(){
        if(deathline.empty()){
            return;
        }
        std::sort(deathline.begin(),deathline.end(),
            [](const HealthMoveable* a, const HealthMoveable*  b) -> bool
            {
                return a->getID() > b->getID();
            }
        );
        for( auto & i : deathline){
            Vec2 pp=i->Pos();
            i->deathEvent.fire(pp);
        }
        deathline.clear();
    }
    void handleHealthLoss(HealthMoveable& b,Num healthloss){

        if(b.health<0.0f&&b.dead==false){
            deathline.push_back(&b);
            b.dead=true;
            //cout<<"someone about to die"<<endl;
        }else{
            b.health-=healthloss;
            //cout<<b.health<<endl;;
        }
    }

};
