#pragma once
#include "MyLib.h"
#include "Moveable.h"
class BotDraw{
    size_t size;
    sf::VertexArray points;
    sf::VertexArray cpoints;
    sf::VertexArray backthrust;
    sf::VertexArray leftthrust;
    sf::VertexArray rightthrust;

    //std::array<sf::VertexArray,
    //sf::Shader shader;

    public:
    BotDraw(size_t si):size(si),points(sf::Triangles,si*3),cpoints(sf::Quads,si*4),backthrust(sf::Triangles,si*3),leftthrust(sf::Triangles,si*3),rightthrust(sf::Triangles,si*3){
//        if (!sf::Shader::isAvailable())
//        {
//            cout<<"u dont have shaders"<<endl;
//        }
//        if (!shader.loadFromFile("vertexshader.vert","fragshader.frag")){
//                cout<<"could not load shader"<<endl;
//        }
    }
    void incrementsize(){
        size++;
        //points.resize(size);
        //cpoints.resize(size);
    }
    void setPoints(size_t i,Soldier& b){
        //System::drawSquare(sf::Color(0,255,0,100),b.Bound());
        const Num z=1/sqrtf(2.0f);
        Vec2 pos=b.Pos();
        Vec2Unit ang=b.Ang();
        Num rad=b.getShape().Radius();
        Vec2 perpAng(ang.y,-ang.x);


        {
            Numi pp=3*i;

            Num thrustw=rad/1.5;;
            Num thrustwhalf=thrustw/2.0f;
            Num thrustheight=rad/2.0f;
            Vec2 p1=pos-ang*z*rad-perpAng*z*rad;
            Vec2 p2=pos-ang*z*rad-(perpAng*z*rad-perpAng*thrustw);
            Vec2 p3=pos-ang*z*rad-(perpAng*z*rad-perpAng*thrustwhalf)-ang*thrustheight;

            unsigned char cc=b.GoingLeft();
            leftthrust[pp]=sf::Vertex(p1,sf::Color(255,0,0,cc));
            leftthrust[pp+1]=sf::Vertex(p2,sf::Color(255,0,0,cc));
            leftthrust[pp+2]=sf::Vertex(p3,sf::Color(255,0,0,cc));
        }
        {
            Numi pp=3*i;

            Num thrustw=rad/1.5;;
            Num thrustwhalf=thrustw/2.0f;
            Num thrustheight=rad/2.0f;
            Vec2 p1=pos-ang*z*rad+perpAng*z*rad;
            Vec2 p2=pos-ang*z*rad+(perpAng*z*rad-perpAng*thrustw);
            Vec2 p3=pos-ang*z*rad+(perpAng*z*rad-perpAng*thrustwhalf)-ang*thrustheight;

            unsigned char cc=b.GoingRight();
            rightthrust[pp]=sf::Vertex(p1,sf::Color(255,0,0,cc));
            rightthrust[pp+1]=sf::Vertex(p2,sf::Color(255,0,0,cc));
            rightthrust[pp+2]=sf::Vertex(p3,sf::Color(255,0,0,cc));
        }
        {
            Numi pp=3*i;


            Num thrustWidthhalf=rad/2.0f;


            //const Num xx=cos(Z_PI_4);
            Vec2 kk=pos-ang*z*rad;
            Vec2 p1=kk+perpAng*thrustWidthhalf;
            Vec2 p2=kk+perpAng*-thrustWidthhalf;
            Vec2 p3=pos-ang*rad*1.4f;
            unsigned char cc=b.GoingForward();
            backthrust[pp]=sf::Vertex(p1,sf::Color(255,0,0,cc));
            backthrust[pp+1]=sf::Vertex(p2,sf::Color(255,0,0,cc));
            backthrust[pp+2]=sf::Vertex(p3,sf::Color(255,0,0,cc));
        }
//        {
//            Numi pp=4*i;
//            Num r=b.Radius();
//            Vec2 p=b.Pos();
//            Vec2 v1=p+Vec2(r,r);
//            Vec2 v2=p+Vec2(r,-r);
//            Vec2 v3=p+Vec2(-r,-r);
//            Vec2 v4=p+Vec2(-r,r);
//
//            //sf::Color k=reinterpret_cast<sf::Color>(r);
//            auto z=sf::Color(20,20,20,120);
//            cpoints[pp]=sf::Vertex((v1),z, sf::Vector2f( 1, 1));
//            cpoints[pp+1]=sf::Vertex((v2),z, sf::Vector2f( 1,  0));
//            cpoints[pp+2]=sf::Vertex((v3),z, sf::Vector2f( 0,  0));
//            cpoints[pp+3]=sf::Vertex((v4),z, sf::Vector2f( 0,  1));
//        }

        {
            sf::Color coll2=b.getTeam().color;
            sf::Color coll(coll2.r,coll2.g,coll2.b,200);
            Numi pp=3*i;
            Vec2Unit an=b.Ang();

            Num j=b.getShape().Radius()*z;
            Vec2 p1(-j,-j);
            Vec2 p2(b.getShape().Radius(),0.0f);
            Vec2 p3(-j,+j);

            Vec2 p1r=b.Pos()+Vec2( p1.x*an.x-p1.y*an.y,p1.x*an.y+p1.y*an.x);
            Vec2 p2r=b.Pos()+Vec2( p2.x*an.x-p2.y*an.y,p2.x*an.y+p2.y*an.x);
            Vec2 p3r=b.Pos()+Vec2( p3.x*an.x-p3.y*an.y,p3.x*an.y+p3.y*an.x);

            points[pp]=sf::Vertex(p1r,coll,sf::Vector2f( -1, -1));
            points[pp+1]=sf::Vertex(p2r,coll,sf::Vector2f( 1, 0));
            points[pp+2]=sf::Vertex(p3r,coll,sf::Vector2f( -1, 1));
        }

    }
    void draw(sf::RenderWindow& w){
        //w.draw(cpoints,&shader);
        //w.draw(cpoints);
        w.draw(points);
        w.draw(backthrust);
        w.draw(leftthrust);
        w.draw(rightthrust);
    }
};
