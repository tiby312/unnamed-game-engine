#include <iostream>
#include "System.h"
#include "bot.h"


//emptyNode=std::numeric_limits<int>::max();


using namespace std;

int main()
{
    System::Start();


    BotControl mb(10,Box(0,0,512));

    Num rr=5;
    for(int i=0;i<1024;i++){
        mb.createBot(Vec2(20+i/10.0f,20+(i+40)/10.0f),rr);
    }

    cout << "Hello world!" << endl;


    BotControl* m=&mb;
    std::function<void(Vec2)> func1=[m](Vec2 pp){
        m->mousedown(pp);
    };
    std::function<void(Vec2)> func2=[m](Vec2 pp){
        m->mouseup(pp);
    };



    System::leftclickevent.registerListener(func1,true);
    System::leftclickupevent.registerListener(func2,true);

    std::function<void(Vec2)> func3=[m,rr](Vec2 pp){
        m->createBot(pp,rr);
    };
    System::rightclickevent.registerListener(func3,true);

    //System::leftclickupevent
    while (System::running()){

        System::stepStart();
        //g.tick();
        mb.step();
        mb.draw();
        System::stepEnd();
	}

    return 0;
}
