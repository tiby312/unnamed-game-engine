#pragma once


template <class T,int csize>
class VectorFixed{

    public:
    VectorFixed():arr(),siz(0){}
    bool push_back(T t){
        if(siz>=csize){
            return false; //reached capacity don't add anything
        }
        arr[siz]=t;
        siz++;
        return true;
    }
    unsigned int size(){
        return siz;
    }
    void clear(){
            siz=0;
    }

    typedef typename std::array<T,csize>::iterator iterator;
    iterator begin() {return arr.begin();}
    iterator end() {return arr.begin()+siz;}

    T& operator[](int j) { return arr[j]; }


    bool isEmpty(){
        return siz==0;
    }
    bool contains(T t){
        for(uint i=0;i<size();i++){
            if(arr[i]==t){
                return true;
            }
        }
        return false;
    }

    bool remove(T t){
        for(int i=0;i<siz;i++){
            if(arr[i]==t){
                for(int j=i;j<siz-1;j++){
                    arr[j]=arr[j+1];
                }
                siz--;
                //cout<<"REMOVED OK"<<endl;
                return true;
            }
        }
        //cout<<"FAILED TO REMOVE FROM VECTOR FIXED"<<endl;
        return false;
    }
    private:
        std::array<T,csize> arr;
        int siz;
    //iterator
};
