// *** ADDED BY HEADER FIXUP ***
#include <algorithm>
#include <complex>
#include <cstdlib>
// *** END ***
#pragma once

#include <cmath>
#include <SFML/System/Vector2.hpp>
#include <SFML/System/Vector3.hpp>
#include <SFML/Graphics.hpp>
#include <ostream>
#include <iostream>
#include <limits>
#include <list>
#include <array>
//#include "fixed_point.h"
using namespace std;


#define Z_PI 	3.14159265359
#define Z_PI_2 	1.57079632679
#define Z_PI_4 	0.78539816339
#define Z_PI_8  0.39269908169

typedef float Num;
typedef int32_t Numi;
#define NUMMAX 999999;

#define DRAWDEBUG



        //0 1 -1 2 -2 3 -3 4 -4
        class SimpySeries{
            public:
            class Iterator{
                friend class SimpySeries;
                private:
                Numi val;
                Numi index;
                Iterator(Numi v,Numi i):val(v),index(i){};
            };
            static Iterator create(){
                return Iterator(0,1);
            }
            static Numi Val(Iterator& i){
                return i.val;
            }
            static void increment(Iterator& i){
                //i.index++;
                Numi n=i.index;
                Numi valplus1=(((-2*n-1)*i.val)+n)/(2*n-1);
                i.index=n+1;
                i.val=valplus1;
            }
        };

        //1 0 2 -1 3 -2 4 -3
        class SimpyEvenSeries{
        public:
            class Iterator{
                friend class SimpyEvenSeries;
                private:
                Numi val;
                Numi index;
                Iterator(Numi v,Numi i):val(v),index(i){};
            };
            static Iterator create(){

                    return Iterator(1,1);

            }
            static Numi Val(Iterator& i){
                return i.val;
            }
            static void increment(Iterator& i){
                //i.index++;
                Numi n=i.index;
                Numi valplus1=(((-2*n-1)*i.val)+3*n)/(2*n-1);
                i.index=n+1;
                i.val=valplus1;
            }
        };


typedef sf::Vector2<Numi> Vec2i;
typedef sf::Vector2<size_t> Vec2ui;

typedef sf::Vector2<Num> Vec2;


inline Numi sign(Numi k){
    if(k<0){
        return -1;
    }else if(k>0){
        return 1;
    }
    return 0;
}

class VectorTool{
    public:


        template <class T>
        static void transpose( std::vector< std::vector<T> >& a)
        {
            std::vector<std::vector<T>> k(a.front().size());

            //std::vector<std::vector<T>> k(a.front().size());

            for(size_t i=0;i<a.size();i++){
                for(size_t j=0;j<a[i].size();j++){
                    k[j].push_back(a[i][j]);
                }
            }


//            size_t backranksize=a.back().size();
//            for(size_t i=0;i<backranksize;i++){
//                k[i].resize(a.size());
//            }
//            for(size_t i=backranksize;i<k.size();i++){
//                k[i].resize(a.size()-1);
//            }
//            cout<<"finished matrix size="<<k.front().size()<<","<<k.size()<<endl;
//
//            for (int i = 0; i < a.size(); i++)
//            {
//                for (int j = 0; j < a[i].size(); j++)
//                {
//                    k[j][i] = a[i][j];
//                }
//            }
            a=k;
        }

        template <class T>
        static void remove(std::vector<T>& vec,T k){
            size_t bs=vec.size();
            vec.erase(std::remove(vec.begin(), vec.end(), k), vec.end());
            size_t af=vec.size();

            size_t diff=bs-af;
            if(diff!=1){
                cout<<"vector remove problem. num deleted="<<diff<<endl;
            }
        }
        template <class T>
        static void append(std::vector<T>& a,const std::vector<T>& b){
            a.insert(a.end(), b.begin(), b.end());
        }

        template <class T>
        static void removeFromAndStack(std::vector<T>& a,std::vector<T>& b,size_t num){
            Numi aa=a.size()-num;
            for(Numi i=std::max(0,aa);i<a.size();i++){
                b.push_back(a[i]);
            }
            Numi i=a.size()-num;
            a.resize(std::max(0,i));
        }

        template <class T>
        static void append(std::vector<T>& a,const std::vector<T>& b,size_t num){
            a.insert(a.end(),b.begin(),b.begin()+num);
        }

        template <class T>
        static void pushFront(std::vector<T>& a,const std::vector<T>& b){
            a.insert(a.begin(), b.begin(), b.end());
        }


//VectorTool::eraseNum(carried,aa);
        template <class T>
        static void eraseNum(std::vector<T>&a, const size_t num){
            for(size_t i=0;i<num;i++){
                a.erase(a.begin());
            }
        }

        template <class T>
        static void appendBackwards(std::vector<T>& a,const std::vector<T>& b){
            appendBackwards(a,b,b.size());
        }
        template <class T>
        static void appendBackwards(std::vector<T>& a,const std::vector<T>& b,size_t aa){
            size_t k=b.size()-1;
            for(size_t i=0;i<aa;i++){
                a.push_back(b[k]);
                k--;
            }

        }

        template <class T>
        static void fillTosize(std::vector<T>& a,size_t s){
            //cout<<"S EQUALS="<<s<<endl;
            while(a.size()<s){
                a.push_back(T());
            }
        }
};

inline Num fastyacos(Num x) {
   return (-0.69813170079773212 * x * x - 0.87266462599716477) * x + 1.5707963267948966;
}


inline float fastysin( float x ){
    //x = fmod(x + Z_PI, Z_PI * 2) - Z_PI; // restrict x so that -M_PI < x < M_PI
    const float B = 4.0f/Z_PI;
    const float C = -4.0f/(Z_PI*Z_PI);

    float y = B * x + C * x * std::abs(x);

    const float P = 0.225f;

    return P * (y * std::abs(y) - y) + y;
}
inline float fastycos(float x)
{
    return fastysin(x + (Z_PI / 2));
}


class MLineSeg{
    public:
    Vec2 a;
    Vec2 b;

};
class Vec2Tool{
    public:

        static Vec2 Flip180(Vec2 a){
            return -a;
        }
        static Num innerProduct( Vec2 a,Vec2 b){
            return (a.x*b.x)+(a.y*b.y);
        }
        static Num crossProduct(Vec2 a,Vec2 b){
            return (a.x*b.y)-(a.y*b.x);
        }
        static Num innerAngle(Num innerProduct){
            return fastyacos(innerProduct);
        }


        static bool intersect(MLineSeg l1,MLineSeg l2){
                Vec2 A=l1.a;
                Vec2 B=l1.b;

                Vec2 C=l2.a;
                Vec2 D=l2.b;


                Vec2 CmP = C-A;//new PointF(C.X - A.X, C.Y - A.Y);
                Vec2 r = B-A;//new PointF(B.X - A.X, B.Y - A.Y);
                Vec2 s = D-A;//new PointF(D.X - C.X, D.Y - C.Y);

                Num CmPxr = crossProduct(CmP,r);
                Num CmPxs = crossProduct(CmP,s);
                Num rxs = crossProduct(r,s);//

                if (CmPxr == 0.0f)
                {
                    // Lines are collinear, and so intersect if they have any overlap

                    return ((C.x - A.x < 0.0f) != (C.x - B.x < 0.0f))
                        || ((C.y - A.y < 0.0f) != (C.y - B.y < 0.0f));
                }

                if (rxs == 0.0f)
                    return false; // Lines are parallel.

                Num rxsr = 1.0f / rxs;
                Num t = CmPxs * rxsr;
                Num u = CmPxr * rxsr;

                return (t >= 0.0f) && (t <= 1.0f) && (u >= 0.0f) && (u <= 1.0f);
            }

//        static bool intersect(MLineSeg l1,MLineSeg l2){
//
//            Vec2 p=l1.a;
//            Vec2 r=l1.b-l1.a;
//
//            Vec2 q=l2.a;
//            Vec2 s=l2.b-l2.a;
//            Num QmPxr=crossProduct(q-p,r)
//            Num rxs=crossProduct(r,s);
//            if(QmPxr==0.0f&&rxs==0.0f){
//                //lines are collinear
//                  0 ≤ (q − p) · r ≤ r · r or 0 ≤ (p − q) · s ≤ s · s
//
//                  if(0<=  )
//            }
//
//            Num u= QmPxr/rxs;
//            //u = (q − p) × r / (r × s)
//        }

        static Num minimumDis(MLineSeg l1,MLineSeg l2){
            if(intersect(l1,l2)){
                return 0;
            }
            std::array<Num,4> bla;
            bla[0]=minDis(l2,l1.a);
            bla[1]=minDis(l2,l1.b);
            bla[2]=minDis(l1,l2.a);
            bla[3]=minDis(l1,l2.b);

            Num smallest=bla[0];
            for(size_t i=1;i<3;i++){
                if(bla[i]<smallest){
                    smallest=bla[i];
                }
            }
            return smallest;

        }
        static Num minDis(MLineSeg l, Vec2 p) {
            Vec2 v=l.a;
            Vec2 w=l.b;
          // Return minimum distance between line segment vw and point p
          const float l2 =  Vec2Tool::getlengthSqr(w-v);// length_squared(v, w);  // i.e. |w-v|^2 -  avoid a sqrt
          if (l2 == 0.0) return distance(p, v);   // v == w case
          // Consider the line extending the segment, parameterized as v + t (w - v).
          // We find projection of point p onto the line.
          // It falls where t = [(p-v) . (w-v)] / |w-v|^2
          const float t = Vec2Tool::innerProduct(p - v, w - v) / l2;
          if (t < 0.0) return Vec2Tool::distance(p, v);       // Beyond the 'v' end of the segment
          else if (t > 1.0) return Vec2Tool::distance(p, w);  // Beyond the 'w' end of the segment
          const Vec2 projection = v + t * (w - v);  // Projection falls on the segment
          return Vec2Tool::distance(p, projection);
        }
        static Num distance(Vec2 v,Vec2 w){
            return sqrt(Vec2Tool::getlengthSqr(w-v));
        }

        static void truncate(Vec2& v,Num& x){
            Num z=getlengthSqr(v);
            if(z>=x*x){
                v/=(sqrt(z));
                v*=(x);
            }

        }

        static Vec2 norm(Vec2 a){
            Num z=sqrt(getlengthSqr(a));
            return Vec2(a.x/z,a.y/z);
        }
        static Vec2 perp90deg(Vec2 v){
            return Vec2(-v.y,v.x);
        }
        static void normalize(Vec2& v){
            Num z=sqrt(getlengthSqr(v));
            v.x/=z;
            v.y/=z;
        }
        static Num getlengthSqr(Vec2 v){
            return (v.x*v.x)+(v.y*v.y);
        }
//        static void rotate(Vec2& v1,Vec2 v2){
//                Vec2 v1b=v1;
//                v1.x = v1b.x * v2.x - v1b.y * v2.y;
//                v1.y = v1b.x * v2.y+ v1b.y * v2.x;
//        }
        static Vec2 rotatey(Vec2 v1,Vec2 v2){
                Vec2 bla;
                bla.x = v1.x * v2.x - v1.y * v2.y;
                bla.y = v1.x * v2.y+ v1.y * v2.x;
                return bla;
        }
        static void rotateBackwards(Vec2& v1,Vec2 v2){
            Vec2 v1b=v1;
            v1.x = v1b.x * v2.x + v1b.y * v2.y;
            v1.y = -v1b.x * v2.y+ v1b.y * v2.x;
        }
};




typedef Vec2 Vec2Unit;

//typedef sf::Vector2<num> vec2;
typedef sf::Vector3<Num> Vec3;


struct MLog{
    static void error(std::string k){
        cout<<k<<endl;
        exit(0);
    }
    static void log(std::string k){
        cout<<k<<endl;
    }
};

class MyTool{
    public:
    static Num sign(Num z){
        if(z<0){
            return -1.0f;
        }else if(z>0){
            return 1.0f;
        }
        return 0.0f;
    }
    static Num truncate(Num k,Num j){
        Num l=sign(k);
        Num z=min(abs(k),j);
        return z*l;
    };

    static bool valueInRange(Num value, Num min, Num max){ return (value >= min) && (value < max); }

    static bool intersects(Vec3 a,Vec3 b) {
        /*
        return (a.x < b.x+b.z && a.x+a.z > b.x &&
            a.y < b.y+b.z && a.y+a.z > b.y);*/

        bool xOverlap = valueInRange(a.x, b.x, b.x + b.z) ||
                        valueInRange(b.x, a.x, a.x + a.z);

        bool yOverlap = valueInRange(a.y, b.y, b.y + b.z) ||
                        valueInRange(b.y, a.y, a.y + a.z);

        //std::cout<<"intersect?="<<a<<b<<"="<<xOverlap && yOverlap;
        return xOverlap && yOverlap;

    }
    static  Vec3 doubleBound(const Vec3& b){
        return Vec3(b.x-(b.z/2.0f),b.y-(b.z/2.0f),2*b.z);
    }
    static  bool contains(const Vec2& p,const Vec3& b){
        bool xOverlap=valueInRange(p.x,b.x,b.x+b.z);
        bool yOverlap=valueInRange(p.y,b.y,b.y+b.z);
        return xOverlap&&yOverlap;
    }
    static  bool contains(const Vec3& p,const Vec3& b){
        bool oo=valueInRange(p.x,b.x,(b.x+b.z)-p.z)&&valueInRange(p.y,b.y,(b.y+b.z)-p.z);
        bool oo2=p.z<=b.z;
        return oo&&oo2;
    }

};


inline ostream& operator<< (ostream &out,const Vec2 &v){
    out << "(" <<v.x << ", " <<   v.y  << ")";
    return out;
}
//ostream& operator<< (ostream &out,const Vec3 &v);


inline ostream& operator<< (ostream &out,const Vec3 &v){
    // Since operator<< is a friend of the Point class, we can access
    // Point's members directly.
    out << "(" <<v.x << ", " <<   v.y  << ","<<v.z<<")";
    return out;
}

//inline Num Nsign(Num n){
//	if(n>0) return 1;
//	if(n<0) return -1;
//	return 0;
//}


//class Quad;
inline bool invalidNum(Num a){
    if(isnan(a)||isinf(a)){
       return true;
       }
        return false;
}

//class Stadium{
//    StadiumShape sh;
//    Vec2Unit ang;
//    Vec2 pos;
//    static Num minimumDis(MLineSeg l1,MLineSeg l2){
//    static intersect
//
//};




//    Vec3 Bound(){
//        return bound;
//    }
//    Vec2 Pos(){
//        return pos;
//    }
//    RadiusPair Radiuspair(){
//        return radiuspair;
//    }

inline void rotateVec(Vec2Unit& a,Num theta){

    Vec2 nn(fastycos(theta),fastysin(theta));
    Vec2Tool::normalize(nn);
    a=Vec2Tool::rotatey(a,nn);

}



/*
class Brain{
    virtual void collideEvent(RepelAnswer a)=0;


};*/

/*
class Color{
    public:
    Num r;
    Num g;
    Num b;
};*/




//typedef fix16_t num;
//typedef Fix16 Num;

class PState{
	public:
        PState(Num i):radius(i){}

        Num radius;
        //Num bouncyness;
	private:
        PState():radius(){}
};
class MState{
    public:
	MState():pos(),vel(),angv(),ang(1,0){}
	MState(Vec2 a):pos(a),vel(),angv(0),ang(1,0){}
	MState(Vec2 a,Vec2 b):pos(a),vel(b),angv(0),ang(1,0){}
	MState(Vec2 a,Vec2 b,Vec2Unit c):pos(a),vel(b),angv(0),ang(c){}
	Vec2 pos;
	Vec2 vel;
	Num angv;
	Vec2Unit ang;

	void add(const MState& rhs){
	    //MState f=lfs;
        pos+=rhs.pos;
        vel+=rhs.vel;
        ang+=rhs.ang;
        angv+=rhs.angv;
	}
};

/*
inline MState operator+(MState lhs, const MState& rhs)
{
	lhs.pos+=rhs.pos;
	lhs.vel+=rhs.vel;
  //lhs += rhs;
  return lhs;
}*/
#include <limits>
#include <mutex>


/*
Vec2 sadd(const Vec2 a,const Vec2 b){
	return Vec2(fix16_sadd(a.x.value,b.x.value),fix16_sadd(a.y.value,b.y.value));
}*/

/*
class Tools{
    std::mutex mm;
    Num getRandAngle();
};*/


//Num getlengthSqr(Vec2 v);
//Vec2 normalize(Vec2 v);

// requires #include <limits>
template<typename T>
inline bool isinf(T value)
{
return std::numeric_limits<T>::has_infinity() &&
value == std::numeric_limits<T>::infinity();
}
/*
Vec2 avoidVector(Vec2 offset,Num length,Num lengthSqr){

	//Num lengthSqr=getlengthSqr(offset);
	//Num length=sqrt(lengthSqr);
	Num intensity=radius/lengthSqr;
	Num intensitynegForce=intensity*-0.5;
	Vec2 norm=offset/length;
	Vec2 comp=norm*intensitynegForce;
	return comp;
}*/
/*
Vec2 avoidVector(const Vec2 a,const Vec2 b,const Num radius){


	Vec2 offset=a-b;

	Num lengthSqr=getlengthSqr(offset);
	Num length=sqrt(lengthSqr);
	Num intensity=radius/lengthSqr;
	Num intensitynegForce=intensity*-0.5;
	Vec2 norm=offset/length;
	Vec2 comp=norm*intensitynegForce;
	return comp;

}*/





inline size_t myIntPow(size_t base,size_t power){
    size_t res=1;
    for(size_t i=0;i<power;i++){
        res*=base;
    }
    //cout<<"base="<<base<<" power="<<power<<" res="<<res<<endl;
    return res;
}



/*
bool intersects(vec3 v,vec3 b){
	return !(v.x>=b.x+b.z||v.y>=b.y+b.z||v.x+v.z<b.x&&v.y+v.z<b.y);

}*/


inline Vec2 friction(Vec2 v){
	Num lengthSqr=Vec2Tool::getlengthSqr(v);//getlengthSqr(v);
	Num length=sqrt(lengthSqr);
	Vec2 norm=v/length;
	norm*=-0.002f;

	return norm;
}





/*
bool intersects(vec3 v,vec3 b){
	return !(v.x>=b.x+b.z||v.y>=b.y+b.z||v.x+v.z<b.x&&v.y+v.z<b.y);

}*/

//bool intersects(Vec3 a,Vec3 b);
//const Num NUM_MAX=std::numeric_limits<int32_t>::max();
//const Num NUM_MIN=std::numeric_limits<int32_t>::min();

/*
Num nsqrt(Num n){
	return  fix16_sqrt(n<<16)>>16;
}
Num nDivX(Num a,Num b){
	return fix16_div(a,b);
}*/



/*
namespace nn{
	typedef int32_t n;
	static const n MIN=0x80000000;
	static const n MAX=0x7FFFFFFF;

	class V{
	public:
		n x;
		n y;
		V():x(),y(){};
		V(n xx,n yy):x(xx),y(yy){}
		V& operator= (const V &cSource)
		{
		    // do the copy
		    x = cSource.x;
		    y=	cSource.y;

		    // return the existing object
		    return *this;
		}
		static V add(V v1,V v2){
			return V(v1.x+v2.x,v1.y+v2.y);
		}
		n lengthSqr(){
			return x*x+y*y;
		}
		void add(V v2){
			x+=v2.x;
			y+=v2.y;
		}
		static V sub(V v1,V v2){
					return V(v1.x-v2.x,v1.y-v2.y);
		}
		void neg(){
			x=-x;
			y=-y;
		}
		static V mult(V v1,n num){
			return V(v1.x*num,v1.y*num);
		}
		static V div2(V c1,unsigned int nn){
			return V(c1.x>>nn,c1.y>>nn);
		}
		sf::Vector2f toSF(){
			return sf::Vector2f(float(x),float(y));
		}
	};
	class B{
	public:
		V topleft;
		V bottomright;
		B():topleft(),bottomright(){};
		B(V v1,V v2):topleft(v1),bottomright(v2){}

		B(V midpoint,n size):topleft(midpoint.x-size,midpoint.y-size),bottomright(midpoint.x+size,midpoint.y+size){}
		V size(){
			return V::sub(bottomright,topleft);
		}

		V midpoint(){
			return   V::add(topleft,V::div2(size(),1));

		}
		bool contains(V p){
			if(p.x>=topleft.x&&p.y>=topleft.y&&p.x<bottomright.x&&p.y<bottomright.y){
				return true;
			}
			return false;
		}
		bool intersects(B b){
			return (topleft.x>=b.bottomright.x&&topleft.y>=b.bottomright.y&&b.topleft.x<bottomright.x&&b.topleft.y<bottomright.y);

		}
	};
	class C{
	public:
		V center;
		n radius;
		C():center(),radius(){};
		C(V c,n r):center(c),radius(r){};
	};
}

*/



class RadiusPair{
    public:
    RadiusPair(Num rad,Num radsqr):radius(rad),radiusSqr(radsqr){};
    RadiusPair():radius(0),radiusSqr(0){};
    Num radius;
    Num radiusSqr;
    void setRadius(Num r){
        radius=r;
        radiusSqr=r*r;
    }
};


class SCircle{
    public:
        Vec2 pos;
        //RadiusPair radiuspair;
        Num radius;
        class Con{
            public:
            Vec2 pos;
            Num radius;
        };
        SCircle(Vec2 p,Num r):pos(p),radius(r){}
        void setRadius(Num r){
            //radiuspair.setRadius(r);
            radius=r;
            //bound=computeBound(pos,r);
        }
        Num Radius() const{
            return radius;
        }
        Num RadiusSqr() const{
            return radius*radius;
        }
        class ResidualData{
            public:
                ResidualData(){}

                ResidualData(Vec2 o,Num r):offset(o),radius(r){}
            Vec2 offset;
            //RadiusPair radiuspair;
            Num radius;
            ResidualData operator-()  {
                ResidualData a;
                a.offset=-offset;
                a.radius=-radius;// radiuspair=radiuspair;
                return a;

           }
        };
         static  bool contains(const SCircle& a,const Vec3& b){

              Num r=a.radius;
              Vec2 p=a.pos;

              if(p.x+r<b.x+b.z&&
                 p.x-r>=b.x&&
                 p.y+r<b.y+b.z&&
                 p.y-r>=b.y

                 ){
//              if(MyTool::contains(Vec2(p.x,p.y-r),b)&&
//                 MyTool::contains(Vec2(p.x+r,p.y),b)&&
//                 MyTool::contains(Vec2(p.x,p.y+r),b)&&
//                 MyTool::contains(Vec2(p.x-r,p.y),b)){
                return true;
              }
              return false;

             //return MyTool::contains(a.bound,b);
        }
        static bool intersects(Vec3 a,SCircle b){
//            if(MyTool::contains(b.pos,a)){
//                return true;
//            }
            return MyTool::intersects(a,Vec3(b.pos.x-b.radius,b.pos.y-b.radius,b.radius*2));

        }


       static bool intersects(const SCircle& a,const SCircle &b,ResidualData& res){
            //if(MyTool::intersects(a.bound,b.bound)){
                Vec2 offset=b.pos-a.pos;
                Num n=Vec2Tool::getlengthSqr(offset);
                    //a=b+c
                    //a^2=(b+c)^2
                    //a^2=b^2+c^2+2bc
                Num j=a.radius*a.radius;
                Num j2=b.radius*b.radius;
                Num j3=2*(a.radius*b.radius);
                    //if(l<dim.radiuspair.radius+bsq.radiuspair.radius){
                if(n<j+j2+j3){
                    Num l=sqrt(n);
                    //RadiusPair rr(l,n);
                    res.offset=offset;
                    //res.radiuspair=rr;
                    res.radius=l;
                    return true;
                    //ans.push_back(typename Quad<T>::Answer(bb,offset,rr));
                }
            //}
            return false;
        }
};
class StadiumShape{
    public:
    Num radius;
    Num length;
    Vec2 ang;
};

class Stadium{
public:
    Stadium(StadiumShape a,Vec2 b,Vec2Unit c):sh(a),pos(b),aang(c){}
    StadiumShape sh;
    Vec2 pos;
    Vec2Unit aang;

    MLineSeg getLineSeg() const{
        Vec2 jj=Vec2Tool::perp90deg(Vec2Tool::rotatey(aang,sh.ang));
        Vec2 pos1=pos+ jj*-sh.length/2.0f;
        Vec2 pos2=pos+ jj*sh.length/2.0f;
        return MLineSeg{pos1,pos2};
    }
    bool static intersectCircle(const Stadium& a,const SCircle& c,Num& residual){
        MLineSeg lin=a.getLineSeg();
        Num k=Vec2Tool::minDis(lin,c.pos);
        if(k<c.Radius()+a.sh.radius){
            residual=k;
            return true;
        }
        return false;
    }
    bool static intersect(const Stadium& a,const Stadium& b,Num& residual){
        //static Num minimumDis(MLineSeg l1,MLineSeg l2){
        MLineSeg l1=a.getLineSeg();
        MLineSeg l2=b.getLineSeg();
        Num lol=Vec2Tool::minimumDis(l1,l2);

        if(lol<a.sh.radius+b.sh.radius){
            residual=lol;
            return true;
        }
        return false;
    }

};
//

