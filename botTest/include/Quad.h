// *** ADDED BY HEADER FIXUP ***
#include <algorithm>
#include <iostream>
#include <vector>

// *** END ***
/*
 * Quad.h
 *
 *  Created on: May 24, 2013
 *      Author: ken

 This is a LOOSE quad tree array implementation. It's threadsafe so you can query concurrently.

 Cool things it does:
    -Query returns objects nearby as well as each of their offset, length, and lengthsqr (so you dont have to recompute)
    -No need to rebuild quadtree every game step. It checks to see if an object is still within the same node, and
    will only reinsert if its not. So if nothing is moving, nothing is changing in the quadtree.
 */
#pragma once
#include <array>
#include <mutex>
#include "MyLib.h"
#include "System.h"
//#include "Moveable.h"
#include "VectorFixed.h"
/*
    QuadObj
    interface that objects you want to keep in quadtree must implement
    objects that implement this interface are responsible for holding on to the above Cont object
*/
//template <class T>


/*
        Cont
        Container that objects in the quadtree need to hold to keep track of which node they are in.
        This allows one to know where an object is in the quadtree instantly
    */

//    class Cont{
//        public:
//            Cont():nodeD(){}
//            NodeD nodeD;
//            //std::vector<Answer> answers;
//    };





//
//class QuadObj{
//public:
//    virtual ~QuadObj(){}
//	virtual Vec2& Pos() const=0;
//	virtual Vec2& Vel() const=0;
//	virtual Num& Mass() const=0;
//	virtual Num& Radius() const=0;
//	virtual Vec3& Bound() const=0;
//	virtual Cont& getCont()=0;
//};

//
// class ResidualData{
//    public:
//    ResidualData(Vec2 o,RadiusPair r):offset(o),radiuspair(r){}
//    Vec2 offset;
//    RadiusPair radiuspair;
//    ResidualData operator-()  {
//        ResidualData a;
//        a.offset=-offset;
//        a.radiuspair=radiuspair;
//        return a;
//
//   }
//    private:
//        ResidualData(){}
//
//};





constexpr size_t myIntPowConst(size_t base,size_t power){
    return power==1 ? base: base*myIntPowConst(base,power-1);
}

constexpr size_t calcNumberOfNodes(size_t level){
    return (myIntPowConst(4,level)-1)/3;
}
/*
template <class T>
class QAnswer{
    //friend class Quad;
    public:
    T* b;
    SCircle::ResidualData r;
    QAnswer(T* bb,SCircle::ResidualData v):b(bb),r(v){}
    QAnswer(){}
};
*/

typedef uint NodeD;

uint MAXSTORE=3;
const NodeD emptyNode=std::numeric_limits<uint>::max();

template <class T,int MaxLevel=4,int maxConts=256,int MaxInQuad=128>
class Quad{
    public:
    struct QAnswer{
        T* b;
        Circle::Ans r;
    };
    struct Cont{
        T* o;
        NodeD index;
        Cont(T& o):o(&o),index(emptyNode){}
        Cont():o(nullptr),index(emptyNode){}

    };
    struct Node{
        Node():bound(),objs(),leaf(){}
        Box bound;
        VectorFixed<T*,MaxInQuad> objs;
        //VectorFixed<T*,3> store;
        bool leaf;
    };
    std::array<Node,calcNumberOfNodes(MaxLevel)> nodes;
    VectorFixed<Cont,maxConts> conts;
    NodeD firstLeaf;


    Quad(Box bound):firstLeaf(nodes.size()-myIntPowConst(4,MaxLevel-1)){
        cout<<"created quad with "<<MaxLevel<<"levels. "<<nodes.size()<<" nodes. firstleaf="<<firstLeaf<<endl;
        for(size_t i=0;i<nodes.size();i++){
            nodes[i].leaf=false;
            //nodes[i].bound=Box();
            //nodes[i].loosebound=Vec3();
        }
        nodes[0].bound=bound;
        setbounds(0);
    }

    bool insert(T& k){
        Cont blep(k);
        //MLog::log("insert start");
        NodeD j=insert(0,blep);
        if(j!=emptyNode){
            actuallyInsert(j,blep);
        }else{
            MLog::error("insert fail");
            return false;
        }

        if(!conts.push_back(blep)){
            MLog::error("cannot fit anyone things into quad!");
        }
        //MLog::log("insert finish");
        return true;
    }


    bool fitsInAChild(Cont& ca){
        T& o=*(ca.o);
        if(ca.index>=firstLeaf){
            return false;
        }
        std::array< NodeD,4> cin(children(ca.index));
        for(int i=0;i<4;i++){
            if(nodes[cin[i]].bound.contains(o)){
                return true;
            }
        }
        return false;
    }


    void update(){
        //MLog::log("update start");

        for(uint i=0;i<conts.size();i++){
            Cont& ca=conts[i];
            NodeD in=ca.index;
            T& o=*(ca.o);
            //bool a=fitsInAChild(o);//if true start insertion from this level;

            //(!nodes[in].leaf&&fitsInAChild(ca))||
            //||!nodes[ca.index].bound.contains(o.pos)
            if(!nodes[ca.index].bound.contains(o)||fitsInAChild(ca)){
                reinsert(ca); //reinsert from root
            }
        }
        //MLog::log("update finish");
    }


	void draw(){
		draw(0);
	}


    template <int TSize>
    void query(VectorFixed<QAnswer,TSize>& ans,T& self,const Circle& shape) {
        query(ans,self,shape,0);
    }

    template <int  TSize>
    void query(VectorFixed<QAnswer,TSize>& ans,T& self,const Circle& shape,int d) {
        if(d>=nodes.size()){
            return;
        }
        Node& nn=nodes[d];

        if(! nn.bound.intersectsApprox(shape) ){
            return;
        }

        for( auto bb  :nn.objs){
            //const SCircle bsq=bb->getShape();
            Circle::Ans res;
            if(bb!=&self&&self.intersects(*bb,res)){
                ans.push_back(QAnswer{bb,res});
            }
        }
        if(nn.leaf){
            return;
        }
        std::array< NodeD,4> n(children(d));
        for(int i=0;i<4;i++){
            query(ans,self,shape,n[i]);
        }
    }






    void setbounds(NodeD d){
		if(d<firstLeaf){
			std::array< NodeD,4> c(children(d));
            Num midsize=nodes[d].bound.z/2.0f;

            Node nd=nodes[d];
            nodes[c[0]].bound=Box(nd.bound.x,nd.bound.y,midsize);
            nodes[c[1]].bound=Box(nd.bound.x+midsize,nd.bound.y,midsize);
            nodes[c[2]].bound=Box(nd.bound.x+midsize,nd.bound.y+midsize,midsize);
            nodes[c[3]].bound=Box(nd.bound.x,nd.bound.y+midsize,midsize);
            //Num midsize2=d.node->loosebound.z/2.0f;

            for(int i=0;i<4;i++){
                setbounds(c[i]);
            }
		}
	}


    static std::array< NodeD,4> children(NodeD d) {
        size_t z=4*d;
        std::array< NodeD,4> v{{
            z+1,
            z+2,
            z+3,
            z+4}
        };
        return v;
    }
    static NodeD parent(NodeD d) {
		return (d-1)/4;
	}



    void draw(NodeD d){
	    if(d>=nodes.size()){
            return;
	    }
	    Vec3 k=nodes[d].bound;

		//cout<<d.level<<endl;
        //Vec3 kj=getBound(d);
        //MLog::log("drawww");
		for( auto &bb  :nodes[d].objs){
            //QuadObj* bb=*it;
        //sf::Color col(0,0,255,10);
        //System::drawSquareOutline(col,nodes[d].bound);
        //sf::Color col2(0,255,0,10);
        //System::drawSquareOutline(col2,nodes[d].loosebound);
            const Circle& bsq=*bb;


			//QuadObj* bbb=*it;
			System::drawLine(
					sf::Color{255,0,0,100},bsq.pos,
					Vec2(k.x,k.y));
                System::drawLine(
					sf::Color{255,0,0,100},bsq.pos,
					Vec2(k.x+k.z,k.y+k.z));
//			System::drawLine(sf::Color{1,0,0},0.5,bsq.pos,
//						Vec2(kj.x+kj.z,
//						kj.y+kj.z));

		}

        if (nodes[d].leaf){
			Vec2 kv(k.x,k.y);
			//col=sf::Color(0,50,0,160);
			System::drawCircle(kv,2,sf::Color(0,1,0,255));
			//return;
			//System::drawSquare(col,Vec3(k.x,k.y,k.z));
			return;
        }



        std::array< NodeD,4> b(children(d));
        for(int i=0;i<4;i++){
            draw(b[i]);
        }


	}

    struct Tuple{
        T* o;
        NodeD targ;
    };
    Cont* findCont(T& o){
        for(uint i=0;i<conts.size();i++){
            Cont& cap=conts[i];
            if(cap.o==&o){
                return &cap;
            }
        }
        MLog::error("could not find cont!");
        return nullptr;
    }
    void actuallyInsert(NodeD d,Cont& c){
        if(nodes[d].objs.push_back(c.o)){
            //MLog::log("INSERTED");
            c.index=d;
        }else{
            c.index=emptyNode;
        }
    }
    NodeD insert(NodeD d,Cont& c){

        T& o=*(c.o);
        if(d>=nodes.size()){
            return emptyNode;
        }
        if(!nodes[d].bound.contains(o)){
            return emptyNode;
        }



        std::array< NodeD,4> ci(children(d));

        for(int i=0;i<4;i++){
            NodeD j=insert(ci[i],c);
            if(j!=emptyNode){
                return j;
            }
        }

        return d;
    }

	NodeD reinsert(Cont& ca){
        if(ca.index!=emptyNode){
            if(nodes[ca.index].objs.remove(ca.o)){
                ca.index=emptyNode;
            }else{
                MLog::error("ERROR REINSERTING");
            }
        }
        NodeD j=insert(0,ca);
        if(j!=emptyNode){
            actuallyInsert(j,ca);
        }
        return j;
	}
};


