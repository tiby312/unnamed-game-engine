


#include "MyLib.h"
#include "Vec.h"
#include "VectorFixed.h"
#include "Quad.h"
#include "System.h"
class Bot: public Circle{

    public:
        Vec2 vel;
        VectorFixed<Bot*,255> handled;
    Bot(Vec2 po,Num rad):Circle(po,rad),vel(0,0){};
    Bot(){};

};

struct ColPair{
    Bot* a;
    Bot* b;
    Circle::Ans ans;
};
typedef Quad<Bot,5,2048> BOTQUAD;
class BotControl{
    private:
    Num maxspeed;
    Num maxspeedSqr;
    VectorFixed<Bot,2048> bots;

    Box bounds;
    BOTQUAD quad;

    Bot* pin;
    public:
    BotControl(Num r,Box bon):maxspeed(4),maxspeedSqr(maxspeed*maxspeed),bots(),bounds(bon),quad(bounds),pin(nullptr){


    }
    void mousedown(Vec2 p){
        Bot* closest=nullptr;
        Num closestSqr=1000000;
        for(uint i=0;i<bots.size();i++){
            Bot* b=&bots[i];
            Vec2 offset=b->pos-p;
            Num j=Vec2Tool::getlengthSqr(offset);
            if(j<closestSqr){
                closest=b;
                closestSqr=j;
            }
        }
        if(closest!=nullptr){
            pin=closest;
        }
    }
    void mouseup(Vec2 p){
        pin=nullptr;
    }
    Bot* createBot(Vec2 pos,Num r){
        bots.push_back(Bot(pos,r));
        Bot* j=&bots[bots.size()-1];
        quad.insert(*j);
        return j;
    }
    void handleCollision(ColPair& ca){

        if(ca.ans.dis<=0.001f){
            ca.a->vel+=Vec2(1,0);
            ca.b->vel+=Vec2(-1,0);
            return;
        }
        Num radsum=ca.a->radius+ca.b->radius;
        Num mag=1-(ca.ans.dis/radsum);
        mag*=mag;
        mag*=64.0f;

        Vec2 offsetNorm=ca.ans.offset/(ca.ans.dis*ca.ans.dis);
        Vec2 val1=offsetNorm*-mag;
        Vec2 val2=offsetNorm*mag;
        ca.a->vel+=val1;
        ca.b->vel+=val2;

    }

    void step(){
        /*
        for(uint i=0;i<bots.size();i++){
            Bot& b1=bots[i];
            for(uint j=i+1;j<bots.size();j++){

                Bot& b2=bots[j];
                Circle::Ans ans;
                if(b1.intersects(b2,ans)){
                    ColPair cpp=ColPair{&b1,&b2,ans};
                    handleCollision(cpp);
                }
            }
        }*/
        /*
        VectorFixed<BOTQUAD::QAnswer,100> ans;
        for(uint i=0;i<bots.size();i++){
            Bot& b=bots[i];
            quad.query(ans,b,b);
            for(uint i=0;i<ans.size();i++){
                BOTQUAD::QAnswer& qa=ans[i];
                Bot* b2=qa.b;
                if(!b2->handled.contains(&b)){
                        Circle::Ans cans=qa.r;
                        ColPair cpp=ColPair{&b,b2,cans};
                        handleCollision(cpp);
                        b.handled.push_back(b2);
                        System::drawLine(sf::Color::Red,b.pos,b2->pos);
                }
            }
            ans.clear();
        }*/
        VectorFixed<Bot*,64> a;
        handleCollide(a,0);
        for(uint i=0;i<bots.size();i++){
            Bot& b=bots[i];
            constrainVel(b);
            b.pos+=b.vel;
            constrainPos(b);

            b.handled.clear();


        }
        if(pin!=nullptr){
            pin->pos=System::mousepos;
        }
        quad.update();
    }

    void checkPair(Bot* b1,Bot* b2){
            //Bot& b2=bots[j];
            Circle::Ans ans;
            if(b1->intersects(*b2,ans)){
                ColPair cpp=ColPair{b1,b2,ans};
                handleCollision(cpp);
                //System::drawLine(sf::Color::Red,b1->pos,b2->pos);
            }
    }
    void handleCollide(VectorFixed<Bot*,64> trail,NodeD d){
        if(d>=quad.nodes.size()){
            return;
        }
        VectorFixed<Bot*,64> trail2;
        for(uint i=0;i<trail.size();i++){
            Bot* t=trail[i];
            if(quad.nodes[d].bound.intersectsApprox(*t)){
                trail2.push_back(t);
            }
        }
        for(uint i=0;i<quad.nodes[d].objs.size();i++){
            Bot* b1=quad.nodes[d].objs[i];
            for(uint j=i+1;j<quad.nodes[d].objs.size();j++){
                Bot* b2=quad.nodes[d].objs[j];
                checkPair(b1,b2);
            }

            for(uint j=0;j<trail2.size();j++){
                Bot* b2=trail2[j];
                checkPair(b1,b2);
            }


        }

        for(uint i=0;i<quad.nodes[d].objs.size();i++){
            Bot* b1=quad.nodes[d].objs[i];
            trail2.push_back(b1);
        }

        std::array< NodeD,4> cc(quad.children(d));
        for(int i=0;i<4;i++){
            handleCollide(trail2,cc[i]);
        }

    }
    void constrainVel(Bot& b){
        //TODO constrain vel
        Num k=Vec2Tool::getlengthSqr(b.vel);

		 if(k>maxspeedSqr){
			 Num l=sqrt(k);
			 Vec2 norm=b.vel/l;
			 b.vel=norm*maxspeed;
		 }

    }
    void constrainPos(Bot& a){
        //const Circle ci=a.getShape();
        if(!bounds.contains(a)){
            const Num sidedamp=1.0f;
            const Num rad=a.radius;//a.shape.radius;
            const Vec2 pos=a.pos;
            Vec2 newpos=pos;
            Vec2 newvel=a.vel;
            if(pos.x<bounds.x+rad){
                newpos.x=bounds.x+rad;
                newvel.x=-sidedamp*newvel.x;
            }else if( pos.x>bounds.x+bounds.z-rad){
                newpos.x=bounds.x+bounds.z-rad;
                newvel.x=-sidedamp*newvel.x;
            }
            if(pos.y<bounds.y+rad){
                newpos.y=bounds.y+rad;
                newvel.y=-sidedamp*newvel.y;
            }else if( pos.y>bounds.y+bounds.z-rad){
                newpos.y=bounds.y+bounds.z-rad;
                newvel.y=-sidedamp*newvel.y;
            }
            a.pos=newpos;
            a.vel=newvel;
        }
    }
    void draw(){
        //quad.draw();
        for(uint i=0;i<bots.size();i++){
            Bot& b=bots[i];
            System::drawCircle(b.pos,b.radius,sf::Color(255,100,0,100));
        }
    }

};
