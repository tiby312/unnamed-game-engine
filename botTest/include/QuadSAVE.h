// *** ADDED BY HEADER FIXUP ***
#include <algorithm>
#include <iostream>
#include <vector>

// *** END ***
/*
 * Quad.h
 *
 *  Created on: May 24, 2013
 *      Author: ken

 This is a LOOSE quad tree array implementation. It's threadsafe so you can query concurrently.

 Cool things it does:
    -Query returns objects nearby as well as each of their offset, length, and lengthsqr (so you dont have to recompute)
    -No need to rebuild quadtree every game step. It checks to see if an object is still within the same node, and
    will only reinsert if its not. So if nothing is moving, nothing is changing in the quadtree.
 */
#pragma once
#include <array>
#include <mutex>
#include "MyLib.h"
#include "System.h"
//#include "Moveable.h"
#include "VectorFixed.h"
/*
    QuadObj
    interface that objects you want to keep in quadtree must implement
    objects that implement this interface are responsible for holding on to the above Cont object
*/
//template <class T>


/*
        Cont
        Container that objects in the quadtree need to hold to keep track of which node they are in.
        This allows one to know where an object is in the quadtree instantly
    */

//    class Cont{
//        public:
//            Cont():nodeD(){}
//            NodeD nodeD;
//            //std::vector<Answer> answers;
//    };





//
//class QuadObj{
//public:
//    virtual ~QuadObj(){}
//	virtual Vec2& Pos() const=0;
//	virtual Vec2& Vel() const=0;
//	virtual Num& Mass() const=0;
//	virtual Num& Radius() const=0;
//	virtual Vec3& Bound() const=0;
//	virtual Cont& getCont()=0;
//};

//
// class ResidualData{
//    public:
//    ResidualData(Vec2 o,RadiusPair r):offset(o),radiuspair(r){}
//    Vec2 offset;
//    RadiusPair radiuspair;
//    ResidualData operator-()  {
//        ResidualData a;
//        a.offset=-offset;
//        a.radiuspair=radiuspair;
//        return a;
//
//   }
//    private:
//        ResidualData(){}
//
//};





constexpr size_t myIntPowConst(size_t base,size_t power){
    return power==1 ? base: base*myIntPowConst(base,power-1);
}

constexpr size_t calcNumberOfNodes(size_t level){
    return (myIntPowConst(4,level)-1)/3;
}
/*
template <class T>
class QAnswer{
    //friend class Quad;
    public:
    T* b;
    SCircle::ResidualData r;
    QAnswer(T* bb,SCircle::ResidualData v):b(bb),r(v){}
    QAnswer(){}
};
*/

typedef uint NodeD;

uint MAXSTORE=3;
const NodeD emptyNode=std::numeric_limits<uint>::max();

template <class T,int MaxLevel=4,int maxConts=256,int MaxInQuad=128>
class Quad{
    public:
    struct QAnswer{
        T* b;
        Circle::Ans r;
    };
    struct Cont{
        T* o;
        NodeD index;
        Cont(T& o):o(&o),index(emptyNode){}
        Cont():o(nullptr),index(emptyNode){}

    };
    struct Node{
        Node():bound(),loosebound(),objs(),leaf(){}
        Box bound;
        Box loosebound;
        VectorFixed<T*,MaxInQuad> objs;
        //VectorFixed<T*,3> store;
        bool leaf;
    };
    std::array<Node,calcNumberOfNodes(MaxLevel)> nodes;
    VectorFixed<Cont,maxConts> conts;
    NodeD firstLeaf;


    Quad(Box bound):firstLeaf(nodes.size()-myIntPowConst(4,MaxLevel-1)){
        cout<<"created quad with "<<MaxLevel<<"levels. "<<nodes.size()<<" nodes. firstleaf="<<firstLeaf<<endl;
        for(size_t i=0;i<nodes.size();i++){
            nodes[i].leaf=true;
            //nodes[i].bound=Box();
            //nodes[i].loosebound=Vec3();
        }
        nodes[0].bound=bound;
        nodes[0].loosebound=Box::doubleB(nodes[0].bound);
        setbounds(0);
    }

    bool insert(T& k){
        Cont blep(k);
        //MLog::log("insert start");
        if(!insert(0,blep)){
            MLog::error("insert fail");
            return false;
        }
        if(!conts.push_back(blep)){
            MLog::error("cannot fit anyone things into quad!");
        }
        //MLog::log("insert finish");
        return true;
    }


    void update(){
        //MLog::log("update start");

        for(uint i=0;i<conts.size();i++){
            Cont& ca=conts[i];
            NodeD in=ca.index;
            T& o=*(ca.o);
            //bool a=fitsInAChild(o);//if true start insertion from this level;


            if((!nodes[in].leaf&&fitsInAChild(ca))||!nodes[ca.index].loosebound.contains(o)||!nodes[ca.index].bound.contains(o.pos)){
                reinsert(ca); //reinsert from root
            }
        }
        //MLog::log("update finish");
    }
    bool fitsInAChild(Cont& ca){
        T& o=*(ca.o);
        if(ca.index>=firstLeaf){
            return false;
        }
        std::array< NodeD,4> cin(children(ca.index));
        for(int i=0;i<4;i++){
            if(nodes[cin[i]].loosebound.contains(o)&&nodes[ca.index].bound.contains(o.pos)){
                return true;
            }
        }
        return false;
    }



	void draw(){
		draw(0);
	}


    template <int TSize>
    void query(VectorFixed<QAnswer,TSize>& ans,T& self,const Circle& shape) {
        query(ans,self,shape,0);
    }

    template <int  TSize>
    void query(VectorFixed<QAnswer,TSize>& ans,T& self,const Circle& shape,int d) {
        Node& nn=nodes[d];
        if(! nn.loosebound.intersectsApprox(shape) ){
            return;
        }

        for( auto bb  :nn.objs){
            //const SCircle bsq=bb->getShape();
            Circle::Ans res;
            if(bb!=&self&&self.intersects(*bb,res)){
                ans.push_back(QAnswer{bb,res});
            }
        }
        if(nn.leaf){
            return;
        }
        std::array< NodeD,4> n(children(d));
        for(int i=0;i<4;i++){
            query(ans,self,shape,n[i]);
        }
    }






    void setbounds(NodeD d){
		if(d<firstLeaf){
			std::array< NodeD,4> c(children(d));
            Num midsize=nodes[d].bound.z/2.0f;

            Node nd=nodes[d];
            nodes[c[0]].bound=Box(nd.bound.x,nd.bound.y,midsize);
            nodes[c[1]].bound=Box(nd.bound.x+midsize,nd.bound.y,midsize);
            nodes[c[2]].bound=Box(nd.bound.x+midsize,nd.bound.y+midsize,midsize);
            nodes[c[3]].bound=Box(nd.bound.x,nd.bound.y+midsize,midsize);
            //Num midsize2=d.node->loosebound.z/2.0f;
            for(int i=0;i<4;i++){
                nodes[c[i]].loosebound=Box::doubleB(nodes[c[i]].bound);
            }
            for(int i=0;i<4;i++){
                setbounds(c[i]);
            }
		}
	}


    static std::array< NodeD,4> children(NodeD d) {
        size_t z=4*d;
        std::array< NodeD,4> v{{
            z+1,
            z+2,
            z+3,
            z+4}
        };
        return v;
    }
    static NodeD parent(NodeD d) {
		return (d-1)/4;
	}


     bool fitsInAChild(NodeD d,const Circle& shape){
        if(d<firstLeaf){
            std::array< NodeD,4> c(children(d));
            for(int i=0;i<4;i++){
                if(nodes[c[i]].loosebound.contains(shape)){
                    return true;
                }
            }
        }
        return false;
    }


    void draw(NodeD d){
	    if(d>=nodes.size()){
            return;
	    }
	    Vec3 k=nodes[d].bound;
        sf::Color col(0,0,255,10);
        System::drawSquare(col,Vec3(k.x,k.y,k.z));
		//cout<<d.level<<endl;
        //Vec3 kj=getBound(d);
		for( auto &bb  :nodes[d].objs){
            //QuadObj* bb=*it;
            const Circle& bsq=*bb;


			//QuadObj* bbb=*it;
			System::drawLine(
					sf::Color{255,0,0,100},bsq.pos,
					Vec2(k.x,k.y));
                System::drawLine(
					sf::Color{255,0,0,100},bsq.pos,
					Vec2(k.x+k.z,k.y+k.z));
//			System::drawLine(sf::Color{1,0,0},0.5,bsq.pos,
//						Vec2(kj.x+kj.z,
//						kj.y+kj.z));

		}

        if (nodes[d].leaf){
			Vec2 kv(k.x,k.y);
			col=sf::Color(0,50,0,160);
			System::drawCircle(kv,2,sf::Color(0,1,0,255));
			//return;
			//System::drawSquare(col,Vec3(k.x,k.y,k.z));
			return;
        }



        std::array< NodeD,4> b(children(d));
        for(int i=0;i<4;i++){
            draw(b[i]);
        }


	}

    struct Tuple{
        T* o;
        NodeD targ;
    };
    void subdivide(NodeD d){
            if(d>=firstLeaf){
                return;
            }

            std::array< NodeD,4> c(children(d));
            Node& n=nodes[d];
            n.leaf=false;
            VectorFixed<Tuple,64> toRemove;
            for(uint i=0;i<4;i++){
                nodes[c[i]].leaf=true;
            }
            for(uint i=0;i<n.objs.size();i++){
                    T& o=*(n.objs[i]);

                    for(uint i=0;i<4;i++){
                        Node& blep=nodes[c[i]];
                        if(blep.loosebound.contains(o)&&blep.bound.contains(o.pos)){
                            if(!toRemove.push_back(        Tuple{&o,c[i]}       )){
                                MLog::error("PROBLEM IN SUBDIVIDE");
                            }
                            break;
                        }
                    }

            }
            //MLog::log("subdivide start");
            for(uint i=0;i<toRemove.size();i++){
                Tuple t=toRemove[i];
                if(!n.objs.remove(t.o)){
                    MLog::error("PROBLEM IN SUBDIVIDE");
                }
                Cont& ca=*findCont(*(t.o));
                actuallyInsert(t.targ,ca);
            }

    }
    Cont* findCont(T& o){
        for(uint i=0;i<conts.size();i++){
            Cont& cap=conts[i];
            if(cap.o==&o){
                return &cap;
            }
        }
        MLog::error("could not find cont!");
        return nullptr;
    }
    void actuallyInsert(NodeD d,Cont& c){
        if(nodes[d].objs.push_back(c.o)){

            c.index=d;
        }else{
            c.index=emptyNode;
        }
    }
    bool insert(NodeD d,Cont& c){
        //Node& n=getNode(d);
        T& o=*(c.o);
        if(d>nodes.size()){
            return false;
        }
        if(!nodes[d].loosebound.contains(o)||!nodes[d].bound.contains(o.pos) ){
            return false;
        }

        if(nodes[d].leaf&&nodes[d].objs.size()<MAXSTORE){
            actuallyInsert(d,c);
            return true;
        }

        //bool subdivided=false;
        if(d<firstLeaf&&nodes[d].leaf){
            subdivide(d);
            //subdivided=true;
        }

        std::array< NodeD,4> ci(children(d));
        for(int i=0;i<4;i++){
            NodeD bla=insert(ci[i],c);
            if  (  bla==true  ){
                return true;
            }
        }

        /*
        if(subdivided){
            //undo the subdivision
            for(int i=0;i<4;i++){
                for(int j=0;j<nodes[ci[i]].objs.size();j++){
                    T* fa=nodes[ci[i]].objs[j];
                    Cont& cv=findCont(*fa);
                    cv.index=d;
                    nodes[d].objs.push_back(fa);
                }
                nodes[ci[i]].objs.clear();
                nodes[ci[i]].leaf=true;
            }
            nodes[d].leaf=true;
        }*/

        actuallyInsert(d,c); //if we get here we know it can't fit into children
        return true;

    }

//    void setParentsToLeaves(NodeD d){
//            if (d.index==0) return;
//            NodeD par=parent(d);
//            Node& n=getNode(par);
//            if(n.leaf){
//                n.leaf=false;
//                setParentsToLeaves(par);
//            }
//    }
//    void actuallyInsert(NodeD d,T& o){
//        Node& n=getNode(d);
//       if(n.objs.push_back(&o)){
//            setNodeD(o,d);
//            setParentsToLeaves(d);
//        }else{
//            setNodeD(o,NodeD());
//        }
//    }
    /*
    bool insert(NodeD d,T& o){
        //cout<<"d="<<d.index<<" max="<<nodes.size()-1<<endl;;
		if(!SCircle::contains(o.getShape(),getNode(d).loosebound)||!MyTool::contains(o.Pos(),getNode(d).bound)){
			return false;
		}

		if(  d.level>=MaxLevel|| !fitsInAChild(d,o.getShape())  ){
			//link(d,o);
            if(getNode(d).objs.push_back(&o)){
                setNodeD(o,d);
            }else{
                setNodeD(o,NodeD());
            }
			return true;
		}

        bool inserted=false;

        std::array< NodeD,4> c(children(d));
        for(int i=0;i<4;i++){
            if  (  insert(c[i],o)  ){inserted=true;break;}
        }


        if(inserted==true){
            if(isLeaf(d)==true){
                setLeaf(d,false);
            }
            return true;
        }else{
            return false;
        }
	}*/

//    bool hasChildren(NodeD d){
//        if(d.level>MaxLevel){
//            return false;
//        }
//        if(!getNode(d).objs.isEmpty()){
//            return true;
//        }
//        std::array< NodeD,4> c(children(d));
//        for(int i=0;i<4;i++){
//            if  (  hasChildren(c[i])  ){
//                return true;
//            }
//        }
//        return false;
////    }
/*
    void remove2(NodeD d){
        if(d==0){
            return;
        }
        MLog::log("yo");
        bool allEmptyLeaves=true;
        std::array< NodeD,4> c(children(d));
        for(int i=0;i<4;i++){
            if  (  nodes[c[i]].leaf==false||!nodes[c[i]].objs.isEmpty()  ){
                allEmptyLeaves=false;
                break;
            }
        }
        if(allEmptyLeaves){
            nodes[d].leaf=true;
        }
        MLog::log(std::to_string(nodes[d].leaf));
        NodeD nn=parent(d);
        remove2(nn);
    }*/
    void remove2(NodeD d){
        if(d==0){
            return;
        }
        std::array< NodeD,4> c(children(d));
        uint sum=0;
        bool fail=false;
        for(uint i=0;i<4;i++){
            //sum+=
            if(!numberOfObjs(sum,c[i])){
                fail=true;
                break;
            }
        }
        //MLog::log("sum="+std::to_string(sum));
        if(fail==false){
        //if(sum<=MAXSTORE/*-nodes[d].objs.size()*/){
            //MLog::log("SUM="+std::to_string(sum));
            //MLog::log("CAP="+std::to_string(MAXSTORE-nodes[d].objs.size()));
            for(int i=0;i<4;i++){
                //sum+=numberOfObjs(c[i]);
                removeAndAddTo(d,c[i]);
            }
            nodes[d].leaf=true;
        }

        remove2(parent(d));
    }
    void removeAndAddTo(NodeD whereToAdd,NodeD d){
        if(d>=nodes.size()){
            return;
        }
        for(uint i=0;i<nodes[d].objs.size();i++){
            T* a=nodes[d].objs[i];
            Cont& c=*findCont(*a);
            if(!nodes[whereToAdd].objs.push_back(a)){
                MLog::error("PROBLEM REMOVEANDADD");
            }
            c.index=whereToAdd;
        }
        nodes[d].objs.clear();
        nodes[d].leaf=true;

        std::array<NodeD,4> c(children(d));
        for(int i=0;i<4;i++){
            removeAndAddTo(whereToAdd,c[i]);
        }

    }
    bool numberOfObjs(uint& sum,NodeD d){
        if(d>=nodes.size()){
            return true;
        }
        sum+=nodes[d].objs.size();

        if(sum>MAXSTORE){
            return false;
        }

        if(nodes[d].leaf){
            return true;
        }

        std::array<NodeD,4> c(children(d));
        for(int i=0;i<4;i++){
            if(numberOfObjs(sum,c[i])==false){return false;};
        }
        return true;
    }

	bool reinsert(Cont& ca){

        if(ca.index!=emptyNode){
            if(nodes[ca.index].objs.remove(ca.o)){

                NodeD pp=parent(ca.index);
                remove2(pp);
                //MLog::log("REMOVE START");

                ca.index=emptyNode;
            }else{
                MLog::error("ERROR REINSERTING");
            }
        }

        return insert(0,ca);
	}
};


