// *** ADDED BY HEADER FIXUP ***
#include <list>
// *** END ***

#pragma once
//#include "MyLib.h"


#include <functional>
template <class T>
class EventM{
    private:
    struct Listener{
        std::function<void(T)> func;
        size_t id;
        bool repeat;
    };
    std::vector<Listener> listeners;
    size_t currid;
    public:
        EventM():listeners(),currid(0){}


    size_t registerListener(std::function<void(T)> f,bool repeat){
        size_t i=currid;
        listeners.push_back(Listener{f,i,repeat});
        currid++;
        return i;
    }
    void degisterListener(size_t id){
        for(size_t i=0;i<listeners.size();i++){
            if(listeners.at(i).id==id){
                listeners.erase(listeners.begin()+i);
                return;
            }
        }
        std::cout<<"did not deregister listener!"<<std::endl;
    }
    void fire(T args){
        std::vector<size_t> toDelete;
         for (auto it = listeners.begin() ; it != listeners.end(); it++){

             it->func(args);
             if(it->repeat==false){
                toDelete.push_back(it->id);
             }
         }
         for(auto i:toDelete){
            degisterListener(i);
         }
    }
};


