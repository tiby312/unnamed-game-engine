#pragma once
#include "MyLib.h"
struct Circle{
    Vec2 pos;
    Num radius;

    Circle(Vec2 pos,Num radius):pos(pos),radius(radius){}
    Circle():pos(),radius(){}
    struct Ans{
        Num dis;
        Vec2 offset;
    };
    bool intersects(Circle& b,Ans& ans) {
        Vec2 offset = b.pos- pos;

        Num sq = Vec2Tool::getlengthSqr(offset);
        Num dis = std::sqrt(sq);
        if (dis < radius+b.radius) {
            ans.dis=dis;
            ans.offset=offset;
            return true;
        }
        return false;
    }
};

struct Box:public Vec3{
    //Vec3 b;
    Box(){}
    Box(float x,float y,float z):Vec3(x,y,z){
    }
    static  Box doubleB(const Box& b){
        return Box(b.x-(b.z/2.0f),b.y-(b.z/2.0f),2*b.z);
    }

    bool contains(const Circle& c){
        Vec2 cp = c.pos;
        Num cr = c.radius;
        return cp.x-cr >= x && cp.x+cr < x+z && cp.y-cr >= y && cp.y+cr < y+z;
    }

    bool contains(const Vec2& p){
        return MyTool::valueInRange(p.x, x, x+z) && MyTool::valueInRange(p.y, y, y+z);
    }
    bool intersectsApprox(const Circle& c){

        return intersects(Box(c.pos.x-c.radius,c.pos.y-c.radius,c.radius*2));
    }
    bool intersects(const Box& b){

        bool xOverlap = MyTool::valueInRange(x, b.x, b.x + b.z) ||
                        MyTool::valueInRange(b.x, x, x + z);

        bool yOverlap = MyTool::valueInRange(y, b.y, b.y + b.z) ||
                        MyTool::valueInRange(b.y, y, y + z);

        return xOverlap && yOverlap;
    }
};

