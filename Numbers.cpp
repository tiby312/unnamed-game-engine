#include <iostream>
#include <ostream>
#include <limits>
typedef int iit;


class Num{
	public:
	Num(iit n,iit d){		
		iit p=maxval/n;
		n=maxval/(d*p);
	}
	float fapval() const{
		return float(n/(float)maxval);
	}
	static iit maxval	;	
	iit n;
};
iit Num::maxval=std::numeric_limits<iit>::max();
inline std::ostream& operator<<(std::ostream& os, const Num& obj)
{		
	float x=obj.fapval();
	os << x << "("<<obj.n<<")";	
	return os;
}


class Vec2Unit{
	public:
	Vec2Unit(signed char xx,signed char yy):x(xx),y(yy){}
	signed char x;
	signed char y;	
	void func(float& nx,float& ny) const{
		nx=x/(float)128;
		ny=y/(float)128;		
	}
};
inline std::ostream& operator<<(std::ostream& os, const Vec2Unit& obj)
{
	float x;
	float y;
	obj.func(x,y);
	os << "(" <<x << ", " <<   y  << ")";  
	os << "(" <<(int)obj.x << ", " <<   (int)obj.y  << ")";  
	return os;
}

int main(){
	Num j(1,1);
	std::cout<<Num::maxval<<std::endl;
	//std::cout<<j<<std::endl;
}
