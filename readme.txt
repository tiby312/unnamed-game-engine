Unnamed Game Engine


Uses codeblocks.

Should work on windows and linux
	(boost is linked statically, but sfml is not. so u must make sure
		the executable knows where the dll or dynamic libraries are.

Requires:
MinGW 4.8.1 for windows //you should compile the below 2 librarys from source using this on linux just use most up to dat g++
Boost 1.54   //since mingw doesnt fully support C++11  (doesnt support std::thread) SO annoying
SFML 1.2.1 

Uses C++11
Also runtime needs modern a video card that supports opengl shaders (i guess opengl 3.3 compatible and up) update: for now I took out the shader code.


So when I made the game multithreaded, it ended up running slower than the single threaded unless I added a 5000+ or more bots. In that case, the multithreaded version ran at 20fps,and the single threaded ran at 15fps and the multithreaded version got better with more and more bots.I guess if you have less bots than that, it takes too long to switch between threads or wake them up and put them to sleep. I might try spin locks but they feel dirty. I guess the best option would be not to do each game step
in parallel, but instead synch up every couple of game steps or something... For now I'll just leave it single threaded. 

Overview and better comments coming soon

Demo video:
http://www.youtube.com/watch?v=nSIGjUSXDpk
