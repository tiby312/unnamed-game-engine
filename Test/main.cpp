#include <iostream>

#include "System.h"
#include "Game.h"
#include <assert.h>

using namespace std;



size_t getNumberFromConsole(){
    size_t myNumber;
    string input = "";
    while (true) {
        //cout<<"How many physical cpu cores does this computer have?"<<endl;
        cout<<"enter number="<<endl;
        getline(cin, input);
        // This code converts from string to number safely.
        stringstream myStream(input);
        if (myStream >> myNumber){
            break;
        }
        cout << "Invalid number, please try again" << endl;
    }
    return myNumber;

}

class CannonCreator{
    public:
    static void createCannon(Game& g,Team t,Vec2 pos,Vec2Unit k){
        PState ps(12);
        MState ms(pos,k);
        ShipProperty sp{0.001f,0.001f};
        Soldier* blep=&g.createSoldier(ps,ms,sp,t,true);
        g.cannoncontrol.createCannon(*blep);
    }

};
class TeamCreator{
    public:
    static void createTeam(Game& g,Team t,Vec2 po,Vec2Unit kj){
        Vec2 pos=po;


        Vec2 b;
        Vec2 perp=Vec2Tool::perp90deg(kj);


        Num maxWidth=400;
        Num maxHeight=60;
        size_t numRow=2;
        size_t numColumn=4;

        Num xspacing=maxWidth/(numColumn-1);
        Num yspacing=maxHeight/(numRow-1);


        Num ycounter=-maxHeight/2.0f;
        for(size_t i=0;i<numRow;i++){
            Num xcounter=-maxWidth/2.0f;
            for(size_t j=0;j<numColumn;j++){
                MState ms(pos+perp*xcounter+kj*ycounter,Vec2(),kj);
                PState ps(2);
                //Formation* f=&TeamCreator::createFormationBunch(g,PState(3),MState(Vec2(256,256)),Team(sf::Color(255,0,0),1),200);
                createFormationBunch(g,ps,ms,t,64);
                //cout<<xcounter<<"=kjklkjlk="<<pos.x<<endl;
                xcounter+=xspacing;

            }
            ycounter+=yspacing;
        }
    }

    static Formation& createFormationBunch(Game& g,PState ps,MState ms,Team team,size_t num){
        Num spacing=ps.radius*3.0f;

        //Team team(sf::Color(255,0,0),0);
        //Vec2 k(256,256);
        ShipProperty sp{0.00002f,0.0001f};
        Soldier* leader=&g.createSoldier(ps,ms,sp,team,true);
        std::vector<Soldier*> bots;
        Vec2Unit rot=ms.ang;

        for(size_t i=0;i<num;i++){
            rotateVec(rot,0.2f);
            bots.push_back(&g.createSoldier(ps,MState(ms.pos,ms.vel,rot),sp,team,false));
        }
        //MState ms(k);
        //ms.ang=kj;
        Formation& f=FormationControlManager::createFormation(g.formationcontrol,leader,bots,ms,team,spacing);
        return f;
    }
};



#define MAXUTIL 1000
//
//int*
//make_random_r(size_t m, size_t n)
//{
//  int i,j;
//  int* r;
//  time_t curr;
//  assert(r = (int*)malloc(sizeof(int)*m*n));
//  curr = time(NULL);
//  srand(curr);
//  //puts("\nINPUT: ");
//  for(i=0;i<m;i++)
//  {
//    //printf("  [ ");
//    for(j=0;j<n;j++)
//    {
//      r[i*n+j] = 1+(rand() % MAXUTIL);
//      //printf("%4d ", r[i*n+j]);
//    }
//    //puts(" ]");
//  }
//  return(r);
//}
//void hungariantest(){
//    hungarian_problem_t p;
//
//  /* an example cost matrix */
//  int r[3*3] =  {  100, 1, 1,
//		   100, 2, 2,
//		   1, 0, 0};
//  int** m = array_to_matrixx(r,3,3);
//
//  /* initialize the gungarian_problem using the cost matrix*/
//  int matrix_size = hungarian_init(&p, m , 3,3, HUNGARIAN_MODE_MINIMIZE_COST) ;
//
//  fprintf(stderr, "assignement matrix has a now a size %d rows and %d columns.\n\n",  matrix_size,matrix_size);
//
//  /* some output */
//  fprintf(stderr, "cost-matrix:");
//  hungarian_print_costmatrix(&p);
//
//  /* solve the assignement problem */
//  hungarian_solve(&p);
//
//  /* some output */
//  fprintf(stderr, "assignment:");
//  hungarian_print_assignment(&p);
//  std::vector<int> k=getAssignment(p.assignment,matrix_size,matrix_size);
//  cout<<"Ans"<<endl;
//  for(size_t i=0;i<k.size();i++){
//    cout<<k[i]<<endl;
//  }
//
//}

/*
 * makes and returns a pointer to an mXn rating matrix with values uniformly
 * distributed between 1 and MAXUTIL
 *
 * allocates storage, which the caller should free().
 */





int main()
{

    //hungariantest();
    size_t myNumber = 1;
//    while (true) {
//        cout<<"How many physical cpu cores does this computer have?"<<endl;
//        getline(cin, input);
//        // This code converts from string to number safely.
//        stringstream myStream(input);
//        if (myStream >> myNumber){
//            if(myNumber==0){
//                cout<<"zero haha.."<<endl;
//            }else{
//                break;
//            }
//        }
//
//        cout << "Invalid number, please try again" << endl;
//    }
//    cout << "The game will run on : " << myNumber << " thread(s). " << endl;


    Num size=512;
	sf::RenderWindow& s=System::Start(myNumber);

    Game& g= *new Game(size,s);
    Game* gp=&g;

    std::function<void(Vec2)> func=[gp](Vec2 pp){
        ShipProperty sp{0.00002f,0.0001f};
        for(size_t i=0;i<20;i++){
            gp->createSoldier(PState(10.0f+i/3.0f),MState(pp),sp,Team::neutral,false);
        }
    };
    System::rightclickevent.registerListener(func,true);





        //TeamCreator::createFormationBunch(g,PState(3),MState(Vec2(100,100)),Team(sf::Color(0,255,0),2),100);
        //TeamCreator::createFormationBunch(g,PState(3),MState(Vec2(256,256)),Team(sf::Color(0,255,0),2),100);
        //TeamCreator::createFormationBunch(g,PState(3),MState(Vec2(256,256)),Team(sf::Color(0,255,0),2),100);
        for(int i=0;i<2038;i++){
        CannonCreator::createCannon(g,Team(sf::Color(200,0,0),1),Vec2(50,256),Vec2Unit(0,1));
        }
        CannonCreator::createCannon(g,Team(sf::Color(200,0,0),1),Vec2(256,256),Vec2Unit(0,1));
        //TeamCreator::createTeam(g,Team(sf::Color(200,0,0),1),Vec2(256,0+100),Vec2Unit(0,1));
        //TeamCreator::createTeam(g,Team(sf::Color(0,0,255),2),Vec2(256,512-100),Vec2Unit(0,-1));



        //TeamCreator::createFormationBunch(g,PState(3),MState(Vec2(100,100)),Team(sf::Color(0,255,0),2),64);
//        Formation* f=&TeamCreator::createFormationBunch(g,PState(3),MState(Vec2(256,256)),Team(sf::Color(255,0,0),1),6);
//
//        std::function<void(Vec2)> func=[gp,f](Vec2 a){
//            //cout<<"removing closest bot!"<<endl;
//            std::vector<Quad<Soldier>::Answer> ans;
//            SCircle k(a,20);
//            gp->botcontrol.queryRadius(ans,nullptr,k);
//            //Bot& bot=botcontrol.findClosest(a);
//            Num closestDist=gp->formationcontrol.world.getMaxDistance();
//            Num closestDistSqr=closestDist*closestDist;
//            Soldier* closestBot=nullptr;
//            for(auto an:ans){
//
//                //Vec2 offset=i->Pos()-a;
//                //Num lengthsqr=Vec2Tool::getlengthSqr(offset);
//                if(an.r.radius*an.r.radius<closestDistSqr){
//                    closestDistSqr=an.r.radius*an.r.radius;
//                    closestBot=an.b;
//                }
//            }
//            if(closestBot!=nullptr){
//                gp->botcontrol.healthcontrol.handleHealthLoss(*closestBot,100.0f);
//                //f->removeMember(*closestBot);
//            }
//
//        };
//        System::rightclickevent.registerListener(func,true);

//        std::function<void(size_t)> func2=[f](size_t a){
//            size_t num=getNumberFromConsole();
//            cout<<"resizing formation to="<<num<<endl;
//            PhalanxResizeControl::resize(f->farranger,num,Vec2Unit(1,0));
//            PhalanxContControl::print(f->farranger);
//        };
//        System::consoleopenevent.registerListener(func2,true);



	while (System::running()){

        System::stepStart();
        g.tick();

		System::stepEnd();
	}
	cout<<"shutdown"<<endl;

    return 0;
}
