const vec2 center = vec2(0.5, 0.5);
const float radius = 0.5;



varying vec2 textureCoordinate;
void main()
 {
	float distanceFromCenter = distance(center, textureCoordinate);
	float checkForPresenceWithinCircle = step(distanceFromCenter, radius);
	
	//float lol=max(0.0,radius-0.2);
	//float checkForPresenceWithinCircle=1.0-smoothstep(lol,radius,distanceFromCenter);
	
	
	
	float hh=max(0.0,radius-0.5);
	float checkForPresenceWithinCircle2=smoothstep(hh,radius,distanceFromCenter);
	
	
	//float checkForPresenceWithinCircle2 = step(radius-distanceFromCenter, 0.2);
	
	
	
	
	
	float jj=min(checkForPresenceWithinCircle,checkForPresenceWithinCircle2);
	
	
	
     	//gl_FragColor = vec4(0.0, 0.0, 0.0, checkForPresenceWithinCircle);// * checkForPresenceWithinCircle;     
	gl_FragColor=gl_Color*jj;
 }