varying vec2 textureCoordinate;

void main()
{
	//gl_PointSize=10.0;
    // transform the vertex position
    gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;

    // transform the texture coordinates
	//gl_TexCoord[0] = gl_TextureMatrix[0] * gl_MultiTexCoord0;
	textureCoordinate=gl_MultiTexCoord0.xy;
    // forward the vertex color

    gl_FrontColor = gl_Color;

}